﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using API.Controllers;
using API.Data;
using API.DTOs.Creation;
using API.DTOs.Edition;
using API.DTOs.View;
using API.Models;
using API.Extensions;
using MyTested.AspNetCore.Mvc;
using Xunit;

namespace API.Tests.Controllers {

    public class CompanyControllerTests : MyController<CompaniesController> {
        public CompanyControllerTests() {
            this.WithDbContext(dbContext => dbContext.WithEntities(ctx => {
                var db = ctx as DatabaseContext;
                db.EnsureSeedDataForContext();
            }));
        }

        [Fact]
        public void Get_Company() {
            this.Calling(c => c.GetById(1))
                .ShouldReturn()
                .Ok()
                .WithModelOfType<CompanyViewDto>()
                .Passing(model =>
                    model.Name.Equals("HdN"));
        }

        [Fact]
        public void Post_Company() {
            CompanyCreationDTO dto = new CompanyCreationDTO {
                Name = "TestPostCompany"
            };

            this.Calling(c => c.Create(dto))
                .ShouldReturn()
                .Created()
                .WithModelOfType<Company>()
                .Passing(model =>
                    model.Name.Equals("TestPostCompany"));
        }

        [Fact]
        public void Put_Company() {
            CompanyEditionDto company = new CompanyEditionDto {
                Id = 1,
                Name = "HdN",
                Address = "Beverly Hills",
                PostalCode = "1234-567",
                MaxAllowedDistance = 50
            };

            this.Calling(c => c.Edit(company.Id, company))
                .ShouldReturn()
                .NoContent();
        }
    }

}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using API.Controllers;
using API.Data;
using API.DTOs.Creation;
using API.DTOs.Edition;
using API.Extensions;
using MyTested.AspNetCore.Mvc;
using Xunit;
using API.DTOs.View;
using API.Models;
using API.Tests.Helpers;

namespace API.Tests.Controllers {

    public class OrdersControllerTests : MyController<OrdersController> {
        public OrdersControllerTests() {
            this.WithDbContext(dbContext => dbContext.WithEntities(ctx => {
                var db = ctx as DatabaseContext;
                db.EnsureSeedDataForContext();
            }));
        }

        [Fact]
        public void Get_All_Orders() {
            this.WithHttpContext(AuthorizationHelpers.AddUser)
                .Calling(c => c.GetList(1, 10))
                .ShouldReturn()
                .Ok()
                .WithModelOfType<PaginatedListViewDto<OrderViewDto>>()
                .Passing(model => model.Page == 1
                                  && model.Limit == 10
                                  && model.Items != null);
        }
        
        [Fact]
        public void Get_Ready_Orders() {
            this.WithHttpContext(AuthorizationHelpers.AddUser)
                .Calling(c => c.GetReadyOrdersList(1, 10))
                .ShouldReturn()
                .Ok()
                .WithModelOfType<PaginatedListViewDto<OrderViewDto>>()
                .Passing(model => model.Page == 1
                                  && model.Limit == 10
                                  && model.Items != null);
        }

        [Fact]
        public void Get_Order() {
            this.Calling(c => c.GetById(1))
                .ShouldReturn()
                .Ok()
                .WithModelOfType<OrderViewDto>()
                .Passing(model => model.Id == 1 &&
                                  model.ReadyToDistribute &&
                                  model.OnArrivingNotified == false);
        }

        [Fact]
        public void Post_Order() {
            OrderCreationDTO dto = new OrderCreationDTO {
                CustomerId = 1,
                EstimatedTime = 5,
                Address = "Rua de teste",
                Address2 = "1º Esq.",
                PostalCode = "1234-567",
                Items = new List<ItemCreationDTO>(new[] {
                    new ItemCreationDTO {
                        Quantity = 1,
                        ProductId = 2,
                        ProductTypeId = 1
                    }
                })
            };

            var time = dto.EstimatedTime + 18; // 18 = GoogleMapsMock minutes
            this.WithHttpContext(AuthorizationHelpers.AddUser)
                .Calling(c => c.Create(dto))
                .ShouldReturn()
                .Created()
                .WithModelOfType<OrderViewDto>()
                .Passing(model =>
                    model.EstimatedDate.CompareTo(DateTime.UtcNow.Add(new TimeSpan(0, 0, time, 0))) < 0 &&
                    model.ReadyToDistribute == false &&
                    model.ItemsComplete == false &&
                    model.Address.Equals("Rua de teste") &&
                    model.Items.Count > 0);
        }
        
        // Order must be created as ready to assemble by default
        [Fact]
        public void Post_Order_ItemProductWithoutPipeline() {
            OrderCreationDTO dto = new OrderCreationDTO {
                CustomerId = 1,
                EstimatedTime = 5,
                Address = "Rua de teste",
                Address2 = "1º Esq.",
                PostalCode = "1234-567",
                Items = new List<ItemCreationDTO>(new[] {
                    new ItemCreationDTO {
                        Quantity = 1,
                        ProductId = 6
                    },
                    new ItemCreationDTO {
                        Quantity = 1,
                        ProductId = 9
                    }
                })
            };

            this.WithHttpContext(AuthorizationHelpers.AddUser)
                .Calling(c => c.Create(dto))
                .ShouldReturn()
                .Created()
                .WithModelOfType<OrderViewDto>()
                .Passing(model =>
                    model.ItemsComplete == true && 
                    model.ReadyToDistribute == false);
        }

        [Fact]
        public void Post_Order_NotValid() {
            OrderCreationDTO dto = new OrderCreationDTO {
                CustomerId = 1,
                EstimatedTime = 5
            };

            this.WithHttpContext(AuthorizationHelpers.AddUser)
                .Calling(c => c.Create(dto))
                .ShouldReturn()
                .BadRequest();
        }

        [Fact]
        public void Edit_Order() {
            OrderEditionDto dto = new OrderEditionDto {
                Id = 4,
                ReadyToDistribute = true,
                Address = "Santo António dos Cavaleiros",
                Address2 = "1º Dir.",
                PostalCode = "8765-432"
            };

            this.WithHttpContext(AuthorizationHelpers.AddUser)
                .Calling(c => c.Edit(dto.Id, dto))
                .ShouldReturn()
                .NoContent();
        }

        [Fact]
        public void Edit_Order_Item() {
            ItemEditionDto dto = new ItemEditionDto {
                Id = 4,
                ProcessId = 15,
                Completed = true
            };
            
            this.WithHttpContext(AuthorizationHelpers.AddUser)
                .Calling(c => c.EditItem(dto.Id, dto))
                .ShouldReturn()
                .NoContent();
        }
    }

}

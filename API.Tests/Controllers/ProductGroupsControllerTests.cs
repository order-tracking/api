﻿using System.Collections.Generic;
using API.Controllers;
using API.Data;
using API.DTOs.Creation;
using API.DTOs.Edition;
using API.Models;
using API.Extensions;
using MyTested.AspNetCore.Mvc;
using Xunit;
using API.DTOs.View;
using API.Tests.Helpers;

namespace API.Tests.Controllers {

    public class ProductGroupsControllerTests : MyController<ProductGroupsController> {
        public ProductGroupsControllerTests() {
            this.WithDbContext(dbContext => dbContext.WithEntities(ctx => {
                var db = ctx as DatabaseContext;
                db.EnsureSeedDataForContext();
            }));
        }

        [Fact]
        public void Get_First_ProductGroups() {
            this.WithHttpContext(AuthorizationHelpers.AddUser)
                .Calling(c => c.GetFirstProductGroups())
                .ShouldReturn()
                .Ok()
                .WithModelOfType<List<ProductGroupViewDto>>()
                .Passing(model => model.Count > 0);
        }

        [Fact]
        public void Post_ProductGroup() {
            ProductGroupCreationDTO dto = new ProductGroupCreationDTO {
                Name = "ProductGroupTest"
            };

            this.WithHttpContext(AuthorizationHelpers.AddUser)
                .Calling(c => c.Create(dto))
                .ShouldReturn()
                .Created()
                .WithModelOfType<ProductGroupViewDto>()
                .Passing(model => model.Name.Equals("ProductGroupTest"));
        }

        [Fact]
        public void Post_ProductGroup_NotValid() {
            ProductGroupCreationDTO dto = new ProductGroupCreationDTO { };

            this.WithHttpContext(AuthorizationHelpers.AddUser)
                .Calling(c => c.Create(dto))
                .ShouldReturn()
                .BadRequest();
        }

        [Fact]
        public void Edit_ProductGroup() {
            ProductGroupEditionDto pg = new ProductGroupEditionDto {
                Id = 3,
                Name = "Teste"
            };
            this.WithHttpContext(AuthorizationHelpers.AddUser)
                .Calling(c => c.Edit(pg.Id, pg))
                .ShouldReturn()
                .NoContent();
        }

        [Fact]
        public void Get_Products_Of_ProductGroup() {
            this.Calling(c => c.GetProducts(8))
                .ShouldReturn()
                .Ok()
                .WithModelOfType<List<Product>>()
                .Passing(model => model.Count == 2);
        }

        [Fact]
        public void Delete_ProductGroup() {
            this.Calling(c => c.Delete(3))
                .ShouldReturn()
                .Ok();
        }
    }

}

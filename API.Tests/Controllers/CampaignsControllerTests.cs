﻿using API.Controllers;
using API.Data;
using API.DTOs.Creation;
using API.DTOs.View;
using API.Extensions;
using API.Tests.Helpers;
using MyTested.AspNetCore.Mvc;
using Xunit;

namespace API.Tests.Controllers {

    public class CampaignsControllerTests : MyController<CampaignsController> {
        public CampaignsControllerTests() {
            this.WithDbContext(dbContext => dbContext.WithEntities(ctx => {
                var db = ctx as DatabaseContext;
                db.EnsureSeedDataForContext();
            }));
        }

        [Fact]
        public void Get_Campaigns() {
            this.WithHttpContext(AuthorizationHelpers.AddUser)
                .Calling(c => c.GetList(1, 10))
                .ShouldReturn()
                .Ok()
                .WithModelOfType<PaginatedListViewDto<CampaignViewDto>>()
                .Passing(model => 
                    model.Page == 1 &&
                    model.Limit == 10);
        }

        [Fact]
        public void Post_Campaign() {
            CampaignCreationDTO dto = new CampaignCreationDTO {
                Title = "Campaign",
                Message = "This is a campaign"
            };
            
            this.WithHttpContext(AuthorizationHelpers.AddUser)
                .Calling(c => c.Create(dto))
                .ShouldReturn()
                .Created()
                .WithModelOfType<CampaignViewDto>()
                .Passing(model => 
                    model.Message.Equals(dto.Message) &&
                    model.Title.Equals(dto.Title));
        }
    }

}
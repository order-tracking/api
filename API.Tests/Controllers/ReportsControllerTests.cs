﻿using API.Controllers;
using API.Data;
using API.DTOs.Creation;
using API.Extensions;
using API.Models;
using API.Tests.Helpers;
using MyTested.AspNetCore.Mvc;
using Xunit;

namespace API.Tests.Controllers {

    public class ReportsControllerTests : MyController<ReportsController> {
        public ReportsControllerTests() {
            this.WithDbContext(dbContext => dbContext.WithEntities(ctx => {
                var db = ctx as DatabaseContext;
                db.EnsureSeedDataForContext();
            }));
        }

        [Fact]
        public void Get_Reports() {
            ReportDto dto = new ReportDto {
                Days = 20
            };

            this.WithHttpContext(AuthorizationHelpers.AddUser)
                .Calling(c => c.GetReport(dto))
                .ShouldReturn()
                .Ok()
                .WithModelOfType<Report>();
        }
    }

}

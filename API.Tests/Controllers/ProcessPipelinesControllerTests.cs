﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using API.Controllers;
using API.Data;
using API.DTOs.Creation;
using API.DTOs.Edition;
using API.Models;
using API.Extensions;
using MyTested.AspNetCore.Mvc;
using Xunit;
using API.DTOs.View;
using API.Tests.Helpers;

namespace API.Tests.Controllers {

    public class ProcessPipelinesControllerTests : MyController<ProcessPipelinesController> {
        public ProcessPipelinesControllerTests() {
            this.WithDbContext(dbContext => dbContext.WithEntities(ctx => {
                var db = ctx as DatabaseContext;
                db.EnsureSeedDataForContext();
            }));
        }

        [Fact]
        public void Get_Pipelines_Of_Company() {
            this.WithHttpContext(AuthorizationHelpers.AddUser)
                .Calling(c => c.GetList(1, 10))
                .ShouldReturn()
                .Ok()
                .WithModelOfType<PaginatedListViewDto<ProcessPipelineViewDto>>()
                .Passing(model => model.Page == 1
                                  && model.Limit == 10
                                  && model.Items.Count > 0);
        }

        [Fact]
        public void Get_Pipeline() {
            this.WithHttpContext(AuthorizationHelpers.AddUser)
                .Calling(c => c.GetById(1))
                .ShouldReturn()
                .Ok()
                .WithModelOfType<ProcessPipelineViewDto>()
                .Passing(model => model.Id == 1 &&
                                  model.Processes.Count == 2);
        }
        
        [Fact]
        public void Get_PipelineProcesses_Ordered() {
            this.WithHttpContext(AuthorizationHelpers.AddUser)
                .Calling(c => c.GetById(1))
                .ShouldReturn()
                .Ok()
                .WithModelOfType<ProcessPipelineViewDto>()
                .Passing(model => {
                    bool toReturn = true;
                    for (var i = 0; i < model.Processes.Count; i++) {
                        if (model.Processes[i].OrderNumber != i + 1) {
                            toReturn = false;
                        }
                    }
                    return toReturn && model.Id == 1 && model.Processes.Count == 2;
                });
        }

        [Fact]
        public void Post_Pipeline() {
            ProcessPipelineCreationDTO dto = new ProcessPipelineCreationDTO {
                Name = "PipelineTest",
                Processes = new List<ProcessCreationDTO>(new[] {
                    new ProcessCreationDTO {OrderNumber = 1, Name = "ProcessTest 1"},
                    new ProcessCreationDTO {OrderNumber = 2, Name = "ProcessTest 2"}
                })
            };

            this.WithHttpContext(AuthorizationHelpers.AddUser)
                .Calling(c => c.Create(dto))
                .ShouldReturn()
                .Created()
                .WithModelOfType<ProcessPipelineViewDto>()
                .Passing(model => model.Name.Equals("PipelineTest") &&
                                  model.Processes.Count == 2);
        }

        [Fact]
        public void Post_Pipeline_NotValid() {
            ProcessPipelineCreationDTO dto = new ProcessPipelineCreationDTO {
                Name = "PipelineTest"
            };

            this.WithHttpContext(AuthorizationHelpers.AddUser)
                .Calling(c => c.Create(dto))
                .ShouldReturn()
                .BadRequest();
        }

        [Fact]
        public void Put_Pipeline() {
            ProcessPipelineEditionDto pipeline = new ProcessPipelineEditionDto {
                Id = 2,
                Name = "Test",
                Processes = new List<ProcessEditionDto>(new [] {
                    new ProcessEditionDto {
                        Id = 5,
                        Name = "Teste 1"
                    },
                    new ProcessEditionDto {
                        Id = 6,
                        Name = "Teste 2"
                    }
                })
            };

            this.Calling(c => c.Edit(pipeline.Id, pipeline))
                .ShouldReturn()
                .NoContent();
        }
    }

}

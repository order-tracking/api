﻿using System.Collections.Generic;
using API.Controllers;
using API.Data;
using API.DTOs.Creation;
using API.DTOs.View;
using API.Extensions;
using API.Tests.Helpers;
using MyTested.AspNetCore.Mvc;
using Xunit;

namespace API.Tests.Controllers {

    public class ProductControllerTests : MyController<ProductsController> {
        
        public ProductControllerTests() {
            this.WithDbContext(dbContext => dbContext.WithEntities(ctx => {
                var db = ctx as DatabaseContext;
                db.EnsureSeedDataForContext();
            }));
        }
        
        [Fact]
        public void Post_ProductNoPipeline() {
            ProductCreationDTO dto = new ProductCreationDTO {
                Name = "ProductTest",
                ProductGroupId = 1
            };

            this.WithHttpContext(AuthorizationHelpers.AddUser)
                .Calling(c => c.Create(dto))
                .ShouldReturn()
                .Created()
                .WithModelOfType<ProductViewDto>()
                .Passing(model => model.Name.Equals("ProductTest"));
        }

        [Fact]
        public void Post_ProductWithTypes() {
            ProductCreationDTO dto = new ProductCreationDTO {
                Name = "ProductTest",
                ProductGroupId = 1,
                ProductTypes = new List<ProductTypeCreationDTO> {
                    new ProductTypeCreationDTO {
                        Name = "Type 1"
                    },
                    new ProductTypeCreationDTO {
                        Name = "Type 2"
                    },
                    new ProductTypeCreationDTO {
                        Name = "Type 3"
                    }
                }
            };

            this.WithHttpContext(AuthorizationHelpers.AddUser)
                .Calling(c => c.Create(dto))
                .ShouldReturn()
                .Created()
                .WithModelOfType<ProductViewDto>()
                .Passing(model => model.Name.Equals("ProductTest") &&
                                  model.ProductTypes.Count == 3);
        }
    }

}

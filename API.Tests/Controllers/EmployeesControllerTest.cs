﻿using API.Controllers;
using API.Data;
using API.DTOs.Creation;
using API.DTOs.Edition;
using API.Models;
using API.Extensions;
using MyTested.AspNetCore.Mvc;
using Xunit;
using API.DTOs.View;
using API.Tests.Helpers;

namespace API.Tests.Controllers {

    public class EmployeesControllerTest : MyController<EmployeesController> {
        public EmployeesControllerTest() {
            this.WithDbContext(dbContext => dbContext.WithEntities(ctx => {
                var db = ctx as DatabaseContext;
                db.EnsureSeedDataForContext();
            }));
        }

        [Fact]
        public void Get_All_Employees() {
            this.WithHttpContext(AuthorizationHelpers.AddUser)
                .Calling(c => c.GetList(1, 10))
                .ShouldReturn()
                .Ok()
                .WithModelOfType<PaginatedListViewDto<EmployeeViewDto>>()
                .Passing(model => model.Count > 0);
        }

        [Fact]
        public void Get_Employees_Paginated() {
            this.WithHttpContext(AuthorizationHelpers.AddUser)
                .Calling(c => c.GetList(1, 10))
                .ShouldReturn()
                .Ok()
                .WithModelOfType<PaginatedListViewDto<EmployeeViewDto>>()
                .Passing(model =>
                    model.Count > 0
                    && !model.HasNextPage
                    && !model.HasPreviousPage
                    && model.Limit == 10
                    && model.Page == 1);
        }

        [Fact]
        public void Get_Employee() {
            this.WithHttpContext(AuthorizationHelpers.AddUser)
                .Calling(c => c.GetById(1))
                .ShouldReturn()
                .Ok()
                .WithModelOfType<EmployeeViewDto>()
                .Passing(model =>
                    model.Name.Equals("Nuno Reis")
                    && model.Id == 1);
        }

        [Fact]
        public void Post_Employee() {
            EmployeeCreationDTO dto = new EmployeeCreationDTO {
                Name = "TestPostEmployee",
                PhoneNumber = "123456123",
                Password = "password",
                Role = Role.Employee
            };

            this.WithHttpContext(AuthorizationHelpers.AddUser)
                .Calling(c => c.Create(dto))
                .ShouldReturn()
                .Created()
                .WithModelOfType<EmployeeViewDto>()
                .Passing(model =>
                    model.Name.Equals("TestPostEmployee") &&
                    model.Role != null && model.Role.Equals(Role.Employee) &&
                    model.PhoneNumber.Equals("123456123"));
        }

        [Fact]
        public void Post_Employee_NotValid() {
            EmployeeCreationDTO dto = new EmployeeCreationDTO { };

            this.WithHttpContext(AuthorizationHelpers.AddUser)
                .Calling(c => c.Create(dto))
                .ShouldReturn()
                .BadRequest();
        }

        [Fact]
        public void Edit_Employee() {
            EmployeeEditionDto employee = new EmployeeEditionDto {
                Name = "Ricardo Cacheira",
                Role = Role.Admin
            };

            this.WithHttpContext(AuthorizationHelpers.AddUserToContext(1, 1, Role.Developer))
                .Calling(c => c.Edit(3, employee))
                .ShouldReturn()
                .NoContent();
        }

        [Fact]
        public void Edit_Employee_NotValid() {
            EmployeeEditionDto employee = new EmployeeEditionDto {
                Name = "Nuno Reis",
                Role = Role.Developer
            };

            this.WithHttpContext(AuthorizationHelpers.AddUserToContext(2, 5, Role.Owner))
                .Calling(c => c.Edit(1, employee))
                .ShouldHave()
                .ModelState(modelState =>
                    modelState.ContainingError("Role")
                        .ThatEquals("Employee Role is higher than yours")
                )
                .AndAlso()
                .ShouldReturn()
                .BadRequest();
        }

        [Fact]
        public void Delete_Employee() {
            this.WithHttpContext(AuthorizationHelpers.AddUserToContext(1, 2, Role.Developer))
                .Calling(c => c.Delete(3))
                .ShouldReturn()
                .Ok()
                .WithNoModel();

            this.Calling(c => c.GetById(3))
                .ShouldReturn()
                .NotFound();
        }
    }

}

﻿using System.Collections.Generic;
using System.Security.Claims;
using API.Controllers;
using API.Data;
using API.DTOs.Creation;
using API.DTOs.Edition;
using API.Models;
using API.Extensions;
using MyTested.AspNetCore.Mvc;
using Xunit;
using API.DTOs.View;
using API.Tests.Helpers;

namespace API.Tests.Controllers {

    public class CustomersControllerTests : MyController<CustomersController> {
        public CustomersControllerTests() {
            this.WithDbContext(dbContext => dbContext.WithEntities(ctx => {
                var db = ctx as DatabaseContext;
                db.EnsureSeedDataForContext();
            }));
        }

        [Fact]
        public void Get_All_Customers() {
            this.WithHttpContext(AuthorizationHelpers.AddUser)
                .Calling(c => c.GetList(1, 10))
                .ShouldReturn()
                .Ok()
                .WithModelOfType<PaginatedListViewDto<CustomerViewDto>>()
                .Passing(model => model.Count > 0);
        }

        [Fact]
        public void Get_Customers_Paginated() {
            this.WithHttpContext(AuthorizationHelpers.AddUser)
                .Calling(c => c.GetList(1, 2))
                .ShouldReturn()
                .Ok()
                .WithModelOfType<PaginatedListViewDto<CustomerViewDto>>()
                .Passing(model =>
                    model.Limit == 2
                    && model.Page == 1);
        }

        [Fact]
        public void Get_Customer() {
            this.Calling(c => c.GetById(1))
                .ShouldReturn()
                .Ok()
                .WithModelOfType<CustomerViewDto>()
                .Passing(model => model.Id == 1
                                  && model.Name.Equals("Nuno Reis"));
        }

        [Fact]
        public void Post_Customer() {
            CustomerCreationDTO dto = new CustomerCreationDTO {
                Name = "TestPost",
                PhoneNumber = "123436689"
            };

            this.WithHttpContext(AuthorizationHelpers.AddUser)
                .Calling(c => c.Create(dto))
                .ShouldReturn()
                .Created()
                .WithModelOfType<CustomerViewDto>()
                .Passing(model =>
                    model.Name.Equals("TestPost")
                    && model.PhoneNumber.Equals("123436689"));
        }

        [Fact]
        public void Post_Customer_NotValid() {
            CustomerCreationDTO dto = new CustomerCreationDTO {
                Name = "TestPost"
            };

            this.WithHttpContext(AuthorizationHelpers.AddUser)
                .Calling(c => c.Create(dto))
                .ShouldReturn()
                .BadRequest();
        }
        
        [Fact]
        public void Post_Customer_DuplicateNumber() {
            CustomerCreationDTO dto = new CustomerCreationDTO {
                Name = "User Test",
                PhoneNumber = "925706007"
            };

            this.WithHttpContext(AuthorizationHelpers.AddUserToContext(2))
                .Calling(c => c.Create(dto))
                .ShouldReturn()
                .BadRequest();
        }

        [Fact]
        public void Edit_Customer() {
            CustomerEditionDto customer = new CustomerEditionDto {
                Id = 1,
                Name = "Nuno Reis",
                PhoneNumber = "123436689"
            };

            this.Calling(c => c.Edit(1, customer))
                .ShouldReturn()
                .NoContent();
        }

        [Fact]
        public void Delete_Customer() {
            this.Calling(c => c.Delete(1))
                .ShouldReturn()
                .Ok()
                .WithNoModel();

            this.Calling(c => c.GetById(1))
                .ShouldReturn()
                .NotFound();
        }
    }

}

﻿using System.Collections.Generic;
using System.Linq;
using API.Controllers;
using API.Data;
using API.DTOs.Creation;
using API.DTOs.Edition;
using API.DTOs.View;
using API.Extensions;
using API.Models;
using API.Tests.Helpers;
using MyTested.AspNetCore.Mvc;
using Xunit;
using static API.DTOs.Creation.JourneyCreationDTO;

namespace API.Tests.Controllers {

    public class JourneysControllerTests : MyController<JourneysController> {
        
        public JourneysControllerTests() {
            this.WithDbContext(dbContext => dbContext.WithEntities(ctx => {
                var db = ctx as DatabaseContext;
                db.EnsureSeedDataForContext();
            }));
        }

        [Fact]
        public void Get_Journeys_Paginated() {
            this.WithHttpContext(AuthorizationHelpers.AddUser)
                .Calling(c => c.GetList(1, 2, null))
                .ShouldReturn()
                .Ok()
                .WithModelOfType<PaginatedListViewDto<JourneyViewDto>>()
                .Passing(model =>
                    model.Limit == 2
                    && model.Page == 1);
        }
        
        [Fact]
        public void Get_CompleteJourneys_Paginated() {
            this.WithHttpContext(AuthorizationHelpers.AddUser)
                .Calling(c => c.GetList(1, 2, "complete"))
                .ShouldReturn()
                .Ok()
                .WithModelOfType<PaginatedListViewDto<JourneyViewDto>>()
                .Passing(model =>
                    model.Count == 2);
        }
        
        [Fact]
        public void Get_IncompleteJourneys_Paginated() {
            this.WithHttpContext(AuthorizationHelpers.AddUser)
                .Calling(c => c.GetList(1, 2, "incomplete"))
                .ShouldReturn()
                .Ok()
                .WithModelOfType<PaginatedListViewDto<JourneyViewDto>>()
                .Passing(model =>
                    model.Count == 1);
        }

        [Fact]
        public void Get_Journey() {
            this.WithHttpContext(AuthorizationHelpers.AddUser)
                .Calling(c => c.GetById(1))
                .ShouldReturn()
                .Ok()
                .WithModelOfType<JourneyViewDto>()
                .Passing(model => model.Id == 1 &&
                                  model.Employee.Id == 1);
        }

        [Fact]
        public void Get_Employees_Journey() {
            this.WithHttpContext(AuthorizationHelpers.AddUser)
                .Calling(c => c.GetIncompleteEmployeeJourneys(1))
                .ShouldReturn()
                .Ok()
                .WithModelOfType<List<JourneyViewDto>>()
                .Passing(model => model.Count > 0);
        }
        
        [Fact]
        public void Get_Unexistent_Journey() {
            this.WithHttpContext(AuthorizationHelpers.AddUser)
                .Calling(c => c.GetById(99))
                .ShouldReturn()
                .NotFound();
        }

        [Fact]
        public void Edit_Journey() {
            JourneyEditionDto journey = new JourneyEditionDto {
                Id = 1,
                Completed = true
            };
            this.WithHttpContext(AuthorizationHelpers.AddUser)
                .Calling(c => c.Edit(journey.Id, journey))
                .ShouldReturn()
                .NoContent();
        }

        [Fact]
        public void Post_Journey() {
            JourneyCreationDTO dto = new JourneyCreationDTO {
                EmployeeId = 1,
                Orders = new List<JourneyOrderCreationDTO>(new JourneyOrderCreationDTO[] {
                    new JourneyOrderCreationDTO { Id = 4 }
                })
            };

            this.WithHttpContext(AuthorizationHelpers.AddUser)
                .Calling(c => c.Create(dto))
                .ShouldReturn()
                .Created()
                .WithModelOfType<JourneyViewDto>()
                .Passing(model => model.Employee.Id == 1 &&
                                  model.Orders.Count > 0);
        }
        
        [Fact]
        public void Post_Journey_NotValid() {
            JourneyCreationDTO dto = new JourneyCreationDTO {
                EmployeeId = 1
            };

            this.WithHttpContext(AuthorizationHelpers.AddUser)
                .Calling(c => c.Create(dto))
                .ShouldReturn()
                .BadRequest();
        }

        [Fact]
        public void Delete_Journey() {
            this.WithHttpContext(AuthorizationHelpers.AddUser)
                .Calling(c => c.Delete(1))
                .ShouldReturn()
                .Ok();
        }
    }

}

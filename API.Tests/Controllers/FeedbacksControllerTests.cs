﻿using API.Controllers;
using API.Data;
using API.DTOs.View;
using API.Extensions;
using API.Models;
using API.Tests.Helpers;
using MyTested.AspNetCore.Mvc;
using Xunit;

namespace API.Tests.Controllers {

    public class FeedbacksControllerTests : MyController<FeedbacksController> {
        public FeedbacksControllerTests() {
            this.WithDbContext(dbContext => dbContext.WithEntities(ctx => {
                var db = ctx as DatabaseContext;
                db.EnsureSeedDataForContext();
            }));
        }

        [Fact]
        public void Get_Feedback() {
            this.WithHttpContext(AuthorizationHelpers.AddUser)
                .Calling(c => c.Get(1))
                .ShouldReturn()
                .Ok()
                .WithModelOfType<FeedbackViewDto>()
                .Passing(model => model.Id == 1);
        }
        
    }

}

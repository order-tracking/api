﻿using API.Controllers;
using API.Data;
using API.DTOs.Creation;
using API.DTOs.View;
using API.Extensions;
using Microsoft.AspNetCore.Http;
using MyTested.AspNetCore.Mvc;
using Xunit;

namespace API.Tests.Controllers {

    public class AuthControllerTests : MyController<AuthController> {
        public AuthControllerTests() {
            this.WithDbContext(dbContext => dbContext.WithEntities(ctx => {
                var db = ctx as DatabaseContext;
                db.EnsureSeedDataForContext();
            }));
        }
        
        [Fact]
        public void Post_Login() {
            var dto = new EmployeeAuthDto {
                PhoneNumber = "914167554",
                Password = "password"
            };
            
            this.Calling(c => c.Login(dto))
                .ShouldReturn()
                .Ok()
                .WithModelOfType<EmployeeViewDto>()
                .Passing(model =>
                    model.Name.Equals("Nuno Reis") &&
                    model.PhoneNumber.Equals("914167554"));
        }
    }

}

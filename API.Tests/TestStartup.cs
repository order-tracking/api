﻿using API.Data.Repository;
using API.Data.Repository.Generic;
using API.Services.DistanceCalculator;
using API.Services.Hypertrack;
using API.Services.SmsSender;
using API.Tests.Services;
using API.Tests.Services.GoogleMaps;
using API.Tests.Services.Hypertrack;
using API.Tests.Services.Twilio;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.DependencyInjection;
using MyTested.AspNetCore.Mvc;

namespace API.Tests {

    public class TestStartup : Startup {
        public TestStartup(IHostingEnvironment hostingEnvironment)
            : base(hostingEnvironment) { }
        
        public void ConfigureTestServices(IServiceCollection services)
        {
            base.ConfigureServices(services);
            
            services.AddSingleton<IHypertrackApi, HypertrackMock>();
            services.AddSingleton<IDistanceCalculator, GoogleMapsMock>();
            services.AddSingleton<ISmsSender, TwilioMock>();
        }
    }

}

﻿using System;
using System.Collections.Generic;
using System.Security.Claims;
using API.Models;
using Microsoft.AspNetCore.Http;

namespace API.Tests.Helpers {

    public class AuthorizationHelpers {
        public static void AddUser(HttpContext httpContext)
        {
            AddUserToContext(1)(httpContext);
        }
        
        public static Action<HttpContext> AddUserToContext(int companyId, int userId = 1, string role = Role.Owner)
        {
            return (httpContext) =>
            {
                var claims = new List<Claim>(new[] {
                    new Claim("role", role, ClaimValueTypes.String),
                    new Claim("CompanyId", companyId.ToString(), ClaimValueTypes.String),
                    new Claim("employeeId", userId.ToString(), ClaimValueTypes.String)
                });

                var identity = new ClaimsIdentity(claims, "Bearer");
                httpContext.User = new ClaimsPrincipal(identity);
            };
        }
    }

}

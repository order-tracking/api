﻿using System;
using System.Threading.Tasks;
using API.Services.SmsSender;

namespace API.Tests.Services.Twilio
{
    public class TwilioMock : ISmsSender
    {
        public Task<bool> SendSmsAsync(string number, string message)
        {
            var res = number.Contains("+");

            return Task.FromResult(res);
        }
    }
}

﻿using System;
using System.Threading.Tasks;
using API.Models;
using API.Services.HttpService;
using API.Services.SmsSender;
using API.Tests.Services.GoogleMaps;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using Moq;
using Xunit;

namespace API.Tests.Services.Twilio
{
    public class TwilioFixture
    {
        public TwilioFixture()
        {
            IConfiguration config = new ConfigurationBuilder()
                .AddJsonFile("appsettings.json")
                .Build();

            var logger = new Mock<ILogger<HttpService>>();
            var httpService = new HttpService(logger.Object);
//            this.Api = new TwilioSmsSender(httpService, config);
            this.Api = new TwilioMock();
        }

        public ISmsSender Api { get; set; }
    }

    public class TwilioTest : IClassFixture<TwilioFixture>
    {
        private readonly TwilioFixture fixture;

        public TwilioTest(TwilioFixture fix)
        {
            this.fixture = fix;
        }

        [Fact]
        public async Task SendSms()
        {
            const string to = "+351925706007";
            const string msg = "Sms from test";
            
            var res = await fixture.Api.SendSmsAsync(to, msg);

            Assert.Equal(true, res);
        }
        
        [Fact]
        public async Task FailToSend()
        {
            const string to = "925706007";
            const string msg = "Sms from test";
            
            var res = await fixture.Api.SendSmsAsync(to, msg);

            Assert.Equal(false, res);
        }
        
    }
}

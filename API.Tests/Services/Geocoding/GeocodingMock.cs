﻿using System;
using System.Threading.Tasks;
using API.Models;
using API.Services.DistanceCalculator;
using API.Services.Geocoding;

namespace API.Tests.Services.Geocoding
{
    public class GeocodingMock : IGeocoding
    {
        public Task<GeoResult> GetCoordinatesForAddress(AddressModel address)
        {
            if (address.Address.Contains("Rua que nao existe"))
            {
                throw new Exception("Failed");
            }
            
            return Task.FromResult(new GeoResult() {
                Latitude = 38.7827406,
                Longitide = -9.1475143,
            });
        }
    }
}

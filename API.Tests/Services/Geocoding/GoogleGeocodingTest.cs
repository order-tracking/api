﻿using System;
using System.Threading.Tasks;
using API.Models;
using API.Services.DistanceCalculator;
using API.Services.Geocoding;
using API.Services.HttpService;
using API.Tests.Services.GoogleMaps;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using Moq;
using Xunit;

namespace API.Tests.Services.Geocoding
{
    public class GeocodingFixture
    {
        public GeocodingFixture()
        {
            IConfiguration config = new ConfigurationBuilder()
                .AddJsonFile("appsettings.json")
                .Build();

            var logger = new Mock<ILogger<HttpService>>();
            var httpService = new HttpService(logger.Object);
//            this.Api = new GoogleGeocodingService(httpService, config);
            this.Api = new GeocodingMock();
        }

        public IGeocoding Api { get; set; }
    }

    public class GoogleGeocodingTest : IClassFixture<GeocodingFixture>
    {
        private readonly GeocodingFixture fixture;

        public GoogleGeocodingTest(GeocodingFixture fix)
        {
            this.fixture = fix;
        }

        [Fact]
        public async Task CalculateGeocoding()
        {
            var address = new AddressModel() {
                Address = "Rua tito morais, 45",
                PostalCode = "1750",
                Country = "Portugal"
            };

            var res = await fixture.Api.GetCoordinatesForAddress(address);
            
            Assert.Equal(38.7827406, res.Latitude);
            Assert.Equal(-9.1475143, res.Longitide);
        }
        
        [Fact]
        public async Task CalculateGeocodingError()
        {
            var from = new AddressModel() {
                Address = "Rua que nao existe",
                PostalCode = "1750",
                Country = "Portugal"
            };

            await Assert.ThrowsAsync<Exception>(async () =>
            {
                var res =
                    await fixture.Api.GetCoordinatesForAddress(from);
            });
        }
    }
}

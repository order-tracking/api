﻿using System;
using System.Threading.Tasks;
using API.Models;
using API.Services.DistanceCalculator;

namespace API.Tests.Services.GoogleMaps
{
    public class GoogleMapsMock : IDistanceCalculator
    {
        public Task<DistanceResultDto> CalculateDistanceBetweenAddressesAsync(
            AddressModel address1,
            AddressModel addressModel2)
        {
            if (address1.Address.Equals("Rua que nao existe"))
            {
                throw new Exception("Test should fail");
            }
            
            return Task.FromResult(new DistanceResultDto() {
                Distance = 10378,
                Minutes = 18
            });
        }
    }
}

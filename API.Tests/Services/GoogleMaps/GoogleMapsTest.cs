﻿using System;
using System.Threading.Tasks;
using API.Models;
using API.Services.DistanceCalculator;
using API.Services.HttpService;
using API.Services.Hypertrack;
using API.Tests.Services.Hypertrack;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using Moq;
using Xunit;

namespace API.Tests.Services.GoogleMaps
{
    public class GoogleMapsFixture
    {
        public GoogleMapsFixture()
        {
            IConfiguration config = new ConfigurationBuilder()
                .AddJsonFile("appsettings.json")
                .Build();

            var logger = new Mock<ILogger<HttpService>>();
            var httpService = new HttpService(logger.Object);
//            this.Api = new GoogleMapsDistanceCalculator(httpService, config);
            this.Api = new GoogleMapsMock();
        }

        public IDistanceCalculator Api { get; set; }
    }

    public class GoogleMapsTest : IClassFixture<GoogleMapsFixture>
    {
        private readonly GoogleMapsFixture fixture;

        public GoogleMapsTest(GoogleMapsFixture fix)
        {
            this.fixture = fix;
        }

        [Fact]
        public async Task CalculateDistance()
        {
            var from = new AddressModel() {
                Address = "Rua tito morais, 45",
                PostalCode = "1750",
                Country = "Portugal"
            };
            
            var to = new AddressModel() {
                Address = "Rua Carlos Mardel, 69",
                PostalCode = "1900",
                Country = "Portugal"
            };

            var res = await fixture.Api.CalculateDistanceBetweenAddressesAsync(from, to);
            
            Assert.Equal(18, res.Minutes);
            Assert.Equal(10378, res.Distance);
        }
        
        [Fact]
        public async Task CalculateDistanceError()
        {
            var from = new AddressModel() {
                Address = "Rua que nao existe",
                PostalCode = "1750",
                Country = "Portugal"
            };
            
            var to = new AddressModel() {
                Address = "Rua Carlos Mardel, 69",
                PostalCode = "1900",
                Country = "Portugal"
            };

            await Assert.ThrowsAsync<Exception>(async () =>
            {
                var res =
                    await fixture.Api.CalculateDistanceBetweenAddressesAsync(from, to);
            });
        }
    }
}

﻿using System.Threading.Tasks;
using API.Services.HttpService;
using API.Services.Hypertrack;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using Moq;
using Xunit;

namespace API.Tests.Services.Hypertrack
{
    public class HypertrackFixture
    {
        public HypertrackFixture()
        {
            IConfiguration config = new ConfigurationBuilder()
                .AddJsonFile("appsettings.json")
                .Build();

            var logger = new Mock<ILogger<HttpService>>();
            var httpService = new HttpService(logger.Object);
//            this.Api = new HypertrackApi(config, httpService);
            this.Api = new HypertrackMock();
        }

        public IHypertrackApi Api { get; set; }
    }

    public class HypertrackTest : IClassFixture<HypertrackFixture>
    {
        private HypertrackFixture fixture;

        public HypertrackTest(HypertrackFixture fix)
        {
            this.fixture = fix;
        }

        [Fact]
        public async Task GetAllActions()
        {
            var actions = await fixture.Api.GetActions();

            Assert.Equal(actions.count, 0);
        }

        [Fact]
        public async Task DeleteAllActions()
        {
            await fixture.Api.DeleteAllActions();

            var actions = await fixture.Api.GetActions();

            Assert.Equal(actions.count, 0);
        }

        [Fact]
        public async Task CreateAndAssignAnAction()
        {
            var user = await fixture.Api.CreateUser("1", "Test User", "+351925706007");

            var action = await fixture.Api.CreateAndAssignAction("1", "order_1",
                "Rua tito morais, 45", "1750", "Portugal");

            Assert.Equal("order_1", action.lookup_id);

            await fixture.Api.DeleteAction(action.id);
        }
        
        [Fact]
        public async Task CreateAnUser()
        {
            var user = await fixture.Api.CreateUser("1", "Test User", "+351925706007");

            Assert.Equal("Test User", user.name);

            await fixture.Api.DeleteUser("1");
        }
        
        [Fact]
        public async Task DeleteAllUsers()
        {
            try
            {
                await fixture.Api.DeleteAllUsers();
            }
            catch
            {
                // ignored
            }
        }
    }
}

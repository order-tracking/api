﻿using System.Collections.Generic;
using System.Threading.Tasks;
using API.Services.Hypertrack;
using API.Services.Hypertrack.DTOs;

namespace API.Tests.Services.Hypertrack
{
    public class HypertrackMock : IHypertrackApi
    {

        public Task<UserDto> CreateUser(string lookupId, string name, string phone)
        {
            return Task.FromResult(new UserDto() {
                name = name,
                phone = phone,
                lookup_id = lookupId
            });
        }

        public Task<UserDto> GetUser(string lookupId)
        {
            return Task.FromResult(new UserDto() {
                lookup_id = lookupId
            });
        }

        public Task DeleteUser(string lookupId)
        {
            return Task.FromResult(0);
        }

        public Task DeleteAllUsers()
        {
            return Task.FromResult(0);
        }

        public Task<ActionDto> CreateAndAssignAction(
            string userLookupId,
            string orderLookupId,
            string address,
            string postalcode,
            string country)
        {
            return Task.FromResult(new ActionDto() {
                lookup_id = orderLookupId,
            });
        }

        public Task DeleteAction(string lookupId)
        {
            return Task.FromResult(0);
        }

        public Task<HyperCollection<ActionDto>> GetActions()
        {
            return Task.FromResult(new HyperCollection<ActionDto>() {
                count = 0,
                results = new List<ActionDto>()
            });
        }

        public Task DeleteAllActions()
        {
            return Task.FromResult(0);
        }
    }
}

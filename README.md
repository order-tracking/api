# oTrack API
[![build status](https://gitlab.com/order-tracking/api/badges/master/build.svg)](https://gitlab.com/order-tracking/api/commits/master)

ASP.NET Core Api.

Continuous Delivery is active for this repository.
When pushing on the:
- `master` branch:
    - The app is deployed to `https://api.otrack.info` (Production)
- `develop` branch:
    - The app is deployed to `https://api-staging.otrack.info` (Staging)

See the Pipelines for more details when pushing.


## Requirements
- dotnet core runtime [download](https://www.microsoft.com/net/download/core)
- Postgres

## Database Connection
Add an environment variable with the name:

- Windows:
  - `oTrack:Database:PostgresConnection`
- Unix based: (: char is not allowed)
  - `oTrack__Database__PostgresConnection`

Value: `User ID=username;Password=password;Host=localhost;Port=5432;Database=otrack`

## Instructions
- `API` directory
- `dotnet ef database update` - Runs the migrations
- `dotnet run` - builds and run

## Tests (No db needed, uses in memory db)
- `Api.Tests` directory
- `dotnet test` - builds and runs all the tests

## Stack
- .NET Core 1.1 (MVC)
- EF Core 1.1
- Postgres DB

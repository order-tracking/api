FROM microsoft/aspnetcore:1.1
COPY /API/bin/Release/PublishOutput/ /api
ENV ASPNETCORE_ENVIRONMENT=Production
ENV ASPNETCORE_URLS http://*:5000
WORKDIR /api
EXPOSE 5000
CMD ["dotnet","API.dll"]

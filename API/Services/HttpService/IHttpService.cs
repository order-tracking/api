﻿using System.Net.Http;
using System.Threading.Tasks;

namespace API.Services.HttpService
{
    public interface IHttpService
    {
        Task<HttpResponseMessage> SendAsync(HttpRequestMessage request);
        
        Task<T> SendAsync<T>(HttpRequestMessage request);

        HttpClient GetClient();
    }
}

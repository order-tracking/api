﻿using System;
using System.Net.Http;
using System.Threading.Tasks;
using Cimpress.Extensions.Http.MessageHandlers;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;

namespace API.Services.HttpService
{
    // This service is inject through DI with Singleton lifetime
    // This ensures one HttpClient for the app lifetime
    // Because of this https://aspnetmonsters.com/2016/08/2016-08-27-httpclientwrong/
    // Also official docs:
    // "HttpClient is intended to be instantiated once and re-used throughout the life of an application."
    // https://docs.microsoft.com/en-us/dotnet/api/system.net.http.httpclient?view=netcore-1.1
    public class HttpService : IHttpService, IDisposable
    {
        private readonly ILogger<HttpService> _logger;
        private readonly HttpClient _httpClient;

        public HttpService(ILogger<HttpService> logger)
        {
            _logger = logger;

            // Adds logging to external requests
            _httpClient = new HttpClient(new MeasurementHandler(_logger));
            _httpClient.Timeout = TimeSpan.FromSeconds(40);
        }

        /// <summary>
        /// Sends the request and returns the raw response
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        public Task<HttpResponseMessage> SendAsync(HttpRequestMessage request)
        {
            return _httpClient.SendAsync(request);
        }
        
        /// <summary>
        /// Sends the request and deserializes the response body
        /// </summary>
        /// <param name="request"></param>
        /// <typeparam name="T"></typeparam>
        /// <returns></returns>
        public async Task<T> SendAsync<T>(HttpRequestMessage request)
        {
            var res = await _httpClient.SendAsync(request);
            
            var body = await res.Content.ReadAsStringAsync();

            return JsonConvert.DeserializeObject<T>(body);
        }

        /// <summary>
        /// Getter for the HttpClient instance
        /// </summary>
        /// <returns></returns>
        public HttpClient GetClient()
        {
            return _httpClient;
        }

        public void Dispose()
        {
            _httpClient?.Dispose();
        }
    }
}

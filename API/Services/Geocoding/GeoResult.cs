﻿namespace API.Services.Geocoding
{
    public class GeoResult
    {
        public double Latitude { get; set; }
        public double Longitide { get; set; }
    }
}

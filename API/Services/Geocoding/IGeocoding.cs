﻿using System.Threading.Tasks;
using API.Models;
using API.Services.DistanceCalculator;

namespace API.Services.Geocoding
{
    public interface IGeocoding
    {
        Task<GeoResult> GetCoordinatesForAddress(
            AddressModel address);
    }
}

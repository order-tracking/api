﻿using System;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using API.Models;
using API.Services.DistanceCalculator;
using API.Services.HttpService;
using Microsoft.Extensions.Configuration;

namespace API.Services.Geocoding
{
    public class GoogleGeocodingService : IGeocoding
    {
        private readonly IHttpService _httpService;
        private readonly string _api;

        private const string Url = "https://maps.googleapis.com/maps/api/geocode/json?address={0}&key={1}";

        public GoogleGeocodingService(
            IHttpService httpService,
            IConfiguration config)
        {
            _httpService = httpService;
            _api = config["GoogleApiGeocoding"];
        }

        public async Task<GeoResult> GetCoordinatesForAddress(AddressModel address)
        {
            var addr = WebUtility.UrlEncode(address.Address + " " + address.PostalCode + ", " + address.Country);

            var finalUrl = string.Format(Url, addr, _api);

            var request = new HttpRequestMessage {RequestUri = new Uri(finalUrl)};

            var dto = await _httpService.SendAsync<GoogleGeocodeDto>(request);
            
            if (!dto.status.Equals("OK"))
            {
                throw new Exception("Invalid request sent.");
            }
            
            if (dto.results.Count == 0)
            {
                throw new Exception("Address is invalid or not found.");
            }
            
            var row = dto.results[0]; // we want the first row only
            
            return new GeoResult() {
                Latitude = row.geometry.location.lat,
                Longitide = row.geometry.location.lng,
            };
        }
    }
}

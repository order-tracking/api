﻿using System.Threading.Tasks;

namespace API.Services.SmsSender
{
    public interface ISmsSender
    {
        Task<bool> SendSmsAsync(string number, string message);
    }
}

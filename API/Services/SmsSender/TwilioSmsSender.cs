﻿using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Threading.Tasks;
using API.Services.HttpService;
using Microsoft.Extensions.Configuration;

namespace API.Services.SmsSender
{
    public class TwilioSmsSender : ISmsSender
    {
        private readonly IHttpService _httpService;
        private readonly string _sid;
        private readonly string _token;
        private readonly string _from;

        private const string Url =
            "https://api.twilio.com/2010-04-01/Accounts/{0}/Messages.json";

        public TwilioSmsSender(IHttpService httpService, IConfiguration config)
        {
            _httpService = httpService;
            _sid = config["Twillio:SID"];
            _token = config["Twillio:Token"];
            _from = config["Twillio:From"];
        }

        public async Task<bool> SendSmsAsync(string number, string message)
        {
            if (message.Length > 1600)
            {
                throw new Exception("Twilio body sms only accept < 1600 chars.");
            }

            var finalUrl = string.Format(Url, _sid);
            var request = new HttpRequestMessage {
                RequestUri = new Uri(finalUrl),
                Method = HttpMethod.Post
            };
            request.Headers.Authorization =
                new BasicAuthenticationHeaderValue(_sid, _token);

            var keyValues = new List<KeyValuePair<string, string>> {
                new KeyValuePair<string, string>("To", number),
                new KeyValuePair<string, string>("From", _from),
                new KeyValuePair<string, string>("Body", message)
            };

            request.Content = new FormUrlEncodedContent(keyValues);

            var res = await _httpService.SendAsync<TwilioResDto>(request);

            return res.status.Equals("queued");
        }
    }
}

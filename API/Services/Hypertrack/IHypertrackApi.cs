﻿using System.Threading.Tasks;
using API.Services.Hypertrack.DTOs;

namespace API.Services.Hypertrack
{
    public interface IHypertrackApi
    {
        /*
        |-----------------------------------------------------------------------
        | Users
        |-----------------------------------------------------------------------
        */
        Task<UserDto> CreateUser(string lookupId, string name, string phone);

        Task<UserDto> GetUser(string lookupId);

        Task DeleteUser(string lookupId);
        
        Task DeleteAllUsers();

        /*
        |-----------------------------------------------------------------------
        | Actions
        |-----------------------------------------------------------------------
        */
        
        Task<ActionDto> CreateAndAssignAction(string userLookupId,
                                              string orderLookupId,
                                              string address,
                                              string postalcode,
                                              string country);
        
        Task DeleteAction(string lookupId);
        
        Task<HyperCollection<ActionDto>> GetActions();
        
        Task DeleteAllActions();
    }
}

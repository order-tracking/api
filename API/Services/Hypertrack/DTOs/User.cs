﻿using System;

namespace API.Services.Hypertrack.DTOs
{
    public class UserDto
    {
        public string id { get; set; }
        public string name { get; set; }
        public string phone { get; set; }
        public string lookup_id { get; set; }
        public string availability_status { get; set; }
        public string location_status { get; set; }
        public bool is_connected { get; set; }
        public string[] pending_actions { get; set; }
        public DateTime? last_heartbeat_at { get; set; }
        public DateTime? last_online_at { get; set; }
        public int? last_battery { get; set; }
    }
    
    public class AssignActionsDto
    {
        public string[] action_ids { get; set; }
    }
    
    public class UserCreationDto
    {
        public string name { get; set; }
        public string phone { get; set; }
        public string lookup_id { get; set; }
    }
}

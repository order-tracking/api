﻿using System;

namespace API.Services.Hypertrack.DTOs
{
    public class ActionDto
    {
        public string id { get; set; }
        public UserDto user { get; set; }
        public string type { get; set; }
        public string lookup_id { get; set; }
        public DateTime? assigned_at { get; set; }
        public DateTime? scheduled_at { get; set; }
        public string status { get; set; }
        public string sub_status { get; set; }
        public string tracking_url { get; set; }
        public DateTime? initial_eta { get; set; }
        public DateTime? eta { get; set; }
        public DateTime? completed_at { get; set; }
        public DateTime? suspended_at { get; set; }
        public DateTime? canceled_at { get; set; }
        public DateTime? modified_at { get; set; }
        public DateTime created_at { get; set; }
    }

    public class ActionCreationDto
    {
        public PlaceCreationDto expected_place { get; set; }
        public string type { get; set; }
        public string lookup_id { get; set; }
        public string user_id { get; set; }
    }
}

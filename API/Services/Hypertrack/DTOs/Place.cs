﻿using System;

namespace API.Services.Hypertrack.DTOs
{
    public class PlaceDto
    {
        public string id { get; set; }
        public string name { get; set; }
        public string address { get; set; }
        public string landmark { get; set; }
        public string zip_code { get; set; }
        public string city { get; set; }
        public string state { get; set; }
        public string country { get; set; }
        public DateTime created_at { get; set; }
        public DateTime? modified_at { get; set; }
    }

    public class PlaceCreationDto
    {
        public string address { get; set; }
        public string zip_code { get; set; }
        public string city { get; set; }
        public string country { get; set; }
    }
}

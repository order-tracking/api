﻿using System.Collections.Generic;

namespace API.Services.Hypertrack.DTOs
{
    public class HyperCollection<T>
    {
        public int count { get; set; }
        public string next { get; set; }
        public string previous { get; set; }
        public List<T> results { get; set; }
    }
}

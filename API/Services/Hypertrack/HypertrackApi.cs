﻿using System;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using API.Extensions;
using API.Services.HttpService;
using API.Services.Hypertrack.DTOs;
using Microsoft.Extensions.Configuration;

namespace API.Services.Hypertrack
{
    public class HypertrackApi : IHypertrackApi
    {
        private const string Url = "https://api.hypertrack.com/api/v1";
        private readonly IHttpService _httpService;
        private readonly IConfiguration _config;

        public HypertrackApi(IConfiguration config, IHttpService httpService)
        {
            this._httpService = httpService;
            this._config = config;
        }

        /*
        |-----------------------------------------------------------------------
        | Users
        |-----------------------------------------------------------------------
        */
        public async Task<UserDto> CreateUser(string lookupId, string name, string phone)
        {
            var dto = new UserCreationDto() {
                lookup_id = lookupId,
                name = name,
                phone = phone
            };

            return await PostRequest<UserDto>("/users/", dto.ToJson());
        }

        public async Task DeleteUser(string lookupId)
        {
            var hyperUser = await GetUser(lookupId);

            await DeleteRequest($"/users/{hyperUser.id}/");
        }

        public Task DeleteAllUsers()
        {
            return DeleteRequest("/users/bulk_delete/");
        }

        public async Task<UserDto> GetUser(string lookupId)
        {
            var id = lookupId;
            var list =
                await GetRequest<HyperCollection<UserDto>>($"/users/?lookup_id={id}");

            return list.results[0];
        }

        /*
        |-----------------------------------------------------------------------
        | Actions
        |-----------------------------------------------------------------------
        */
        public async Task<ActionDto> CreateAndAssignAction(
            string userLookupId,
            string orderLookupId,
            string address,
            string postalcode,
            string country)
        {
            var hyperUser = await this.GetUser(userLookupId);
            var dto = new ActionCreationDto() {
                expected_place = new PlaceCreationDto() {
                    address = address,
                    zip_code = postalcode,
                    country = country
                },
                lookup_id = orderLookupId,
                user_id = hyperUser.id,
                type = "delivery"
            };

            return await PostRequest<ActionDto>("/actions/", dto.ToJson());
        }

        public Task<HyperCollection<ActionDto>> GetActions()
        {
            return GetRequest<HyperCollection<ActionDto>>("/actions/?page_size=50");
        }

        public async Task DeleteAction(string actionId)
        {
            await DeleteRequest("/actions/" + actionId);
        }

        public async Task DeleteAllActions()
        {
            var actions = await GetActions();

            foreach (var action in actions.results)
            {
                await DeleteAction(action.id);
            }
        }

        /*
        |-----------------------------------------------------------------------
        | Request utils
        |-----------------------------------------------------------------------
        */
        private HttpRequestMessage CreateRequest(string path)
        {
            var request = new HttpRequestMessage {RequestUri = new Uri(Url + path)};
            request.Headers.Add("Authorization", "token " + _config["Hypertrack_Token"]);

            return request;
        }

        private Task<T> GetRequest<T>(string path)
        {
            var request = this.CreateRequest(path);

            return _httpService.SendAsync<T>(request);
        }

        private Task<T> PostRequest<T>(string path, string jsonBody)
        {
            var request = this.CreateRequest(path);
            request.Method = HttpMethod.Post;

            request.Content =
                new StringContent(jsonBody, Encoding.UTF8, "application/json");

            return _httpService.SendAsync<T>(request);
        }

        private Task DeleteRequest(string path)
        {
            var request = this.CreateRequest(path);
            request.Method = HttpMethod.Delete;

            return _httpService.SendAsync(request);
        }
    }
}

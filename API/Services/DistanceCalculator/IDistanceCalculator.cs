﻿using System.Threading.Tasks;
using API.Models;

namespace API.Services.DistanceCalculator
{
    public interface IDistanceCalculator
    {
        Task<DistanceResultDto> CalculateDistanceBetweenAddressesAsync(
            AddressModel address1,
            AddressModel addressModel2);
    }
}

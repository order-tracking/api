﻿namespace API.Services.DistanceCalculator
{
    public class DistanceResultDto
    {
        public int Distance { get; set; }
        public int Minutes { get; set; }
    }
}

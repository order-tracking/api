﻿using System;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using API.Models;
using API.Services.HttpService;
using Microsoft.Extensions.Configuration;

namespace API.Services.DistanceCalculator
{
    public class GoogleMapsDistanceCalculator : IDistanceCalculator
    {
        private readonly IHttpService _httpService;
        private readonly string _api;

        private const string Url = "https://maps.googleapis.com/maps/api/distancematrix/json?origins={0}&destinations={1}&key={2}&region={3}";

        public GoogleMapsDistanceCalculator(
            IHttpService httpService,
            IConfiguration config)
        {
            _httpService = httpService;
            _api = config["GoogleApiMatrixApi"];
        }

        public async Task<DistanceResultDto> CalculateDistanceBetweenAddressesAsync(
            AddressModel from,
            AddressModel to)
        {
            var fromEncoded = WebUtility.UrlEncode(from.Address);
            var toEncoded = WebUtility.UrlEncode(to.Address);

            var finalUrl = string.Format(Url, fromEncoded, toEncoded, _api, from.Country);

            var request = new HttpRequestMessage {RequestUri = new Uri(finalUrl)};

            var dto = await _httpService.SendAsync<GoogleResponseDto>(request);

            if (!dto.status.Equals("OK"))
            {
                throw new Exception("Invalid request sent.");
            }

            if (dto.rows.Count == 0)
            {
                throw new Exception("Address are invalid or not found.");
            }

            var row = dto.rows[0]; // we want the first row only

            // First element with OK
            var element = row.elements.Find(e => e.status == "OK");

            if (element == null)
            {
                throw new Exception("No valid path was found.");
            }
            
            var result = new DistanceResultDto() {
                Distance = element.distance.value,
                Minutes = (int) Math.Round(element.duration.value / 60.0)
            };
            
            return result;
        }
    }
}

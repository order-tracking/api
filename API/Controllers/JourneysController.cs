﻿using System;
using System.Linq;
using System.Threading.Tasks;
using API.Controllers.Common;
using API.Data.Mapper;
using API.Data.Repository.Generic;
using API.DTOs.Creation;
using API.DTOs.Edition;
using API.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using X.PagedList;

namespace API.Controllers
{
    [Route("[controller]")]
    [Authorize(Roles = Roles.EmployeePermission)]
    public class JourneysController : AuthorizationController
    {
        private readonly IJourneysRepository _repo;

        public JourneysController(IJourneysRepository repo)
        {
            _repo = repo;
        }

        [AllowAnonymous]
        [HttpGet("", Name = Routes.GetJourneys)]
        public async Task<IActionResult> GetList(
            [FromQuery] int page = 1,
            [FromQuery] int limit = 10,
            [FromQuery] string search = null)
        {
            IPagedList<Journey> journeys = null;

            if (search != null)
            {
                var tmp = search.ToLower();
                if (tmp.Equals("complete"))
                    journeys =
                        await _repo.GetCompletePaginatedJourneys(GetCompanyId(), page,
                            limit);
                if (tmp.Equals("incomplete"))
                    journeys =
                        await _repo.GetIncompletePaginatedJourneys(GetCompanyId(), page,
                            limit);
            }

            if (journeys == null)
                journeys = await _repo.GetPaginatedJourneys(GetCompanyId(), page, limit);

            var view = journeys.Select(JourneyMapper.Map).ToList();

            var paginatedListView = PaginationMapper.Map(
                view,
                page,
                limit,
                journeys.TotalItemCount,
                journeys.HasNextPage,
                journeys.HasPreviousPage
            );

            return Ok(paginatedListView);
        }

        [HttpGet("{employeeId}/incomplete")]
        [Authorize(Roles = Roles.DistributorPermission)]
        public async Task<IActionResult> GetIncompleteEmployeeJourneys(int employeeId)
        {
            var journeys = await _repo.GetIncompleteEmployeeJourneys(employeeId);

            return Ok(journeys.Select(JourneyMapper.Map).ToList());
        }

        [HttpGet("{id}", Name = Routes.GetJourney)]
        [Authorize(Roles = Roles.DistributorPermission)]
        public async Task<IActionResult> GetById(int id)
        {
            var journey = await _repo.GetJourney(id);

            if (journey == null)
            {
                return NotFound();
            }

            return Ok(JourneyMapper.Map(journey));
        }

        [HttpPost("", Name = Routes.CreateJourney)]
        public async Task<IActionResult> Create([FromBody] JourneyCreationDTO dto)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var journey = JourneyMapper.Map(dto);
            journey.CompanyId = GetCompanyId();
            journey.CreatedAt = DateTime.UtcNow;

            var entity = await _repo.Create(journey);

            if (entity == null)
            {
                return CreationFailed();
            }

            if (entity.Employee == null)
            {
                // Fetch full journey details
                entity = await _repo.GetJourney(entity.Id);
            }

            var view = JourneyMapper.Map(await _repo.GetJourney(entity.Id));

            return CreatedAtRoute(Routes.GetJourney, new {id = view.Id}, view);
        }

        [HttpPut("{id}", Name = Routes.EditJourney)]
        public async Task<IActionResult> Edit(int id, [FromBody] JourneyEditionDto dto)
        {
            if (!ModelState.IsValid) {
                return BadRequest(ModelState);
            }
            
            if (dto == null || dto.Id != id)
            {
                return BadRequest();
            }

            var journey = await _repo.Get(dto.Id);
            if (journey == null)
            {
                return NotFound();
            }

            journey.Change(dto);
            var success = await _repo.Update(journey);

            return success ? NoContent() : EditionFailed();
        }

        [HttpDelete("{id}", Name = Routes.DeleteJourney)]
        public async Task<IActionResult> Delete(int id)
        {
            var entity = await _repo.Get(id);
            if (entity == null)
            {
                return NotFound();
            }

            if (await _repo.Delete(entity))
            {
                return Ok();
            }

            return DeletionFailed();
        }
    }
}

using System.Threading.Tasks;
using API.Controllers.Common;
using API.Data.Mapper;
using API.Data.Repository.Generic;
using API.DTOs.Creation;
using API.DTOs.Edition;
using API.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace API.Controllers
{
    [Route("[controller]")]
    [Authorize(Roles = Roles.OwnerPermission)]
    public class CompaniesController : AuthorizationController
    {
        private readonly ICompaniesRepository _repo;

        public CompaniesController(ICompaniesRepository repo)
        {
            _repo = repo;
        }

        [HttpGet("{id}", Name = Routes.GetCompany)]
        public async Task<IActionResult> GetById(int id)
        {
            var entity = await _repo.Get(id);

            if (entity == null)
                return NotFound();

            return Ok(CompanyMapper.Map(entity));
        }

        [HttpPost("", Name = Routes.CreateCompany)]
        [Authorize(Roles = Roles.DeveloperPermission)]
        public async Task<IActionResult> Create([FromBody] CompanyCreationDTO dto)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var company = new Company {Name = dto.Name};

            var entity = await _repo.Create(company);

            if (entity == null)
            {
                return CreationFailed();
            }

            return CreatedAtRoute(Routes.GetCompany, new {id = entity.Id}, entity);
        }

        [HttpPut("{id}", Name = Routes.EditCompany)]
        public async Task<IActionResult> Edit(int id, [FromBody] CompanyEditionDto dto)
        {
            var entity = await _repo.Get(id);
            if (entity == null)
            {
                return NotFound();
            }

            entity.Change(dto);
            var success = await _repo.Update(entity);

            return success ? NoContent() : EditionFailed();
        }
    }
}

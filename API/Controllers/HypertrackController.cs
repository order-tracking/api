﻿using System;
using System.Threading.Tasks;
using API.Data.Repository.Generic;
using API.DTOs.HypertrackEvents;
using API.Events;
using API.Models;
using MediatR;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;

namespace API.Controllers {

    [AllowAnonymous]
    [Route("hypertrack")]
    public class HypertrackController : Controller {
        private readonly ICustomerRepository _customerRepository;
        private readonly IOrdersRepository _ordersRepository;
        private readonly IMediator _mediator;
        private readonly ILogger<HypertrackController> _logger;
        private readonly IConfiguration _config;

        public HypertrackController(
            ICustomerRepository customerRepository,
            IOrdersRepository ordersRepository,
            IMediator mediator,
            ILogger<HypertrackController> logger,
            IConfiguration config) {
            _customerRepository = customerRepository;
            _ordersRepository = ordersRepository;
            _mediator = mediator;
            _logger = logger;
            _config = config;
        }

        /// <summary>
        /// Receive the Hypertrack webhooks and dispatch the right domain events
        /// </summary>
        /// <param name="dto"></param>
        /// <returns></returns>
        [HttpPost("webhook", Name = Routes.HypertrackWebhook)]
        public async Task<IActionResult> Webhook([FromBody] HypertrackEventDto dto) {
            try {
                _logger.LogInformation("Received Webhook of type: " + dto.type);
                switch (dto.type) {
                    // Distributor starting the delivery  
                    case "action.leaving_now":
                        var order = await GetOrder(dto);
                        await _mediator.Publish(
                            new DistributorIsOnTheWayEvent(order, order.Customer, dto));
                        break;
                        
                    // Distributor arriving    
                    case "action.arriving":
                        var order1 = await GetOrder(dto);
                        await _mediator.Publish(
                            new DistributorIsArrivingEvent(order1, order1.Customer, dto));
                        break;
                        
                    // Delivery completed
                    case "action.completed":
                    case "action.completed_at_different_place_than_expected":
                        var order2 = await GetOrder(dto);
                        await _mediator.Publish(
                            new OrderCompletedEvent(order2, dto));
                        break;
                        
                    // Delivery ETA changed
                    case "action.delayed":
                    case "action.on_time":
                    case "action.on_the_way":
                        var order3 = await GetOrder(dto);
                        await _mediator.Publish(
                            new DeliveryTimeChangedEvent(order3, dto));
                        break;
                        
                    // Distributor is ready to work
                    case "tracking.started":
                    case "tracking.ended":
                        await _mediator.Publish(
                            new EmployeeTrackingChangedEvent(dto));
                        break;
                }
            }
            catch (Exception e) {
                _logger.LogError(e.StackTrace);
            }

            return Ok(); // hypertrack needs 200 status code
        }

        [HttpGet("key", Name = Routes.HypertrackSK)]
        [Authorize(Roles = Roles.AdminPermission)]
        public IActionResult GetHypertrackSk() {
            return Ok(
                new {
                    SK = _config["Hypertrack_Token"]
                }
            );
        }

        private async Task<Order> GetOrder(HypertrackEventDto dto)
        {
            var orderId = int.Parse(dto.data.@object.lookup_id);
            return await _ordersRepository.Get(orderId);
        }
    }

}

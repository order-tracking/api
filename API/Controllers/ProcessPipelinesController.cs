using System;
using System.Linq;
using System.Threading.Tasks;
using API.Controllers.Common;
using API.Data.Mapper;
using API.Data.Repository.Generic;
using API.DTOs.Creation;
using API.DTOs.Edition;
using API.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace API.Controllers {

    [Route("[controller]")]
    [Authorize(Roles = Roles.EmployeePermission)]
    public class ProcessPipelinesController : AuthorizationController {
        private readonly IProcessPipelineRepository _repo;
        private readonly IEmployeeRepository _employeeRepository;

        public ProcessPipelinesController(
            IProcessPipelineRepository repo,
            IEmployeeRepository employeeRepository) {
            _repo = repo;
            _employeeRepository = employeeRepository;
        }

        [HttpGet("", Name = Routes.GetProcessPipelines)]
        public async Task<IActionResult> GetList(
            [FromQuery] int page = 1,
            [FromQuery] int limit = 10) {
            var processPipelines =
                await _repo.GetPaginatedPipelinesOfCompany(GetCompanyId(), page, limit);

            var listView = processPipelines.Select(ProcessPipelineMapper.Map).ToList();

            var view = PaginationMapper.Map(
                listView,
                page,
                limit,
                processPipelines.TotalItemCount,
                processPipelines.HasNextPage,
                processPipelines.HasPreviousPage
            );

            return Ok(view);
        }

        [HttpGet("{id}", Name = Routes.GetProcessPipeline)]
        public async Task<IActionResult> GetById(int id) {
            var pipeline = await _repo.Get(id);

            if (pipeline == null) {
                return NotFound();
            }

            // Update working user
            var employee = await _employeeRepository.Get(GetUserId());
            employee.LastTimeOnline = DateTime.UtcNow;
            await _employeeRepository.Update(employee);

            return Ok(ProcessPipelineMapper.Map(pipeline));
        }

        [HttpGet("processes/{processId}/items", Name = Routes.GetProcessPipelineItems)]
        public async Task<IActionResult> GetItemsFromProcess(int processId) {
            var items = await _repo.GetProcessItems(processId);

            return Ok(items.Select(ItemMapper.Map));
        }

        [HttpPost("", Name = Routes.CreateProcessPipeline)]
        [Authorize(Roles = Roles.AdminPermission)]
        public async Task<IActionResult> Create([FromBody] ProcessPipelineCreationDTO dto) {
            if (!ModelState.IsValid) {
                return BadRequest(ModelState);
            }

            var pipeline = ProcessPipelineMapper.Map(dto);
            pipeline.CompanyId = GetCompanyId();
            pipeline.Processes.ForEach(p => p.CompanyId = GetCompanyId());

            var result = await _repo.Create(pipeline);

            if (result == null) {
                return CreationFailed();
            }

            var view = ProcessPipelineMapper.Map(await _repo.Get(result.Id));

            return CreatedAtRoute(Routes.GetProcessPipeline, new {id = view.Id}, view);
        }

        [HttpPut("{id}", Name = Routes.EditProcessPipeline)]
        [Authorize(Roles = Roles.AdminPermission)]
        public async Task<IActionResult> Edit(int id, [FromBody] ProcessPipelineEditionDto dto) {
            if (!ModelState.IsValid) {
                return BadRequest(ModelState);
            }

            if (dto == null || dto.Id != id) {
                return BadRequest();
            }

            var entity = await _repo.Get(id);
            if (entity == null) {
                return NotFound();
            }

            entity.Change(dto);
            var success = await _repo.Update(entity);

            return success ? NoContent() : EditionFailed();
        }

        [HttpDelete("{id}", Name = Routes.DeleteProcessPipeline)]
        [Authorize(Roles = Roles.AdminPermission)]
        public async Task<IActionResult> Delete(int id) {
            var entity = await _repo.Get(id);
            if (entity == null) {
                return NotFound();
            }

            if (await _repo.Delete(entity)) {
                return Ok();
            }

            return DeletionFailed();
        }
    }

}
using System;
using System.Linq;
using System.Threading.Tasks;
using API.Controllers.Common;
using API.Data.Mapper;
using API.Data.Repository.Generic;
using API.DTOs.Creation;
using API.DTOs.Edition;
using API.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace API.Controllers {

    [Route("[controller]")]
    [Authorize(Roles = Roles.EmployeePermission)]
    public class ProductGroupsController : AuthorizationController {
        private readonly IProductGroupsRepository _repo;

        public ProductGroupsController(IProductGroupsRepository repo) {
            _repo = repo;
        }

        [HttpGet("first", Name = Routes.GetFirstProductGroups)]
        public async Task<IActionResult> GetFirstProductGroups() {
            Console.WriteLine(GetCompanyId());
            var productGroups = await _repo.GetFirstProductGroups(GetCompanyId());

            var listView = productGroups.Select(ProductGroupMapper.Map).ToList();

            return Ok(listView);
        }

        [HttpGet("{id}/next", Name = Routes.GetNextProductGroups)]
        public async Task<IActionResult> GetNextProductGroups(int id) {
            var productGroups = await _repo.GetNextProductGroups(id);

            if (productGroups == null) {
                return NotFound();
            }

            var listView = productGroups.Select(ProductGroupMapper.Map).ToList();

            return Ok(listView);
        }

        [HttpGet("{id}", Name = Routes.GetProductGroup)]
        public async Task<IActionResult> GetById(int id) {
            var productGroup = await _repo.Get(id);

            if (productGroup == null) {
                return NotFound();
            }

            return Ok(ProductGroupMapper.Map(productGroup));
        }

        [HttpGet("{id}/products", Name = Routes.GetProductGroupProducts)]
        public async Task<IActionResult> GetProducts(int id) {
            var entities = await _repo.GetProducts(id);

            return Ok(entities);
        }

        [HttpPost("", Name = Routes.CreateProductGroup)]
        public async Task<IActionResult> Create([FromBody] ProductGroupCreationDTO dto) {
            if (!ModelState.IsValid) {
                return BadRequest(ModelState);
            }

            var productGroup = ProductGroupMapper.Map(dto);
            productGroup.CompanyId = GetCompanyId();

            var entity = await _repo.Create(productGroup);

            if (entity == null) {
                return CreationFailed();
            }

            var view = ProductGroupMapper.Map(await _repo.Get(entity.Id));

            return CreatedAtRoute(Routes.GetProductGroup, new {id = view.Id}, view);
        }

        [HttpPut("{id}", Name = Routes.EditProductGroup)]
        public async Task<IActionResult> Edit(int id, ProductGroupEditionDto dto) {
            if (!ModelState.IsValid) {
                return BadRequest(ModelState);
            }

            if (dto == null || dto.Id != id) {
                return BadRequest();
            }

            var entity = await _repo.Get(id);
            if (entity == null) {
                return NotFound();
            }

            entity.Change(dto);
            var success = await _repo.Update(entity);

            return success ? NoContent() : EditionFailed();
        }

        [HttpDelete("{id}", Name = Routes.DeleteProductGroup)]
        public async Task<IActionResult> Delete(int id) {
            var entity = await _repo.Get(id);
            if (entity == null) {
                return NotFound();
            }

            if (await _repo.Delete(entity)) {
                return Ok();
            }

            return DeletionFailed();
        }
    }

}
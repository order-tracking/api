using System;
using System.Linq;
using System.Threading.Tasks;
using API.Controllers.Common;
using API.Data.Mapper;
using API.Data.Repository.Generic;
using API.DTOs.Creation;
using API.Events;
using API.Models;
using MediatR;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace API.Controllers
{
    [Route("[controller]")]
    [Authorize(Roles = Roles.AdminPermission)]
    public class CampaignsController : AuthorizationController
    {
        private readonly ICampaignRepository _repo;
        private readonly IMediator _mediator;

        public CampaignsController(ICampaignRepository repo, IMediator mediator)
        {
            _repo = repo;
            _mediator = mediator;
        }

        [HttpGet("", Name = Routes.GetCampaigns)]
        public async Task<IActionResult> GetList(
            [FromQuery] int page = 1,
            [FromQuery] int limit = 10)
        {
            var campaigns =
                await _repo.GetPaginated(GetCompanyId(), page, limit);

            var view = campaigns.Select(CampaignMapper.Map).ToList();

            var paginatedView = PaginationMapper.Map(
                view,
                page,
                limit,
                campaigns.TotalItemCount,
                campaigns.HasNextPage,
                campaigns.HasPreviousPage
            );

            return Ok(paginatedView);
        }

        [HttpGet("{id}", Name = Routes.GetCampaign)]
        public async Task<IActionResult> GetById(int id)
        {
            var entity = await _repo.Get(id);

            if (entity == null)
                return NotFound();

            return Ok(CampaignMapper.Map(entity));
        }

        [HttpPost("", Name = Routes.CreateCampaign)]
        public async Task<IActionResult> Create([FromBody] CampaignCreationDTO dto)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var campaign = new Campaign {
                Title = dto.Title,
                Message = dto.Message,
                CreatedAt = DateTime.UtcNow,
                CompanyId = GetCompanyId()
            };

            var entity = await _repo.Create(campaign);

            if (entity == null)
            {
                return CreationFailed();
            }

            // maybe is better to not await and let this run in the background and 
            // return a 202 ACCEPTED
            await _mediator.Publish(new SendNewCampaignEvent(campaign));

            return CreatedAtRoute(Routes.GetCampaign, new {id = entity.Id}, CampaignMapper.Map(entity));
        }

    }
}

﻿using System;
using System.Threading.Tasks;
using API.Controllers.Common;
using API.Data.Repository.Generic;
using API.DTOs.Creation;
using API.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Caching.Memory;

namespace API.Controllers {

    [Route("[controller]")]
    [Authorize(Roles = Roles.AdminPermission)]
    public class ReportsController : AuthorizationController {
        private readonly IReportsRepository _reportsRepository;
        private readonly IMemoryCache _cache;

        public ReportsController(IReportsRepository reportsRepository, IMemoryCache cache) {
            _reportsRepository = reportsRepository;
            _cache = cache;
        }

        /// <summary>
        /// Get reports between a given timeframe
        /// Reports are cached in memory for 1 hour because this is an expensive 
        /// operation
        /// </summary>
        /// <param name="dto"></param>
        /// <returns></returns>
        [HttpGet("", Name = Routes.GetReports)]
        public async Task<IActionResult> GetReport([FromQuery] ReportDto dto) {
            if (!ModelState.IsValid) {
                return BadRequest(ModelState);
            }

            var from = DateTime.UtcNow.AddDays(dto.Days * -1);
            var to = DateTime.UtcNow;

            var key = GenerateKey(
                from,
                to
            );

            // Look for the report in the cache
            Report cacheEntry;
            if (!_cache.TryGetValue(key, out cacheEntry)) {
                // Key not in cache, so get data.
                cacheEntry = await _reportsRepository.GetReportBetween(
                    GetCompanyId(),
                    from,
                    to
                );

                // 1h expiration
                var cacheEntryOptions = new MemoryCacheEntryOptions()
                    .SetSlidingExpiration(TimeSpan.FromHours(1));

                // Save data in cache.
                _cache.Set(key, cacheEntry, cacheEntryOptions);
            }


            return Ok(cacheEntry);
        }

        /// <summary>
        /// Cache Key
        /// </summary>
        /// <param name="from"></param>
        /// <param name="to"></param>
        /// <returns></returns>
        private static string GenerateKey(DateTime from, DateTime to) {
            return string.Format("{0}:{1}", from, to);
        }
    }

}
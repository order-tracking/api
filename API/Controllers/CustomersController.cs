using System.Linq;
using System.Threading.Tasks;
using API.Controllers.Common;
using API.Data.Mapper;
using API.Data.Repository.Generic;
using API.DTOs.Creation;
using API.DTOs.Edition;
using API.Events;
using API.Models;
using MediatR;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace API.Controllers
{
    [Route("[controller]")]
    [Authorize(Roles = Roles.EmployeePermission)]
    public class CustomersController : AuthorizationController
    {
        private readonly ICustomerRepository _repo;
        private readonly IOrdersRepository _ordersRepo;
        private readonly IMediator _events;

        public CustomersController(
            IHttpContextAccessor httpContextAccessor,
            ICustomerRepository repo,
            IOrdersRepository ordersRepository,
            IMediator events)
        {
            _repo = repo;
            _ordersRepo = ordersRepository;
            _events = events;
        }

        [HttpGet("", Name = Routes.GetAllCustomers)]
        [Authorize(Roles = Roles.AdminPermission)]
        public async Task<IActionResult> GetList(
            [FromQuery] int page = 1,
            [FromQuery] int limit = 10)
        {
            var customers =
                await _repo.GetPaginatedCustomersOfCompany(GetCompanyId(), page, limit);

            var view = customers.Select(CustomerMapper.Map).ToList();

            var paginatedView = PaginationMapper.Map(
                view,
                page,
                limit,
                customers.TotalItemCount,
                customers.HasNextPage,
                customers.HasPreviousPage
            );

            return Ok(paginatedView);
        }

        [HttpGet("phoneNumber/{phoneNumber}", Name = Routes.GetCustomerByPhoneNumber)]
        public async Task<IActionResult> GetByPhone(string phoneNumber)
        {
            var entity =
                await _repo.GetCustomerByPhoneNumber(GetCompanyId(), phoneNumber);

            if (entity == null)
                return NotFound();

            entity.Orders = await _ordersRepo.GetLastOrdersOfCustomer(entity, 5);

            return Ok(CustomerMapper.Map(entity));
        }

        [HttpGet("{id}", Name = Routes.GetCustomer)]
        public async Task<IActionResult> GetById(int id)
        {
            var entity = await _repo.Get(id);

            if (entity == null)
                return NotFound();

            entity.Orders = await _ordersRepo.GetLastOrdersOfCustomer(entity, 5);

            return Ok(CustomerMapper.Map(entity));
        }

        [HttpPost("", Name = Routes.CreateCustomer)]
        public async Task<IActionResult> Create([FromBody] CustomerCreationDTO dto)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var customer = CustomerMapper.Map(dto);
            customer.CompanyId = GetCompanyId();

            var entity = await _repo.Create(customer);

            if (entity == null)
            {
                return CreationFailed("Unable to create Customer");
            }

            await _events.Publish(new CustomerCreatedEvent(customer));

            var view = CustomerMapper.Map(await _repo.Get(entity.Id));

            return CreatedAtRoute(Routes.GetCustomer, new {id = view.Id}, view);
        }

        [HttpPut("{id}", Name = Routes.UpdateCustomer)]
        public async Task<IActionResult> Edit(int id, [FromBody] CustomerEditionDto dto)
        {
            if (!ModelState.IsValid) {
                return BadRequest(ModelState);
            }
            
            if (dto == null || dto.Id != id)
            {
                return BadRequest();
            }
            
            var entity = await _repo.Get(id);
            if (entity == null)
            {
                return NotFound();
            }
            
            entity.Change(dto);
            var success = await _repo.Update(entity);

            return success ? NoContent() : EditionFailed("Unable to edit Customer");
        }

        [HttpDelete("{id}", Name = Routes.DeleteCustomer)]
        [Authorize(Roles = Roles.AdminPermission)]
        public async Task<IActionResult> Delete(int id)
        {
            var entity = await _repo.Get(id);
            if (entity == null)
            {
                return NotFound();
            }

            if (await _repo.Delete(entity))
            {
                return Ok();
            }

            return DeletionFailed("Unable to delete Customer");
        }
    }
}

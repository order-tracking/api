﻿using System.Threading.Tasks;
using API.Controllers.Common;
using API.Data.Mapper;
using API.Data.Repository.Generic;
using API.DTOs.View;
using API.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace API.Controllers
{
    [Route("[controller]")]
    [Authorize(Roles = Roles.EmployeePermission)]
    public class StatsController : AuthorizationController
    {
        private readonly IStatsRepository _repo;
        private readonly IEmployeeRepository _employeeRepository;

        public StatsController(
            IStatsRepository repo,
            IEmployeeRepository employeeRepository)
        {
            _repo = repo;
            _employeeRepository = employeeRepository;
        }

        [HttpGet("today", Name = Routes.GetHomeStats)]
        public async Task<IActionResult> Get()
        {
            Stats stats = await _repo.GetCompanyStats(GetCompanyId());

            var distributors = await _employeeRepository.GetEmployeesDistributing();
            var working = await _employeeRepository.GetEmployeesWorking();

            stats.DistributorsWorking = distributors.Count;
            stats.EmployeesWorking = working.Count;
            StatsViewDto view = StatsMapper.Map(stats);

            return Ok(view);
        }
    }
}

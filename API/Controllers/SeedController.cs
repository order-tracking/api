﻿using System;
using API.Data;
using API.Extensions;
using API.Services.Hypertrack;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace API.Controllers
{
    /// <summary>
    /// Helpers for staging quick seed the database
    /// </summary>
    [AllowAnonymous]
    [Route("[controller]")]
    public class SeedController : Controller
    {
        private readonly DatabaseContext _context;
        private readonly IHypertrackApi _api;
        private const string FailedResponseText = "No can do. Production environment.";

        public SeedController(
            DatabaseContext context,
            IHypertrackApi api)
        {
            _context = context;
            _api = api;
        }

        [HttpGet("", Name = Routes.Seed)]
        public IActionResult Seed()
        {
            var env = Environment.GetEnvironmentVariable("ASPNETCORE_ENVIRONMENT");

            if (!env.Equals("Development"))
                return BadRequest(FailedResponseText);

            _context.ClearAllData();
            _context.EnsureSeedDataForContext();

            return Ok("Seed successfull");
        }

        [HttpGet("clear", Name = Routes.DeleteSeed)]
        public IActionResult UnSeed()
        {
            var env = Environment.GetEnvironmentVariable("ASPNETCORE_ENVIRONMENT");

            if (!env.Equals("Development"))
                return BadRequest(FailedResponseText);

            _context.ClearAllData();
            return Ok("Deleted all data");
        }
        
        [HttpGet("clear/hypertrack", Name = Routes.HypertrackClear)]
        public IActionResult HypertrackClear()
        {
            var env = Environment.GetEnvironmentVariable("ASPNETCORE_ENVIRONMENT");

            if (!env.Equals("Development"))
                return BadRequest(FailedResponseText);
            
            try
            {
                // Remove all test and actions users
                _api.DeleteAllUsers().Wait();
                _api.DeleteAllActions().Wait();
            }
            catch (Exception)
            {
                // ignored
            }
            
            return Ok("Deleted all hypertrack data");
        }
        
        
    }
}

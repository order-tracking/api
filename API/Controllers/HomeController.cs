﻿using System;
using API.DTOs.View;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace API.Controllers
{
    [AllowAnonymous]
    public class HomeController : Controller
    {
        [HttpGet("/", Name = Routes.Index)]
        public IActionResult Index()
        {
            var env = Environment.GetEnvironmentVariable("ASPNETCORE_ENVIRONMENT");

            return Ok(new {
                Message = "Welcome to oTrack API. Endpoints are protected.",
                Hostname = Environment.MachineName,
                Environment = env
            });
        }

        /// <summary>
        /// Catch any middleware failure response and return a custom representation
        /// </summary>
        /// <param name="code"></param>
        /// <returns></returns>
        [HttpGet("error/{code}")]
        public IActionResult Error(int code)
        {
            var response = new ErrorViewDto() {
                Code = code,
                Description = null
            };

            switch (code)
            {
                case 401:
                    response.Description =
                        "Not authorized, the JWT invalid or not present.";
                    break;
                case 403:
                    response.Description = "You not allowed to access that resource.";
                    break;
                case 500:
                    response.Description = "Houston, we have a problem!";
                    break;
            }

            return StatusCode(code, response);
        }
    }
}

﻿using System.Threading.Tasks;
using API.Data.Mapper;
using API.Data.Repository.Generic;
using API.DTOs.Creation;
using API.Utils;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace API.Controllers
{
    /// <summary>
    /// This controller is used by Identity Server to test the user credentials
    /// and authenticate the user
    /// </summary>
    [Route("[controller]")]
    [AllowAnonymous]
    public class AuthController : Controller
    {
        private readonly IEmployeeRepository _repo;
        private readonly ICompaniesRepository _repoCompany;

        public AuthController(IEmployeeRepository repo, ICompaniesRepository comp)
        {
            _repo = repo;
            _repoCompany = comp;
        }

        [HttpPost("login", Name = Routes.AuthenticateEmployee)]
        public async Task<IActionResult> Login([FromBody] EmployeeAuthDto dto)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var employee = await _repo.GetEmployeeByPhone(dto.PhoneNumber);
            if (employee == null)
            {
                return NotFound();
            }

            if (!BCrypt.Net.BCrypt.Verify(dto.Password, employee.HashedPassword))
            {
                return BadRequest();
            }

            var company = await _repoCompany.Get(employee.CompanyId);
            employee.Company = company;

            var responseModel = EmployeeMapper.Map(employee);

            responseModel.PrefixedPhoneNumber =
                PhoneUtils.Parse(employee.PhoneNumber, company.CountryCode);

            return Ok(responseModel);
        }
    }

}

using System.Threading.Tasks;
using API.Controllers.Common;
using API.Data.Mapper;
using API.Data.Repository.Generic;
using API.DTOs.Creation;
using API.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace API.Controllers {

    [Route("[controller]")]
    [Authorize(Roles = Roles.EmployeePermission)]
    public class ProductsController : AuthorizationController {
        private readonly IProductRepository _repo;

        public ProductsController(IProductRepository repo){
            _repo = repo;
        }

        [HttpGet("{id}", Name = Routes.GetProduct)]
        public async Task<IActionResult> GetById(int id) {
            var product = await _repo.Get(id);

            if (product == null) {
                return NotFound();
            }

            return Ok(ProductMapper.Map(product));
        }

        [HttpPost("", Name = Routes.CreateProduct)]
        public async Task<IActionResult> Create([FromBody] ProductCreationDTO dto) {
            if (!ModelState.IsValid) {
                return BadRequest(ModelState);
            }

            var product = ProductMapper.Map(dto);
            product.CompanyId = GetCompanyId();

            var result = await _repo.Create(product);

            if (result == null) {
                return CreationFailed();
            }
            
            var view = ProductMapper.Map(await _repo.Get(result.Id));

            return CreatedAtRoute(Routes.GetProduct, new {id = result.Id}, view);
        }

        [HttpDelete("{id}", Name = Routes.DeleteProduct)]
        public async Task<IActionResult> Delete(int id) {
            var entity = await _repo.Get(id);
            if (entity == null) {
                return NotFound();
            }

            if (await _repo.Delete(entity)) {
                return Ok();
            }

            return DeletionFailed();
        }
    }
}

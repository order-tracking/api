﻿using System;
using System.Threading.Tasks;
using API.Controllers.Common;
using API.Data.Mapper;
using API.Data.Repository.Generic;
using API.DTOs.Creation;
using API.DTOs.Edition;
using API.Models;
using API.Utils;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace API.Controllers
{
    [Route("[controller]")]
    [AllowAnonymous]
    public class FeedbacksController : AuthorizationController
    {
        private readonly IFeedbackRepository _repo;
        private readonly IOrdersRepository _ordersRepository;

        public FeedbacksController(
            IFeedbackRepository repo,
            IOrdersRepository ordersRepository)
        {
            _repo = repo;
            _ordersRepository = ordersRepository;
        }

        [HttpGet("{id}", Name = Routes.GetFeedback)]
        [Authorize(Roles = Roles.EmployeePermission)]
        public async Task<IActionResult> Get(int id)
        {
            var feedback = await _repo.Get(id);

            if (feedback == null)
            {
                return NotFound();
            }

            return Ok(FeedbackMapper.Map(feedback));
        }

        [HttpPost("", Name = Routes.CreateFeedback)]
        public async Task<IActionResult> Create([FromBody] FeedbackCreationDTO dto)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var order =
                await _ordersRepository.Get(HashIdsUtils.Decode(dto.OrderHashedId));

            var feedback = FeedbackMapper.Map(dto);
            feedback.CompanyId = order.CompanyId;
            feedback.CreatedAt = DateTime.UtcNow;

            var result = await _repo.Create(feedback);

            if (result == null)
            {
                return CreationFailed();
            }

            var view = FeedbackMapper.Map(await _repo.Get(result.Id));

            return CreatedAtRoute("GetEmployee", new {id = view.Id}, view);
        }

        [HttpPut("{id}", Name = Routes.UpdateFeedback)]
        public async Task<IActionResult> Edit(int id, [FromBody] FeedbackEditionDto dto)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (dto == null || dto.Id != id)
            {
                return BadRequest();
            }

            var entity = await _repo.Get(id);
            if (entity == null)
            {
                return NotFound();
            }

            entity.Change(dto);
            var success = await _repo.Update(entity);

            return success ? NoContent() : EditionFailed();
        }
    }
}

using System;
using System.Linq;
using System.Threading.Tasks;
using API.Controllers.Common;
using API.Data.Mapper;
using API.Data.Repository.Generic;
using API.DTOs.Creation;
using API.DTOs.Edition;
using API.DTOs.View;
using API.Events;
using API.Models;
using API.Services.DistanceCalculator;
using API.Services.Geocoding;
using API.Utils;
using MediatR;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace API.Controllers
{
    [Route("[controller]")]
    [Authorize(Roles = Roles.EmployeePermission)]
    public class OrdersController : AuthorizationController
    {
        private readonly ICompaniesRepository _companiesRepository;
        private readonly IDistanceCalculator _distanceCalculator;
        private readonly IGeocoding _geocoding;
        private readonly IOrdersRepository _repo;
        private readonly IMediator _events;

        public OrdersController(
            IOrdersRepository repo,
            IMediator events,
            ICompaniesRepository companiesRepository,
            IDistanceCalculator distanceCalculator,
            IGeocoding geocoding)
        {
            _companiesRepository = companiesRepository;
            _distanceCalculator = distanceCalculator;
            _geocoding = geocoding;
            _repo = repo;
            _events = events;
        }

        [HttpGet("", Name = Routes.GetOrders)]
        public async Task<IActionResult> GetList(
            [FromQuery] int page = 1,
            [FromQuery] int limit = 10)
        {
            var orders =
                await _repo.GetPaginatedOrdersOfCompany(GetCompanyId(), page, limit);

            var listView = orders.Select(order => OrderMapper.Map(order)).ToList();

            var paginatedView = PaginationMapper.Map(
                listView,
                page,
                limit,
                orders.TotalItemCount,
                orders.HasNextPage,
                orders.HasPreviousPage
            );

            return Ok(paginatedView);
        }

        [HttpGet("ready", Name = Routes.GetReadyOrders)]
        public async Task<IActionResult> GetReadyOrdersList(
            [FromQuery] int page = 1,
            [FromQuery] int limit = 10)
        {
            var orders =
                await _repo.GetPaginatedReadyOrders(GetCompanyId(), page, limit);

            var listView = orders.Select(OrderMapper.Map).ToList();

            var paginatedView = PaginationMapper.Map(
                listView,
                page,
                limit,
                orders.TotalItemCount,
                orders.HasNextPage,
                orders.HasPreviousPage
            );

            return Ok(paginatedView);
        }

        [HttpGet("incomplete", Name = Routes.GetIncompleteOrders)]
        public async Task<IActionResult> GetIncompleteOrdersList(
            [FromQuery] int page = 1,
            [FromQuery] int limit = 10)
        {
            var orders =
                await _repo.GetPaginatedIncompleteOrders(GetCompanyId(), page, limit);

            var listView = orders.Select(order => OrderMapper.Map(order)).ToList();

            var paginatedView = PaginationMapper.Map(
                listView,
                page,
                limit,
                orders.TotalItemCount,
                orders.HasNextPage,
                orders.HasPreviousPage
            );

            return Ok(paginatedView);
        }

        [HttpGet("{id}", Name = Routes.GetOrder)]
        [Authorize(Roles = Roles.DistributorPermission)]
        public async Task<IActionResult> GetById(int id)
        {
            var order = await _repo.Get(id);

            if (order == null)
                return NotFound();

            return Ok(OrderMapper.Map(order));
        }

        // AllowAnonymous access because this endpoint is used in the customer app 
        // to fetch the order details, the id hashed to prevent guessing of ids
        [AllowAnonymous]
        [HttpGet("client/{hash}", Name = Routes.GetOrderHashed)]
        public async Task<IActionResult> GetByIdHashed(string hash)
        {
            var id = HashIdsUtils.Decode(hash);

            return await GetById(id);
        }

        [HttpGet("{id}/feedback", Name = Routes.GetOrderFeedback)]
        public async Task<IActionResult> GetOrderFeedback(int id)
        {
            var feedback = await _repo.GetOrderFeedback(id);

            if (feedback == null)
                return NotFound();

            return Ok(FeedbackMapper.Map(feedback));
        }

        // since the feedback is submited by the customer app, there should not be 
        // required any auth to send feedback
        [AllowAnonymous]
        [HttpGet("client/{hash}/feedback", Name = Routes.GetHashedOrderFeedback)]
        public async Task<IActionResult> GetHashedOrderFeedback(string hash)
        {
            var id = HashIdsUtils.Decode(hash);

            return await GetOrderFeedback(id);
        }

        [HttpGet("{id}/items", Name = Routes.GetOrderItems)]
        public async Task<IActionResult> GetOrderItems(int id)
        {
            var order = await _repo.Get(id);

            if (order == null)
                return NotFound();

            return Ok(order.Items);
        }

        [HttpPost("", Name = Routes.CreateOrder)]
        public async Task<IActionResult> Create([FromBody] OrderCreationDTO dto)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var company = await _companiesRepository.Get(GetCompanyId());

            // Parse address
            dto.Address = AddressUtils.Parse(dto.Address);
            dto.Address2 = AddressUtils.Parse(dto.Address2);
            dto.PostalCode = AddressUtils.ParsePostalCode(dto.PostalCode);

            var order = OrderMapper.Map(dto);

            order.CreatedAt = DateTime.UtcNow;
            order.CompanyId = GetCompanyId();

            var confectionTime = dto.EstimatedTime;
            int distanceTime;
            try
            {
                var distance =
                    await CalculateDistanceFromRestaurant(dto.Address, dto.PostalCode, company);

                order.Distance = distance.Distance;
                distanceTime = distance.Minutes;
            }
            catch (Exception)
            {
                order.Distance = 0;
                distanceTime = 0; // no estimative can be given
            }

            order.AddEstimatedTime(confectionTime, distanceTime);

            // For better accuracy, coordinates are better
            double latitude;
            double longitude;
            try
            {
                var geo = await _geocoding.GetCoordinatesForAddress(new AddressModel() {
                    Address = dto.Address,
                    Address2 = dto.Address2,
                    PostalCode = dto.PostalCode,
                    Country = CountryList.Get(company.CountryCode).Name
                });

                latitude = geo.Latitude;
                longitude = geo.Longitide;
            }
            catch (Exception)
            {
                latitude = 0;
                longitude = 0;
            }

            order.Latitude = latitude;
            order.Longitude = longitude;

            var result = await _repo.Create(order);

            if (result == null)
            {
                return CreationFailed();
            }

            order = await _repo.Get(result.Id);

            await _events.Publish(new OrderCreatedEvent(order));

            var view = OrderMapper.Map(order);

            return CreatedAtRoute(Routes.GetOrder, new {id = view.Id}, view);
        }

        /// <summary>
        /// Used to give an estimative of the distance to the employee while creating the 
        /// order if too far, he can reject the order
        /// </summary>
        /// <param name="dto"></param>
        /// <returns></returns>
        [HttpPost("distance", Name = Routes.CalculateDistance)]
        public async Task<IActionResult> CalculateDistance(
            [FromBody] AddressDistanceCalculatorDTO dto)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            try
            {
                var address = AddressUtils.Parse(dto.Address);
                var postal = AddressUtils.ParsePostalCode(dto.PostalCode);

                var company = await _companiesRepository.Get(GetCompanyId());

                var res = await CalculateDistanceFromRestaurant(address, postal, company);

                return Ok(new AddressDistanceCalculatorViewDto {
                    Distance = res.Distance,
                    MaxAllowedDistance = company.MaxAllowedDistance,
                    Duration = res.Minutes
                });
            }
            catch (Exception)
            {
                return BadRequest(new ErrorViewDto() {
                    Code = 400,
                    Description = "Error calculating the distance."
                });
            }
        }

        [HttpPut("{id}", Name = Routes.EditOrder)]
        public async Task<IActionResult> Edit(int id, [FromBody] OrderEditionDto dto)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (dto == null || dto.Id != id)
            {
                return BadRequest();
            }

            var entity = await _repo.Get(id);
            if (entity == null)
            {
                return NotFound();
            }

            entity.Change(dto);
            var success = await _repo.Update(entity);

            return success ? NoContent() : EditionFailed();
        }

        [HttpPut("items/{itemId}", Name = Routes.EditOrderItem)]
        public async Task<IActionResult> EditItem(
            int itemId,
            [FromBody] ItemEditionDto dto)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (dto == null || dto.Id != itemId)
            {
                return BadRequest();
            }

            var entity = await _repo.GetOrderItem(itemId);
            if (entity == null)
            {
                return NotFound();
            }

            entity.Change(dto);
            var success = await _repo.UpdateOrderItem(entity);

            return success ? NoContent() : EditionFailed();
        }

        [HttpDelete("{id}", Name = Routes.DeleteOrder)]
        public async Task<IActionResult> Delete(int id)
        {
            var entity = await _repo.Get(id);
            if (entity == null)
            {
                return NotFound();
            }

            await _events.Publish(new OrderDeletedEvent(entity));

            if (await _repo.Delete(entity))
            {
                return Ok();
            }

            return DeletionFailed();
        }

        /*
        |-----------------------------------------------------------------------
        | Distance Calculator from restaurant to an address
        |-----------------------------------------------------------------------
        */
        private async Task<DistanceResultDto> CalculateDistanceFromRestaurant(
            string address,
            string postalCode,
            Company cmp = null)
        {
            // fetch the company or use the object received (to save 1 request)
            var company = cmp ?? await _companiesRepository.Get(GetCompanyId());

            var country = CountryList.Get(company.CountryCode);

            var from = new AddressModel() {
                Address = company.Address,
                PostalCode = company.PostalCode,
                Country = country.Name
            };

            var to = new AddressModel() {
                Address = address,
                PostalCode = postalCode,
                Country = country.Name // Should be the same country as the restaurant
            };

            return
                await _distanceCalculator.CalculateDistanceBetweenAddressesAsync(from,
                    to);
        }
    }

}

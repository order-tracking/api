﻿using Microsoft.AspNetCore.Mvc;

namespace API.Controllers.Common
{
    public class GenericController : Controller
    {
        protected IActionResult CreationFailed(string message = "Internal Server Error")
        {
            return StatusCode(500, message);
        }

        protected IActionResult EditionFailed(string message = "Internal Server Error")
        {
            return StatusCode(500, message);
        }

        protected IActionResult DeletionFailed(string message = "Internal Server Error")
        {
            return StatusCode(500, message);
        }
    }
}

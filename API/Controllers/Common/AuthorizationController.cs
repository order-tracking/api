using System;
using System.Security.Claims;
using API.Models;
using Microsoft.AspNetCore.Authorization;

namespace API.Controllers.Common
{
    [Authorize]
    public class AuthorizationController : GenericController
    {
        protected int GetCompanyId()
        {
            var companyId = HttpContext.User.FindFirstValue("CompanyId");

            return Convert.ToInt32(companyId);
        }
        
        protected int GetUserId()
        {
            var employeeId = HttpContext.User.FindFirstValue("employeeId");

            return Convert.ToInt32(employeeId);
        }
        
        protected string GetUserRole()
        {
            var role = HttpContext.User.FindFirstValue("role");

            return role;
        }
        
        protected bool UserRoleIsParent(string roleToCompare)
        {
            var userRole = GetUserRole();
            
            if (userRole.Equals(Role.Developer))
                return true; // can do everything, even to other developers

            return Roles.IsChildRole(userRole, roleToCompare);
        }
    }
}

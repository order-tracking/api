using System.Linq;
using System.Security.Claims;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using API.Controllers.Common;
using API.Data.Mapper;
using API.Data.Repository.Generic;
using API.DTOs.Creation;
using API.DTOs.Edition;
using API.Events;
using API.Models;
using MediatR;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace API.Controllers
{
    [Route("[controller]")]
    [Authorize(Roles = Roles.AdminPermission)]
    public class EmployeesController : AuthorizationController
    {
        private readonly IEmployeeRepository _repo;
        private readonly IMediator _events;

        public EmployeesController(IEmployeeRepository repo, IMediator events)
        {
            _repo = repo;
            _events = events;
        }

        [HttpGet("", Name = Routes.GetEmployees)]
        public async Task<IActionResult> GetList(
            [FromQuery] int page = 1,
            [FromQuery] int limit = 10)
        {
            var employees =
                await _repo.GetPaginatedEmployeesOfCompany(GetCompanyId(), page, limit);

            var view = employees.Select(EmployeeMapper.Map).ToList();

            var paginatedListView = PaginationMapper.Map(
                view,
                page,
                limit,
                employees.TotalItemCount,
                employees.HasNextPage,
                employees.HasPreviousPage
            );

            return Ok(paginatedListView);
        }

        [HttpGet("{id}", Name = Routes.GetEmployee)]
        public async Task<IActionResult> GetById(int id)
        {
            var employee = await _repo.GetEmployee(id);

            if (employee == null)
            {
                return NotFound();
            }

            return Ok(EmployeeMapper.Map(employee));
        }

        [HttpPost("", Name = Routes.CreateEmployee)]
        public async Task<IActionResult> Create([FromBody] EmployeeCreationDTO dto)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            dto.PhoneNumber = Regex.Replace(dto.PhoneNumber, @"\s+", "");
            dto.Password = BCrypt.Net.BCrypt.HashPassword(dto.Password);

            var employee = EmployeeMapper.Map(dto);
            employee.CompanyId = GetCompanyId();

            var result = await _repo.Create(employee);

            if (result == null)
            {
                return CreationFailed();
            }

            await _events.Publish(new EmployeeCreatedEvent(employee));

            var view = EmployeeMapper.Map(await _repo.GetEmployee(result.Id));

            return CreatedAtRoute(Routes.GetEmployee, new {id = view.Id}, view);
        }

        [HttpPut("{id}", Name = Routes.UpdateEmployee)]
        public async Task<IActionResult> Edit(int id, [FromBody] EmployeeEditionDto dto)
        {
            if (!ModelState.IsValid) {
                return BadRequest(ModelState);
            }

            var entity = await _repo.GetEmployee(id);
            if (entity == null)
            {
                return NotFound();
            }

            // An employee cannot edit their role
            if (HttpContext.User.FindFirstValue("employeeId").Equals(id.ToString())) {
                dto.Role = entity.Role;
            }
            else {
                var userRole = HttpContext.User.FindFirstValue("role");

                //Trying to edit a higher roled employee: Not able
                if (!Roles.IsChildRole(userRole, entity.Role)) {
                    ModelState.AddModelError("Role", "Employee Role is higher than yours");
                    return BadRequest(ModelState);
                }

                //Trying to edit to a higher role: Not able
                if (!userRole.Equals(Role.Developer) && !Roles.IsChildRole(userRole, dto.Role)) {
                    ModelState.AddModelError("Role", "Role is not valid or is not a child of your role.");
                    return BadRequest(ModelState);
                }
            }

            entity.Change(dto);
            var success = await _repo.Update(entity);

            return success ? NoContent() : EditionFailed();
        }

        [HttpDelete("{id}", Name = Routes.DeleteEmployee)]
        public async Task<IActionResult> Delete(int id)
        {
            var employee = await _repo.Get(id);
            if (employee == null)
            {
                return NotFound();
            }

            // only can delete employees with a child role
            if (!UserRoleIsParent(employee.Role))
            {
                return Unauthorized();
            }

            if (await _repo.Delete(employee))
            {
                return Ok();
            }

            return DeletionFailed();
        }
    }

}

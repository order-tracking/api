﻿using System;
using System.Linq;
using System.Reflection;
using API.Data;
using API.Data.Repository;
using API.Data.Repository.Generic;
using API.Extensions;
using API.Services.DistanceCalculator;
using API.Services.Geocoding;
using API.Services.HttpService;
using API.Services.Hypertrack;
using API.Services.SmsSender;
using MediatR;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Cors.Internal;
using Microsoft.AspNetCore.Routing;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;

namespace API
{
    public class Startup
    {
        private const string CorsPolicy = "oTrack-testPolicy";

        private IConfigurationRoot Configuration { get; }

        public Startup(IHostingEnvironment env)
        {
            var builder = new ConfigurationBuilder()
                .SetBasePath(env.ContentRootPath)
                .AddJsonFile("appsettings.json", optional: false, reloadOnChange: true)
                .AddJsonFile($"appsettings.{env.EnvironmentName}.json", optional: true)
                .AddEnvironmentVariables();

            Configuration = builder.Build();
        }

        public void ConfigureServices(IServiceCollection services)
        {
            // Add framework services.
            services.AddMvc().AddJsonOptions(options =>
            {
                options.SerializerSettings.NullValueHandling =
                    NullValueHandling.Ignore;
            });

            // Add EF Core with Postgres
            var con = GetConnectionString();
            Console.WriteLine("Connection: " + con);
            services.AddDbContext<DatabaseContext>(
                opt => opt.UseNpgsql(con)
            );

            services.AddSession();

            services.AddCors(options =>
            {
                options.AddPolicy(CorsPolicy,
                    builder =>
                    {
                        var origins = Configuration.GetSection("SafeOrigins")
                            .GetChildren()
                            .Select(x => x.Value)
                            .ToArray();

                        builder
                            .WithOrigins(origins)
                            .WithHeaders(new[] {
                                "Authorization",
                                "Content-Type"
                            })
                            .AllowAnyMethod();
                    });
            });

            services.Configure<RouteOptions>(options => options.LowercaseUrls = true);

            services.Configure<MvcOptions>(options =>
            {
                options.Filters.Add(new CorsAuthorizationFilterFactory(CorsPolicy));
            });

            services.AddSingleton<IHttpContextAccessor, HttpContextAccessor>();
            services.AddSingleton<IConfiguration>(Configuration);

            // Single instace of the HttpClient
            services.AddSingleton<IHttpService, HttpService>();

            // Domain repositories
            services.AddScoped<ICustomerRepository, CustomersRepository>();
            services.AddScoped<ICompaniesRepository, CompaniesRepository>();
            services.AddScoped<IEmployeeRepository, EmployeesRepository>();
            services.AddScoped<IOrdersRepository, OrdersRepository>();
            services.AddScoped<IProcessPipelineRepository, ProcessPipelineRepository>();
            services.AddScoped<IProductGroupsRepository, ProductGroupsRepository>();
            services.AddScoped<IStatsRepository, StatsRepository>();
            services.AddScoped<IProductRepository, ProductRepository>();
            services.AddScoped<IJourneysRepository, JourneysRepository>();
            services.AddScoped<IFeedbackRepository, FeedbackRepository>();
            services.AddScoped<IReportsRepository, ReportsRepository>();
            services.AddScoped<ICampaignRepository, CampaignRepository>();

            // Third party services
            services.AddSingleton<IHypertrackApi, HypertrackApi>();
            services.AddSingleton<IDistanceCalculator, GoogleMapsDistanceCalculator>();
            services.AddSingleton<IGeocoding, GoogleGeocodingService>();
            services.AddSingleton<ISmsSender, TwilioSmsSender>();

            // Load MediatR with Event Handlers
            services.AddScoped<IMediator, Mediator>();
            services.AddScoped<SingleInstanceFactory>(p => t => p.GetRequiredService(t));
            services.AddScoped<MultiInstanceFactory>(p => t => p.GetRequiredServices(t));
            services.AddEventHandlers(typeof(Startup).GetTypeInfo().Assembly);
            
            services.AddMemoryCache();
        }

        public void Configure(
            IApplicationBuilder app,
            ILoggerFactory loggerFactory, IHostingEnvironment env)
        {
            loggerFactory.AddConsole(Configuration.GetSection("Logging"));
            loggerFactory.AddDebug();

            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseExceptionHandler("/error/500");
            }

            app.UseCors(CorsPolicy);

            app.UseIdentityServerAuthentication(new IdentityServerAuthenticationOptions {
                Authority = Configuration["IDENTITY_URL"],
                ApiName = "api1",
                AutomaticAuthenticate = true,
                AutomaticChallenge = true
            });

            // Custom error responses based on status code
            app.UseStatusCodePagesWithReExecute("/error/{0}");

            app.UseMvcWithDefaultRoute();
        }

        /*
        |-----------------------------------------------------------------------
        | Connection String
        |-----------------------------------------------------------------------
        */
        private string GetConnectionString()
        {
            const string con = "oTrack:Database:PostgresConnection";
            if (!string.IsNullOrEmpty(Configuration[con]))
            {
                return Configuration[con];
            }

            var host = Getenv("DB_HOST");
            var name = Getenv("DB_NAME");
            var user = Getenv("DB_USER");
            var password = Getenv("DB_PASSWORD");

            return
                $"User ID={user};Password={password};Host={host};Port={5432};Database={name}";
        }

        private static string Getenv(string key) =>
            Environment.GetEnvironmentVariable(key);
    }

}

﻿using API.Models;
using MediatR;

namespace API.Events
{
    public class CustomerCreatedEvent : INotification
    {
        public Customer Customer { get; }
        public CustomerCreatedEvent(Customer customer)
        {
            Customer = customer;
        }
    }
}

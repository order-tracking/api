﻿using API.Models;
using MediatR;

namespace API.Events
{
    public class OrderCreatedEvent : INotification
    {
        public Order Order { get; }

        public OrderCreatedEvent(Order order)
        {
            Order = order;
        }
    }
}

﻿using API.Models;
using MediatR;

namespace API.Events
{
    public class OrderDeletedEvent : INotification
    {
        public Order Order { get; }

        public OrderDeletedEvent(Order order)
        {
            Order = order;
        }
    }
}

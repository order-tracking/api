﻿using API.DTOs.HypertrackEvents;
using API.Models;
using MediatR;

namespace API.Events
{
    public class DeliveryTimeChangedEvent : INotification
    {
        public Order Order { get; }
        public HypertrackEventDto Dto { get; }

        public DeliveryTimeChangedEvent(Order order, HypertrackEventDto dto)
        {
            Order = order;
            Dto = dto;
        }

    }
}

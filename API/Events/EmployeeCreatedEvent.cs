﻿using API.Models;
using MediatR;

namespace API.Events
{
    public class EmployeeCreatedEvent : INotification
    {
        public Employee Employee { get; }
        public EmployeeCreatedEvent(Employee employee)
        {
            Employee = employee;
        }
    }
}

﻿using API.DTOs.HypertrackEvents;
using API.Models;
using MediatR;

namespace API.Events
{
    public class DistributorIsOnTheWayEvent : INotification
    {
        public Order Order { get; }
        public Customer Customer { get; }
        public HypertrackEventDto Dto { get; }

        public DistributorIsOnTheWayEvent(Order order, Customer customer, HypertrackEventDto dto)
        {
            Order = order;
            Customer = customer;
            Dto = dto;
        }
    }
}

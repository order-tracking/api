﻿using API.DTOs.HypertrackEvents;
using MediatR;

namespace API.Events
{
    public class EmployeeTrackingChangedEvent : INotification
    {
        public HypertrackEventDto Dto { get; }

        public EmployeeTrackingChangedEvent(HypertrackEventDto dto)
        {
            Dto = dto;
        }
    }
}

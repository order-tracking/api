﻿using API.DTOs.HypertrackEvents;
using API.Models;
using MediatR;

namespace API.Events
{
    public class OrderCompletedEvent : INotification
    {
        public readonly HypertrackEventDto _dto;
        public readonly Order Order;

        public OrderCompletedEvent(Order order, HypertrackEventDto dto)
        {
            _dto = dto;
            Order = order;
        }
    }
}

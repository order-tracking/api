﻿using API.Models;
using MediatR;

namespace API.Events
{
    public class SendNewCampaignEvent : INotification
    {
        public readonly Campaign Campaign;

        public SendNewCampaignEvent(Campaign campaign)
        {
            Campaign = campaign;
        }

    }
}

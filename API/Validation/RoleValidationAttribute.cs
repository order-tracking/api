﻿using System.ComponentModel.DataAnnotations;
using System.Security.Claims;
using API.Models;
using Microsoft.AspNetCore.Http;

namespace API.Validation
{
    /// <summary>
    /// Verify if the input role exists and is a child role of the user role
    /// </summary>
    public class RoleValidationAttribute : ValidationAttribute
    {

        protected override ValidationResult IsValid(
            object value,
            ValidationContext validationContext)
        {
            var httpContext =
                (IHttpContextAccessor) validationContext.GetService(
                    typeof(IHttpContextAccessor));

            var userRole = httpContext.HttpContext.User.FindFirstValue("role");
            var inputRole = (string) value;
            
            if (!userRole.Equals(Role.Developer) && 
                !Roles.IsChildRole(userRole, inputRole))
            {
                return new ValidationResult("Role is not valid or is not a child of your role.");
            }

            return ValidationResult.Success;
        }

    }
}

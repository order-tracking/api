﻿using System.ComponentModel.DataAnnotations;
using System.Security.Claims;
using API.Data.Repository.Generic;
using Microsoft.AspNetCore.Http;

namespace API.Validation
{
    /// <summary>
    /// Verify if the given phone number already exists in Customers table
    /// </summary>
    public class UniquePhoneNumberAttribute : ValidationAttribute
    {

        protected override ValidationResult IsValid(
            object value,
            ValidationContext validationContext)
        {
            var repo =
                (ICustomerRepository) validationContext.GetService(
                    typeof(ICustomerRepository));
            var httpContext =
                (IHttpContextAccessor) validationContext.GetService(
                    typeof(IHttpContextAccessor));

            var companyId = httpContext.HttpContext.User.FindFirstValue("CompanyId");

            if (repo.GetCustomerByPhoneNumber(int.Parse(companyId), (string) value)
                    .Result !=
                null)
            {
                return new ValidationResult("Phone number already exists.");
            }

            return ValidationResult.Success;
        }

    }
}

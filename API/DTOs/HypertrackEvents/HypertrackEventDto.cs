﻿using System;
using System.Collections.Generic;

namespace API.DTOs.HypertrackEvents
{
    public class HypertrackEventDto
    {
        public string id { get; set; }
        public string user_id { get; set; }
        public DateTime recorded_at { get; set; }
        public string type { get; set; }
        public Data data { get; set; }
        public DateTime created_at { get; set; }
    }
    
    public class Geojson
    {
        public string type { get; set; }
        public List<double> coordinates { get; set; }
    }

    public class LastLocation
    {
        public DateTime recorded_at { get; set; }
        public string activity { get; set; }
        public double speed { get; set; }
        public Geojson geojson { get; set; }
        public double accuracy { get; set; }
    }

    public class Display
    {
        public string sub_status_text { get; set; }
        public double seconds_elapsed_since_last_heartbeat { get; set; }
        public int battery { get; set; }
        public string status_text { get; set; }
        public double speed { get; set; }
        public bool is_warning { get; set; }
    }

    public class User
    {
        public string id { get; set; }
        public object group_id { get; set; }
        public object lookup_id { get; set; }
        public string name { get; set; }
        public object phone { get; set; }
        public string photo { get; set; }
        public string availability_status { get; set; }
        public string vehicle_type { get; set; }
        public List<object> pending_actions { get; set; }
        public bool is_connected { get; set; }
        public LastLocation last_location { get; set; }
        public DateTime last_heartbeat_at { get; set; }
        public int last_battery { get; set; }
        public string location_status { get; set; }
        public Display display { get; set; }
        public DateTime created_at { get; set; }
        public DateTime modified_at { get; set; }
    }

    public class Location
    {
        public string type { get; set; }
        public List<double> coordinates { get; set; }
    }

    public class StartedPlace
    {
        public string id { get; set; }
        public object name { get; set; }
        public Location location { get; set; }
        public string address { get; set; }
        public string locality { get; set; }
        public object landmark { get; set; }
        public string zip_code { get; set; }
        public string city { get; set; }
        public string state { get; set; }
        public string country { get; set; }
    }

    public class Location2
    {
        public string type { get; set; }
        public List<double> coordinates { get; set; }
    }

    public class ExpectedPlace
    {
        public string id { get; set; }
        public object name { get; set; }
        public Location2 location { get; set; }
        public string address { get; set; }
        public string locality { get; set; }
        public object landmark { get; set; }
        public string zip_code { get; set; }
        public string city { get; set; }
        public string state { get; set; }
        public string country { get; set; }
    }

    public class Display2
    {
        public double duration_elapsed { get; set; }
        public object duration_remaining { get; set; }
        public string sub_status_text { get; set; }
        public bool show_summary { get; set; }
        public DateTime ended_at { get; set; }
        public object distance_remaining { get; set; }
        public string status_text { get; set; }
        public bool is_late { get; set; }
        public string distance_unit { get; set; }
    }

    public class Object
    {
        public string id { get; set; }
        public User user { get; set; }
        public string type { get; set; }
        public string vehicle_type { get; set; }
        public StartedPlace started_place { get; set; }
        public DateTime started_at { get; set; }
        public ExpectedPlace expected_place { get; set; }
        public object scheduled_at { get; set; }
        public object expected_at { get; set; }
        public object completed_place { get; set; }
        public DateTime completed_at { get; set; }
        public DateTime assigned_at { get; set; }
        public object suspended_at { get; set; }
        public object canceled_at { get; set; }
        public string status { get; set; }
        public object sub_status { get; set; }
        public string eta { get; set; }
        public DateTime initial_eta { get; set; }
        public string short_code { get; set; }
        public string tracking_url { get; set; }
        public string lookup_id { get; set; }
        public object collection_id { get; set; }
        public DateTime created_at { get; set; }
        public DateTime modified_at { get; set; }
        public Display2 display { get; set; }
        public List<object> event_flags { get; set; }
        public List<object> metadata { get; set; }
        public double distance { get; set; }
        public int duration { get; set; }
    }

    public class Data
    {
        public Object @object { get; set; }
    }
}

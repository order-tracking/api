using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace API.DTOs.Creation {

    public class ProductCreationDTO {
        
        [Required]
        [MinLength(3)]
        public string Name { get; set; }
        
        [Url]
        public string Photo { get; set; }

        public int? PipelineId { get; set; }

        [Required]
        public int ProductGroupId { get; set; }

        public List<ProductTypeCreationDTO> ProductTypes { get; set; }
    }

}

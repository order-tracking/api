﻿using System.ComponentModel.DataAnnotations;

namespace API.DTOs.Creation
{
    public class AddressDistanceCalculatorDTO
    {
        [Required]
        public string Address { get; set; }

        public string Address2 { get; set; }

        [Required]
        public string PostalCode { get; set; }
    }
}

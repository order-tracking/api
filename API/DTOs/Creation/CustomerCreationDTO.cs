using System.ComponentModel.DataAnnotations;
using API.Validation;

namespace API.DTOs.Creation {

    public class CustomerCreationDTO {
        [Required]
        public string Name { get; set; }

        [Required]
        [UniquePhoneNumber]
        public string PhoneNumber { get; set; }
    }

}

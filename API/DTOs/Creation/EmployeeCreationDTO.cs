using System.ComponentModel.DataAnnotations;
using API.Validation;

namespace API.DTOs.Creation {

    public class EmployeeCreationDTO {
        [Required]
        [MinLength(3)]
        public string Name { get; set; }

        [Required]
        [MinLength(5)]
        public string PhoneNumber { get; set; }

        [Required]
        [MinLength(4)]
        public string Password { get; set; }

        [Required]
        [RoleValidation]
        public string Role { get; set; }
    }

}

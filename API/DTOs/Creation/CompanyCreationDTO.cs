using System.ComponentModel.DataAnnotations;

namespace API.DTOs.Creation {

    public class CompanyCreationDTO {
        [Required]
        public string Name { get; set; }
    }

}

﻿using System.ComponentModel.DataAnnotations;

namespace API.DTOs.Creation
{
    public class EmployeeAuthDto
    {
        [Required]
        [MinLength(4)]
        public string PhoneNumber { get; set; }

        [Required]
        [MinLength(4)]
        public string Password { get; set; }
    }
}

using System.ComponentModel.DataAnnotations;

namespace API.DTOs.Creation {

    public class ProductGroupCreationDTO {
        
        [Required]
        [MinLength(3)]
        public string Name { get; set; }

        public int? PreviousProductGroupId { get; set; }
    }

}

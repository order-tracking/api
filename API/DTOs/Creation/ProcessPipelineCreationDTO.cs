using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace API.DTOs.Creation {

    public class ProcessPipelineCreationDTO {
        
        [Required]
        [MinLength(3)]
        public string Name { get; set; }

        [Required]
        public List<ProcessCreationDTO> Processes { get; set; }
    }

}

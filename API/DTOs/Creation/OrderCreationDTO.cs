using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace API.DTOs.Creation {

    public class OrderCreationDTO {
        [Required]
        public int EstimatedTime { get; set; }

        [Required]
        public int CustomerId { get; set; }

        [Required]
        [MinLength(4)]
        public string Address { get; set; }
        
        public string Address2 { get; set; }
        
        [Required]
        [MinLength(4)]
        public string PostalCode { get; set; }

        [Required]
        public List<ItemCreationDTO> Items { get; set; }
    }

}

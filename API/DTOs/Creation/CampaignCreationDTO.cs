using System.ComponentModel.DataAnnotations;

namespace API.DTOs.Creation
{

    public class CampaignCreationDTO
    {
        [Required]
        [MinLength(3)]
        public string Title { get; set; }
        
        [Required]
        [MaxLength(1500)]
        public string Message { get; set; }
    }

}

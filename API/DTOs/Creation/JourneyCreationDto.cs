﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace API.DTOs.Creation {

    public class JourneyCreationDTO {

        public class JourneyOrderCreationDTO {
            public int Id { get; set; }
        }
        
        [Required]
        public int EmployeeId { get; set; }

        [Required]
        public List<JourneyOrderCreationDTO> Orders { get; set; }
    }

}
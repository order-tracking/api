﻿using System.ComponentModel.DataAnnotations;

namespace API.DTOs.Creation {

    public class ProductTypeCreationDTO {
        
        [Required]
        [MinLength(3)]
        public string Name { get; set; }
    }

}

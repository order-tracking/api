using System.ComponentModel.DataAnnotations;

namespace API.DTOs.Creation {

    public class ProcessCreationDTO {
        [Required]
        public int OrderNumber { get; set; }

        [Required]
        public string Name { get; set; }
    }

}

﻿using System.ComponentModel.DataAnnotations;

namespace API.DTOs.Creation
{
    public class ReportDto
    {   
        [Required]
        public int Days { get; set; }
    }
}

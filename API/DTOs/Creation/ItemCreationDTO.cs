using System.ComponentModel.DataAnnotations;

namespace API.DTOs.Creation
{
    public class ItemCreationDTO
    {
        public string Note { get; set; }

        [Required]
        public int Quantity { get; set; }

        [Required]
        public int ProductId { get; set; }

        public int? ProductTypeId { get; set; }
    }
}

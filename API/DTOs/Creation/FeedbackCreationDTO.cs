﻿using System.ComponentModel.DataAnnotations;
using API.Models;

namespace API.DTOs.Creation {

    public class FeedbackCreationDTO {
        
        [Required]
        public Rating Rating { get; set; }
        
        public string Note { get; set; }

        [Required]
        public string OrderHashedId { get; set; }
    }

}
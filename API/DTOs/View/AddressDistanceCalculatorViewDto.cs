﻿namespace API.DTOs.View
{

    public class AddressDistanceCalculatorViewDto
    {
        /// <summary>
        /// Meters
        /// </summary>
        public int Distance { get; set; }

        /// <summary>
        /// Meters
        /// </summary>
        public int MaxAllowedDistance { get; set; }
        
        /// <summary>
        /// Minutes
        /// </summary>
        public int Duration { get; set; }
    }
}

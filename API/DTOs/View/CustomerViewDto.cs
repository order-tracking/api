using System.Collections.Generic;

namespace API.DTOs.View {

    public class CustomerViewDto {
        public int Id { get; set; }

        public string Name { get; set; }

        public string PhoneNumber { get; set; }

        public List<OrderViewDto> Orders { get; set; }
    }

}
﻿namespace API.DTOs.View
{
    public class ErrorViewDto
    {
        public int Code { get; set; }
        public string Description { get; set; }
    }

}

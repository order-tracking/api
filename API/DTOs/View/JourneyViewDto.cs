﻿using System;
using System.Collections.Generic;

namespace API.DTOs.View {

    public class JourneyViewDto {
        
        public int Id { get; set; }

        public bool Completed { get; set; }
        
        public bool OnGoing { get; set; }

        public EmployeeViewDto Employee { get; set; }
        
        public DateTime CreatedAt { get; set; }
        
        public DateTime? FinishedAt { get; set; }

        public virtual List<OrderViewDto> Orders { get; set; }
    }

}

﻿using System;

namespace API.DTOs.View {

    public class CampaignViewDto {
        public int Id { get; set; }
        
        public string Title { get; set; }

        public string Message { get; set; }
        
        public int TotalSmsSent { get; set; }

        public int TotalCustomers { get; set; }

        public DateTime CreatedAt { get; set; }
    }
}

namespace API.DTOs.View {

    public class ProductTypeViewDto {
        public int Id { get; set; }
        public string Name { get; set; }
    }

}
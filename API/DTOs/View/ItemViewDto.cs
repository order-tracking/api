namespace API.DTOs.View {

    public class ItemViewDto {
        public int Id { get; set; }
        public string Note { get; set; }
        public int Quantity { get; set; }
        public bool Completed { get; set; }
        public int OrderId { get; set; }
        public int? ProcessId { get; set; }
        public ProductViewDto Product { get; set; }
        public ProductTypeViewDto ProductType { get; set; }
    }

}
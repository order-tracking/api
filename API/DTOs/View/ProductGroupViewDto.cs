using System.Collections.Generic;

namespace API.DTOs.View {

    public class ProductGroupViewDto {
        public int Id { get; set; }
        public string Name { get; set; }
        public List<ProductGroupViewDto> NextProductGroups { get; set; }
        public List<ProductViewDto> Products { get; set; }
    }

}
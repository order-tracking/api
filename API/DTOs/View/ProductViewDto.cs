using System.Collections.Generic;

namespace API.DTOs.View {

    public class ProductViewDto {
        public int Id { get; set; }

        public string Name { get; set; }

        public string Photo { get; set; }

        public List<ProductTypeViewDto> ProductTypes { get; set; }

        public ProcessPipelineViewDto ProcessPipeline { get; set; }
    }

}
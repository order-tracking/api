using System;
using System.Collections.Generic;

namespace API.DTOs.View {

    public class OrderViewDto {
        public int Id { get; set; }
        public string HashId { get; set; }
        public int Distance { get; set; }
        public int ConfectionTime { get; set; }
        public int JourneyDuration { get; set; }
        public string Address { get; set; }
        public string Address2 { get; set; }
        public string PostalCode { get; set; }
        public double Latitude { get; set; }
        public double Longitude { get; set; }
        public DateTime EstimatedDate { get; set; }
        public bool ReadyToDistribute { get; set; }
        public bool ItemsComplete { get; set; }
        public bool OnArrivingNotified { get; set; }
        public bool OnGoing { get; set; }
        public DateTime CreatedAt { get; set; }
        public DateTime? DeliveredAt { get; set; }
        public int? FeedbackId { get; set; }
        public CustomerViewDto Customer { get; set; }
        public List<ItemViewDto> Items { get; set; }
    }

}

﻿using API.Models;

namespace API.DTOs.View {

    public class FeedbackViewDto {
        public int Id { get; set; }

        public Rating Rating { get; set; }

        public string Note { get; set; }

        public int OrderId { get; set; }
        
        public string HashOrderId { get; set; }
    }

}
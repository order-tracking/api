using System.Collections.Generic;

namespace API.DTOs.View {

    public class PaginatedListViewDto<T> {
        public int Count { get; set; }
        public int Limit { get; set; }
        public int Page { get; set; }
        public bool HasNextPage { get; set; }
        public bool HasPreviousPage { get; set; }
        public List<T> Items { get; set; }
    }

}
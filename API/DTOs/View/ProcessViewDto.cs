using System.Collections.Generic;

namespace API.DTOs.View {

    public class ProcessViewDto {
        public int Id { get; set; }

        public string Name { get; set; }

        public int OrderNumber { get; set; }
        
        public List<ItemViewDto> Items { get; set; }
    }

}
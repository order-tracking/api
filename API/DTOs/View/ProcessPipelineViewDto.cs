using System.Collections.Generic;

namespace API.DTOs.View {

    public class ProcessPipelineViewDto {
        public int Id { get; set; }

        public string Name { get; set; }

        public List<ProcessViewDto> Processes { get; set; }
    }

}
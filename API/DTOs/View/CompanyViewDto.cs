﻿namespace API.DTOs.View {

    public class CompanyViewDto {
        public int Id { get; set; }

        public string Name { get; set; }
        
        public string Address { get; set; }

        public string PostalCode { get; set; }

        public int MaxAllowedDistance { get; set; }
    }

}
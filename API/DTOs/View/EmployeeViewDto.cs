namespace API.DTOs.View {

    public class EmployeeViewDto {
        public int Id { get; set; }
        
        public int CompanyId { get; set; }
        
        public string CompanyName { get; set; }

        public string Name { get; set; }

        public string PhoneNumber { get; set; }
        
        /// <summary>
        /// Phone number with country code like: +351925706001
        /// </summary>
        public string PrefixedPhoneNumber { get; set; }

        public string Role { get; set; }
    }

}

﻿using System.ComponentModel.DataAnnotations;

namespace API.DTOs.Edition {

    public class ProductGroupEditionDto {
        
        [Required]
        public int Id { get; set; }

        [Required]
        public string Name { get; set; }
    }

}
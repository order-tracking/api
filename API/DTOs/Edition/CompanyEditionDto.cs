﻿using System.ComponentModel.DataAnnotations;

namespace API.DTOs.Edition {

    public class CompanyEditionDto {
        [Required]
        public int Id { get; set; }
        
        [Required]
        public string Name { get; set; }
        
        [Required]
        public string Address { get; set; }
        
        [Required]
        public string PostalCode { get; set; }
        
        [Required]
        public int MaxAllowedDistance { get; set; }
    }

}
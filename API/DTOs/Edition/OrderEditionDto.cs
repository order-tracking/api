﻿using System;
using System.ComponentModel.DataAnnotations;

namespace API.DTOs.Edition {

    public class OrderEditionDto {
        
        [Required]
        public int Id { get; set; }
        
        [Required]
        public string Address { get; set; }

        public string Address2 { get; set; }

        [Required]
        public string PostalCode { get; set; }

        [Required]
        public bool ReadyToDistribute { get; set; }

        [Required]
        public bool OnGoing { get; set; }

        public DateTime? DeliveredAt { get; set; }
    }

}
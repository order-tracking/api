﻿using System.ComponentModel.DataAnnotations;
using API.Models;

namespace API.DTOs.Edition {

    public class FeedbackEditionDto {
        
        [Required]
        public int Id { get; set; }

        [Required]
        public Rating Rating { get; set; }
        
        public string Note { get; set; }
    }

}
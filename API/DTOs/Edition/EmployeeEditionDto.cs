﻿using System.ComponentModel.DataAnnotations;

namespace API.DTOs.Edition {

    public class EmployeeEditionDto {

        [Required]
        [MinLength(3)]
        public string Name { get; set; }

        public string Role { get; set; }
    }

}
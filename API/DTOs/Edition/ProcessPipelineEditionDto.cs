﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace API.DTOs.Edition {

    public class ProcessPipelineEditionDto {
        
        [Required]
        public int Id { get; set; }
        
        [Required]
        public string Name { get; set; }
        
        [Required]
        public List<ProcessEditionDto> Processes { get; set; }
    }

    public class ProcessEditionDto {
        
        [Required]
        public int Id { get; set; }

        [Required]
        public string Name { get; set; }
    }

}
﻿using System.ComponentModel.DataAnnotations;

namespace API.DTOs.Edition {

    public class JourneyEditionDto {
        
        [Required]
        public int Id { get; set; }

        [Required]
        public bool Completed { get; set; }
    }

}
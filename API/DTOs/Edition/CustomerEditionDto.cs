﻿using System.ComponentModel.DataAnnotations;

namespace API.DTOs.Edition {

    public class CustomerEditionDto {
        
        [Required]
        public int Id { get; set; }

        [Required]
        public string Name { get; set; }

        [Required]
        public string PhoneNumber { get; set; }
    }

}
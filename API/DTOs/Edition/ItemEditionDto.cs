﻿using System.ComponentModel.DataAnnotations;

namespace API.DTOs.Edition {

    public class ItemEditionDto {
        
        [Required]
        public int Id { get; set; }
        
        [Required]
        public bool Completed { get; set; }

        public int? ProcessId { get; set; }
    }

}
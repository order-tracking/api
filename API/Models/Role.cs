using System.Collections.Generic;
using System.Linq;

namespace API.Models
{
    /// <summary>
    /// Application Roles
    /// 
    /// Each role inherits the permissions of the child roles
    /// Parent role > Child role
    /// 
    /// Owner > Admin > Employee > Distributor
    /// </summary>
    public class Role
    {
        /// <summary>
        /// Order distributor
        /// 
        /// Only can access some endpoints which are necessary for the mobile app:
        /// - Incomplete journeys designated to him
        /// - Journey details
        /// - Order details
        /// </summary>
        public const string Distributor = "Distributor";

        /// <summary>
        /// Restaurant Employee
        /// 
        /// Can access and manage:
        /// - Orders
        /// - Journeys
        /// - Pipelines
        /// - Products
        /// </summary>
        public const string Employee = "Employee";

        /// <summary>
        /// Trustworthy employees
        /// 
        /// Can access and manage:
        /// - Processes
        /// - Customers
        /// - Employees (cannot create/delete other admins or owners)
        /// - Campaigns
        /// - Reports
        /// </summary>
        public const string Admin = "Admin";

        /// <summary>
        /// Restaurant Manager, generally one per Restaurant
        /// 
        /// Can do everything in the context of the restaurant, additions to the Admin role:
        /// - Can create/delete Admins
        /// - Can edit the Restaurant settings
        /// </summary>
        public const string Owner = "Owner";

        /// <summary>
        /// Project Developer
        /// 
        /// Internal Role (Full Access)
        /// - Can create and delete Restaurants
        /// - Can create Owner Employees
        /// </summary>
        public const string Developer = "Developer";
    }

    /// <summary>
    /// Roles Hierarchy
    /// 
    /// This method was choosed over policies because is simplier and did the job
    /// 
    /// These constants are used in the controllers and endpoint handlers to restrict access
    /// example: [Authorize(Roles = Roles.AdminPermission)]
    /// 
    /// This is because ASP.NET Core supports passing multiple roles to the Authorize attribute
    /// example: [Authorize(Roles = "Owner,Admin")] => this means Owner || Admin
    /// 
    /// If we had many roles this was not a good solution, but since we only have a 
    /// few there is no problem
    /// </summary>
    public class Roles
    {
        public const string DistributorPermission = "Developer,Owner,Admin,Employee,Distributor";
        public const string EmployeePermission = "Developer,Owner,Admin,Employee";
        public const string AdminPermission = "Developer,Owner,Admin";
        public const string OwnerPermission = "Developer,Owner";
        public const string DeveloperPermission = "Developer";

        /// <summary>
        /// Check if a given role is a child role of another role
        /// </summary>
        /// <param name="role"></param>
        /// <param name="roleToCheck"></param>
        /// <returns></returns>
        public static bool IsChildRole(string role, string roleToCheck)
        {
            var rolesHierarchy = new Dictionary<string, string[]>() {
                {Role.Distributor, new string[]{}}, // no child roles
                {Role.Employee, new[]{Role.Distributor}},
                {Role.Admin, new[]{Role.Employee, Role.Distributor}},
                {Role.Owner, new[]{Role.Admin, Role.Employee, Role.Distributor}},
                {Role.Developer, new[]{Role.Owner,Role.Admin, Role.Employee, Role.Distributor}},
            };

            string[] childRoles;
            if (!rolesHierarchy.TryGetValue(role, out childRoles))
            {
                return false; // not a valid role if does not exist in the dictionary
            }

            return childRoles.Contains(roleToCheck);
        }
    }

}

﻿using System;

namespace API.Models
{
    public class Report
    {
        public DateTime From { get; set; }
        public DateTime To { get; set; }

        public int TotalOrders { get; set; }
        public int TotalJourneys { get; set; }
        public int TotalEmployees { get; set; }
        public int TotalCustomers { get; set; }
        public int TotalProducts { get; set; }
        public int TotalFeedbacks { get; set; }
        public int TotalCampaigns { get; set; }
        public int TotalPipelines { get; set; }
        public int TotalProcesses { get; set; }
    }
}

using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace API.Models {

    public class Journey {
        public int Id { get; set; }

        public bool Completed { get; set; }
        
        public bool OnGoing { get; set; }

        public int CompanyId { get; set; }
        
        [ForeignKey("CompanyId")]
        public Company Company { get; set; }

        public int EmployeeId { get; set; }

        [ForeignKey("EmployeeId")]
        public Employee Employee { get; set; }

        [Required]
        public DateTime CreatedAt { get; set; }
        
        public DateTime? FinishedAt { get; set; }

        public virtual List<Order> Orders { get; set; }
    }

}

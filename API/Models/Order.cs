using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using API.EventHandlers;
using API.Utils;
using Microsoft.Extensions.Logging;

namespace API.Models {

    public class Order {
        public int Id { get; set; }
        
        // Meters from the store to the address
        public int Distance { get; set; }
        
        // Minutes
        public int ConfectionTime { get; set; }
        
        // Minutes
        public int JourneyDuration { get; set; }

        /// <summary>
        /// DateTime.UtcNow + ConfectionTime + JourneyDuration
        /// </summary>
        [Required]
        public DateTime EstimatedDate { get; set; }

        // Assembly arranged
        [Required]
        public bool ReadyToDistribute { get; set; }
        
        // All order items are done
        [Required]
        public bool ItemsComplete { get; set; }
        
        [Required]
        public bool OnArrivingNotified { get; set; }

        [Required]
        public string Address { get; set; }
        
        public string Address2 { get; set; }

        [Required]
        public string PostalCode { get; set; }

        public double Latitude { get; set; }
        
        public double Longitude { get; set; }
        
        [Required]
        public DateTime CreatedAt { get; set; }
        
        [Required]
        public bool OnGoing { get; set; }
        
        // Delivery Date
        public DateTime? DeliveredAt { get; set; }

        public int CompanyId { get; set; }

        [ForeignKey("CompanyId")]
        public Company Company { get; set; }

        public int? JourneyId { get; set; }

        [ForeignKey("JourneyId")]
        public Journey Journey { get; set; }

        public int CustomerId { get; set; }

        [ForeignKey("CustomerId")]
        public Customer Customer { get; set; }

        public int? FeedbackId { get; set; }

        [ForeignKey("FeedbackId")]
        public Feedback Feedback { get; set; }

        public List<Item> Items { get; set; }

        
        public void AddEstimatedTime(int confectionMinutes, int journeyMinutes)
        {
            var totalMinutes = confectionMinutes + journeyMinutes;

            EstimatedDate = DateTime.UtcNow.Add(new TimeSpan(0, 0, totalMinutes, 0));
            ConfectionTime = confectionMinutes;
            JourneyDuration = journeyMinutes;
        }

        public void UpdateEstimatedTime(int journeyMinutes)
        {
            EstimatedDate = DateTime.UtcNow.Add(new TimeSpan(0, 0, journeyMinutes, 0));
            JourneyDuration = journeyMinutes;
        }
        
        public int UpdateEstimatedTimeBasedOnETA(string utcDate)
        {
            if(utcDate == null) return 0;

            var minutes = TimeUtils.GetMinutesToDate(utcDate);
            
            UpdateEstimatedTime(minutes);

            return minutes;
        }
    }

}

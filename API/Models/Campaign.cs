﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace API.Models
{
    public class Campaign
    {
        public int Id { get; set; }
        
        [Required]
        public string Title { get; set; }
        
        // Sms body
        [Required]
        public string Message { get; set; }
        
        // Total success sms sent
        public int TotalSmsSent { get; set; }
        
        // The number of customers that should receive the campaign at the time
        public int TotalCustomers { get; set; }
        
        [Required]
        public DateTime CreatedAt { get; set; }
        
        public DateTime? FinishedAt { get; set; }
        
        public int CompanyId { get; set; }

        [ForeignKey("CompanyId")]
        public Company Company { get; set; }
    }
}

using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace API.Models {

    public class Product {
        public int Id { get; set; }

        [Required]
        public string Name { get; set; }

        public string Photo { get; set; }
        
        public int CompanyId { get; set; }
        
        [ForeignKey("CompanyId")]
        public Company Company { get; set; }

        public int? PipelineId { get; set; }

        [ForeignKey("PipelineId")]
        public ProcessPipeline Pipeline { get; set; }

        public int ProductGroupId { get; set; }

        [ForeignKey("ProductGroupId")]
        public ProductGroup ProductGroup { get; set; }

        public List<ProductType> ProductTypes { get; set; }

        public List<Item> Items { get; set; }
    }

}
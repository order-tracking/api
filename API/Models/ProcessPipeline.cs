using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace API.Models {

    public class ProcessPipeline {
        public int Id { get; set; }

        [Required]
        public string Name { get; set; }

        public int CompanyId { get; set; }

        [ForeignKey("CompanyId")]
        public Company Company { get; set; }
        
        public List<Process> Processes { get; set; }

//        public List<EmployeeProcessPipeline> EmployeeProcessPipelines { get; set; }

        public List<Item> Items { get; set; }
    }

}
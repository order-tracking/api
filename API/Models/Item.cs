using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using EntityFrameworkCore.Triggers;
using Microsoft.EntityFrameworkCore;

namespace API.Models {

    public class Item {
        public int Id { get; set; }

        public string Note { get; set; }

        public int Quantity { get; set; }

        public bool Completed { get; set; }

        public int OrderId { get; set; }

        [ForeignKey("OrderId")]
        public Order Order { get; set; }

        public int? ProductTypeId { get; set; }

        [ForeignKey("ProductTypeId")]
        public ProductType ProductType { get; set; }

        public int ProductId { get; set; }

        [ForeignKey("ProductId")]
        public Product Product { get; set; }

        public int? ProcessId { get; set; }

        [ForeignKey("ProcessId")]
        public Process Process { get; set; }

        static Item() {
            Triggers<Item>.Updated += e => {
                Order order = e.Context.Set<Order>()
                    .Where(o => o.Id == e.Entity.OrderId)
                    .Include(o => o.Items)
                    .First();

                bool itemsComplete = true;
                order.Items.ForEach(item => {
                    if (!item.Completed) {
                        itemsComplete = false;
                    }
                });

                order.ItemsComplete = itemsComplete;
                e.Context.SaveChanges();
            };
        }
    }

}

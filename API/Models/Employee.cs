using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace API.Models {

    public class Employee {
        public int Id { get; set; }

        [Required]
        public string Name { get; set; }
        
        [Required]
        public string PhoneNumber { get; set; }
        
        /// <summary>
        /// Hashed with bcrypt
        /// </summary>
        [Required]
        public string HashedPassword { get; set; }

        public int CompanyId { get; set; }
        
        /// <summary>
        /// Used to track the employess working, updated when the user is in a pipeline view
        /// </summary>
        public DateTime? LastTimeOnline { get; set; }
        
        /// <summary>
        /// Used to check if the employee is on a journey, updated by the events
        /// </summary>
        public bool Distributing { get; set; }

        [ForeignKey("CompanyId")]
        public Company Company { get; set; }

        [Required]
        public string Role { get; set; }

        public List<Process> Processes { get; set; }

        public List<Journey> Journeys { get; set; }

//        public List<EmployeeProcessPipeline> EmployeeProcessPipelines { get; set; }
    }

//    public class EmployeeProcessPipeline {
//        public int EmployeeId { get; set; }
//
//        [ForeignKey("EmployeeId")]
//        public Employee Employee { get; set; }
//
//        public int PipelineId { get; set; }
//
//        [ForeignKey("PipelineId")]
//        public ProcessPipeline Pipeline { get; set; }
//    }

}

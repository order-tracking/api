﻿namespace API.Models
{
    public class AddressModel
    {
        public string Address { get; set; }
        
        public string Address2 { get; set; }
        
        public string PostalCode { get; set; }
        
        public string Country { get; set; }
    }
}

using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace API.Models {

    public class ProductGroup {
        public int Id { get; set; }

        [Required]
        public string Name { get; set; }

        public int CompanyId { get; set; }

        [ForeignKey("CompanyId")]
        public Company Company { get; set; }

        public int? PreviousProductGroupId { get; set; }

        [ForeignKey("PreviousProductGroupId")]
        public ProductGroup PreviousProductGroup { get; set; }

        public List<ProductGroup> NextProductGroups { get; set; }

        public List<Product> Products { get; set; }
    }

}
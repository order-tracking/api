﻿using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace API.Models {
    
    public enum Rating {
        Horrible = 1,
        NeedsImprovement = 2,
        Okay = 3,
        Good = 4,
        Excellent = 5
    }

    public class Feedback {
        
        public int Id { get; set; }

        public Rating Rating { get; set; }

        public string Note { get; set; }
        
        public DateTime CreatedAt { get; set; }
        
        public int CompanyId { get; set; }
        
        [ForeignKey("CompanyId")]
        public Company Company { get; set; }

        public int OrderId { get; set; }

        [ForeignKey("OrderId")]
        public Order Order { get; set; }
    }

}

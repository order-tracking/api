using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace API.Models {

    /// <summary>
    /// Represents a Restaurant/Store
    /// </summary>
    public class Company {
        public int Id { get; set; }

        [Required]
        public string Name { get; set; }
        
        [Required]
        public string Address { get; set; }

        [Required]
        public string PostalCode { get; set; }
        
        [Required]
        public string CountryCode { get; set; }

        [Required]
        public int MaxAllowedDistance { get; set; }

        public List<Customer> Customers { get; set; }

        public List<Order> Orders { get; set; }

        public List<Employee> Employees { get; set; }
    }

}

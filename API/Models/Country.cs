﻿namespace API.Models
{
    public class Country
    {
        public string Code { get; set; }
        public string Name { get; set; }
        public string PhonePrefix { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;

namespace API.Models
{
    public class CountryList
    {
        /// <summary>
        /// List of supported countries
        /// </summary>
        private static Dictionary<string, Country> _map =
            new Dictionary<string, Country>() {
                {
                    "pt",
                    new Country() {Code = "pt", Name = "Portugal", PhonePrefix = "+351"}
                }
            };


        public static Country Get(string code)
        {
            Country c;
            if (_map.TryGetValue(code, out c))
            {
                return c;
            }
            else
            {
                throw new Exception("Country not supported yet.");
            }
        }
    }
}

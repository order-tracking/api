namespace API.Models
{
    public class Stats
    {
        public int PendingOrders { get; set; }

        public int DistributorsWorking { get; set; }

        public int EmployeesWorking { get; set; }

        public int OrdersCompletedToday { get; set; }
    }
}

using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace API.Models {

    public class Process {
        public int Id { get; set; }

        [Required]
        public int OrderNumber { get; set; }

        [Required]
        public string Name { get; set; }
        
        public int CompanyId { get; set; }
        
        [ForeignKey("CompanyId")]
        public Company Company { get; set; }

        public int PipelineId { get; set; }

        [ForeignKey("PipelineId")]
        public ProcessPipeline Pipeline { get; set; }

        public List<Item> Items { get; set; }
    }

}
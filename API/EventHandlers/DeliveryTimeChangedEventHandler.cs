﻿using System;
using System.Threading.Tasks;
using API.Data.Repository.Generic;
using API.Events;
using API.Services.SmsSender;
using MediatR;
using Microsoft.Extensions.Logging;

namespace API.EventHandlers
{

    /// <summary>
    /// This handler is called when the event DeliveryDelayedEvent is triggered
    /// 
    /// His job is to calculate a new estimated time caused by the delay
    /// </summary>
    public class DeliveryTimeChangedEventHandler
        : IAsyncNotificationHandler<DeliveryTimeChangedEvent>
    {
        private readonly ILogger<DeliveryTimeChangedEventHandler> _logger;
        private readonly IOrdersRepository _ordersRepository;

        public DeliveryTimeChangedEventHandler(
            ILogger<DeliveryTimeChangedEventHandler> logger,
            IOrdersRepository ordersRepository)
        {
            _logger = logger;
            _ordersRepository = ordersRepository;
        }

        public async Task Handle(DeliveryTimeChangedEvent evt)
        {
            var order = evt.Order;
            
            _logger.LogWarning("Updating Estimated Delivery Time");
            _logger.LogWarning("Received: " + evt.Dto.data.@object.eta);
            
            _logger.LogWarning("Before: " + order.EstimatedDate);
            
            // Update Estimated Delivery Time
            var minutes = order.UpdateEstimatedTimeBasedOnETA(evt.Dto.data.@object.eta);
            order.OnGoing = true;
            
            _logger.LogWarning("Minutes: " + minutes + ", After: " + order.EstimatedDate);
            
            await _ordersRepository.Update(order);
        }
    }
}

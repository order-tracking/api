﻿using System;
using System.Threading.Tasks;
using API.Data.Repository.Generic;
using API.Events;
using API.Services.SmsSender;
using API.Utils;
using MediatR;
using Microsoft.Extensions.Logging;

namespace API.EventHandlers
{
    /// <summary>
    /// This handler is called when the event DistributorIsOnTheWayEvent is triggered
    /// 
    /// His job is to send a sms to the customer and update the estimated delivery time
    /// with the new trip ETA received from the Hypertrack event
    /// </summary>
    public class DistributorIsOnTheWayEventHandler
        : IAsyncNotificationHandler<DistributorIsOnTheWayEvent>
    {
        private readonly ISmsSender _smsSender;
        private readonly ICompaniesRepository _companiesRepository;
        private readonly IOrdersRepository _ordersRepository;
        private readonly IJourneysRepository _journeysRepository;
        private readonly ILogger<DistributorIsOnTheWayEventHandler> _logger;

        public DistributorIsOnTheWayEventHandler(
            ISmsSender smsSender,
            ICompaniesRepository companiesRepository,
            IOrdersRepository ordersRepository,
            IJourneysRepository journeysRepository,
            ILogger<DistributorIsOnTheWayEventHandler> logger)
        {
            _smsSender = smsSender;
            _companiesRepository = companiesRepository;
            _ordersRepository = ordersRepository;
            _journeysRepository = journeysRepository;
            _logger = logger;
        }

        public async Task Handle(DistributorIsOnTheWayEvent evt)
        {
            // If this order was already processed for this event don't do anything
            if (evt.Order.OnGoing)
            {
                _logger.LogWarning("Already processed, exiting.");
                return;
            }

            // Update Estimated Delivery Time
            var order = evt.Order;
            
            _logger.LogWarning("Updating Estimated Delivery Time");
            _logger.LogWarning("Received: " + evt.Dto.data.@object.eta);
            
            _logger.LogWarning("Before: " + order.EstimatedDate);
            var minutes = order.UpdateEstimatedTimeBasedOnETA(evt.Dto.data.@object.eta);
            order.OnGoing = true;
            
            _logger.LogWarning("Minutes: " + minutes + ", After: " + order.EstimatedDate);
            await _ordersRepository.Update(order);
            
            // Mark the journey as on going too
            var journey = await _journeysRepository.GetJourney(order.JourneyId.Value);

            if (!journey.OnGoing)
            {
                journey.OnGoing = true;
                await _journeysRepository.Update(journey);
            }

            // Send sms to the customer
            var body = "Your order is on its way to your address.";

            if (minutes > 0)
            {
                body += " Estimated delivery time: " + minutes + " minutes.";
            }
            
            try
            {
                var company = await _companiesRepository.Get(evt.Order.CompanyId);
                var number =
                    PhoneUtils.Parse(evt.Customer.PhoneNumber, company.CountryCode);

                var smsbody =
                    string.Format(body, TimeUtils.GetHours(order.EstimatedDate));
                
                _logger.LogWarning("SMS Body: {0}", smsbody);
                var res = await _smsSender.SendSmsAsync(number, smsbody);

                if (!res)
                {
                    _logger.LogCritical("Failed to send a sms! {0} ({1})",
                        evt.Customer.Name, number);
                }
                else
                {
                    _logger.LogWarning("SMS Sent to: {0} ({1})", evt.Customer.Name, number);
                }
            }
            catch (Exception e)
            {
                _logger.LogCritical(
                    "Exception when sending a sms! Message: " + e.Message);
            }
        }
    }
}

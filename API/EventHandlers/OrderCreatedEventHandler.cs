﻿using System;
using System.Threading.Tasks;
using API.Data.Repository.Generic;
using API.Events;
using API.Services.SmsSender;
using API.Utils;
using MediatR;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;

namespace API.EventHandlers {

    public class OrderCreatedEventHandler
        : IAsyncNotificationHandler<OrderCreatedEvent> {
        private readonly ISmsSender _smsSender;
        private readonly ICompaniesRepository _companiesRepository;
        private readonly ILogger<OrderCreatedEventHandler> _logger;
        private readonly IConfiguration _configuration;

        public OrderCreatedEventHandler(
            ISmsSender smsSender,
            ICompaniesRepository companiesRepository,
            ILogger<OrderCreatedEventHandler> logger,
            IConfiguration configuration) {
            _smsSender = smsSender;
            _companiesRepository = companiesRepository;
            _logger = logger;
            _configuration = configuration;
        }

        public async Task Handle(OrderCreatedEvent evt) {
            string url = _configuration["CLIENT_APP_URL"];

            // Send SMS to the customer
            var body = "Thank you for your order! Tracking: "
                       + url + "/" + HashIdsUtils.Encode(evt.Order.Id);

            _logger.LogWarning("SMS Body: {0}", body);
            
            try {
                var company = await _companiesRepository.Get(evt.Order.CompanyId);
                var number =
                    PhoneUtils.Parse(evt.Order.Customer.PhoneNumber, company.CountryCode);
                
                var res = await _smsSender.SendSmsAsync(number,
                    string.Format(body));

                if (!res)
                {
                    _logger.LogCritical("Failed to send a sms! {0} ({1})", evt.Order.Customer.Name, number);
                }
                else
                {
                    _logger.LogWarning("SMS Sent to: {0} ({1})", evt.Order.Customer.Name, number);
                }
            }
            catch (Exception e) {
                _logger.LogCritical(
                    "Exception when sending a sms! Message: " + e.Message);
            }
        }
    }

}

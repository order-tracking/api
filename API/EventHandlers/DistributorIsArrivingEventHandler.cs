﻿using System;
using System.Threading.Tasks;
using API.Data.Repository.Generic;
using API.Events;
using API.Services.SmsSender;
using API.Utils;
using MediatR;
using Microsoft.Extensions.Logging;

namespace API.EventHandlers
{
    /// <summary>
    /// This handler is called when the event DistributorIsArrivingEvent is triggered
    /// 
    /// His job is to send a sms to the customer to say that the distributor is arriving
    /// </summary>
    public class DistributorIsArrivingEventHandler
        : IAsyncNotificationHandler<DistributorIsArrivingEvent>
    {
        private readonly ISmsSender _smsSender;
        private readonly IOrdersRepository _ordersRepository;
        private readonly ICompaniesRepository _companiesRepository;
        private readonly ILogger<DistributorIsArrivingEventHandler> _logger;

        public DistributorIsArrivingEventHandler(
            ISmsSender smsSender,
            IOrdersRepository ordersRepository,
            ICompaniesRepository companiesRepository,
            ILogger<DistributorIsArrivingEventHandler> logger)
        {
            _smsSender = smsSender;
            _ordersRepository = ordersRepository;
            _companiesRepository = companiesRepository;
            _logger = logger;
        }

        public async Task Handle(DistributorIsArrivingEvent evt)
        {
            // If this order was already processed for this event don't do anything
            if (evt.Order.OnArrivingNotified || evt.Order.DeliveredAt != null)
            {
                _logger.LogWarning("Already processed, exiting.");
                return;
            }

            // Update order flag
            var order = evt.Order;
            order.OnArrivingNotified = true;

            _logger.LogWarning("Updating Estimated Delivery Time");
            _logger.LogWarning("Received: " + evt.Dto.data.@object.eta);

            _logger.LogWarning("Before: " + order.EstimatedDate);
            var minutes = order.UpdateEstimatedTimeBasedOnETA(evt.Dto.data.@object.eta);

            _logger.LogWarning("Minutes: " +
                                   minutes +
                                   ", After: " +
                                   order.EstimatedDate);
            await _ordersRepository.Update(order);

            // Send sms to the customer
            var body = "Your order is about to arrive. Get ready.";

            try
            {
                var company = await _companiesRepository.Get(evt.Order.CompanyId);
                var number =
                    PhoneUtils.Parse(evt.Customer.PhoneNumber, company.CountryCode);

                var res = await _smsSender.SendSmsAsync(number, body);

                if (!res)
                {
                    _logger.LogCritical("Failed to send a sms! {0} ({1})",
                        evt.Customer.Name, number);
                }
                else
                {
                    _logger.LogWarning("SMS Sent to: {0} ({1})", evt.Customer.Name,
                        number);
                }
            }
            catch (Exception e)
            {
                _logger.LogCritical("Exception when sending a sms! Message: " +
                                    e.Message);
            }
        }
    }
}

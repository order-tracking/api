﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using API.Data.Repository.Generic;
using API.Events;
using API.Services.SmsSender;
using API.Utils;
using MediatR;
using Microsoft.Extensions.Logging;

namespace API.EventHandlers
{
    /// <summary>
    /// This handler is called when the event SendNewCampaignEvent is launched
    /// 
    /// His job is to send an sms to every customer in the Company with campaign message
    /// </summary>
    public class SendNewCampaignEventHandler
        : IAsyncNotificationHandler<SendNewCampaignEvent>
    {
        private readonly ICustomerRepository _customerRepository;
        private readonly ICompaniesRepository _companiesRepository;
        private readonly ICampaignRepository _campaignRepository;
        private readonly ISmsSender _smsSender;
        private readonly ILogger<SendNewCampaignEventHandler> _logger;

        public SendNewCampaignEventHandler(
            ICustomerRepository customerRepository,
            ICompaniesRepository companiesRepository,
            ICampaignRepository campaignRepository,
            ISmsSender smsSender, ILogger<SendNewCampaignEventHandler> logger)
        {
            _customerRepository = customerRepository;
            _companiesRepository = companiesRepository;
            _campaignRepository = campaignRepository;
            _smsSender = smsSender;
            _logger = logger;
        }

        /// <summary>
        /// Send an sms to every customer of the company
        /// Start all requests at the same time and then wait for all
        /// 
        /// This task can make a large number of requests at the same time, a better option
        /// is to divide the list and send a chunk of requests with some delay
        /// </summary>
        /// <param name="evt"></param>
        /// <returns></returns>
        public async Task Handle(SendNewCampaignEvent evt)
        {
            var campaign = evt.Campaign;
            var users = await _customerRepository.GetAllCustomersOfCompany(campaign.CompanyId);
            var company = await _companiesRepository.Get(evt.Campaign.CompanyId);
            
            _logger.LogWarning("Sending SMS to {0} customers", users.Count);
            var tasks = new List<Task<bool>>();
            
            users.ForEach(customer =>
            {
                var number =
                    PhoneUtils.Parse(customer.PhoneNumber, company.CountryCode);
                
                tasks.Add(_smsSender.SendSmsAsync(number, campaign.Message));
                _logger.LogWarning("Started sending to {0} ({1})", customer.Name, number);
            });

            campaign.TotalCustomers = users.Count;
            campaign.TotalSmsSent = users.Count; // YOLO

            await Task.WhenAll(tasks); // Wait for all
            _logger.LogWarning("All Sent!");
            
            campaign.FinishedAt = DateTime.UtcNow;
            
            await _campaignRepository.Update(campaign);
        }
    }
}

﻿using System;
using System.Linq;
using System.Threading.Tasks;
using API.Data.Repository.Generic;
using API.Events;
using MediatR;
using Microsoft.Extensions.Logging;

namespace API.EventHandlers
{
    /// <summary>
    /// This handler is called when the event OrderCompletedEvent is triggered
    /// 
    /// His job is to mark the order as completed by changing some flags
    /// </summary>
    public class OrderCompletedEventHandler
        : IAsyncNotificationHandler<OrderCompletedEvent>
    {
        private readonly IOrdersRepository _ordersRepository;
        private readonly ILogger<OrderCompletedEventHandler> _logger;
        private readonly IJourneysRepository _journeysRepository;

        public OrderCompletedEventHandler(
            IOrdersRepository ordersRepository,
            ILogger<OrderCompletedEventHandler> logger,
            IJourneysRepository journeysRepository)
        {
            _ordersRepository = ordersRepository;
            _logger = logger;
            _journeysRepository = journeysRepository;
        }

        public async Task Handle(OrderCompletedEvent evt)
        {
            // If this order was already processed for this event don't do anything
            if (evt.Order.DeliveredAt != null)
            {
                _logger.LogWarning("Already delivered, exiting.");
                return;
            }

            // Update order
            var order = evt.Order;
            order.OnGoing = false;
            order.DeliveredAt = DateTime.UtcNow;

            _logger.LogWarning("Order marked as delivered!");
            await _ordersRepository.Update(order);

            // Mark the journey as completed is needed

            var journey = await _journeysRepository.GetJourney(order.JourneyId.Value);

            var totalDelivered = journey.Orders.Count(o => o.DeliveredAt != null);

            if (journey.Orders.Count == totalDelivered)
            {
                journey.Completed = true;
                journey.FinishedAt = DateTime.UtcNow;
                journey.OnGoing = false;
                
                await _journeysRepository.Update(journey);
            }

        }
    }
}

﻿using System;
using System.Runtime.InteropServices.ComTypes;
using System.Threading.Tasks;
using API.Data.Repository.Generic;
using API.Events;
using API.Services.Hypertrack;
using API.Utils;
using MediatR;
using Microsoft.Extensions.Logging;

namespace API.EventHandlers
{
    /// <summary>
    /// This handler is called when the event EmployeeTrackingChangedEvent is launched
    /// (Started the mobile app)
    /// 
    /// His job is to change the Distributing flag in the employee
    /// (used to count the distributors available)
    /// </summary>
    public class EmployeeTrackingChangedEventHandler
        : IAsyncNotificationHandler<EmployeeTrackingChangedEvent>
    {
        private readonly IEmployeeRepository _employeeRepository;
        private readonly ILogger<EmployeeTrackingChangedEventHandler> _logger;

        public EmployeeTrackingChangedEventHandler(
            IEmployeeRepository employeeRepository,
            ILogger<EmployeeTrackingChangedEventHandler> logger)
        {
            _employeeRepository = employeeRepository;
            _logger = logger;
        }

        public async Task Handle(EmployeeTrackingChangedEvent e)
        {
            var dto = e.Dto;
            var employee =
                await _employeeRepository.GetEmployee(
                    int.Parse(dto.data.@object.lookup_id));

            employee.Distributing = dto.type.Contains("started");

            _logger.LogWarning("Changed employee {0} Distributing to {1}",
                employee.Name, employee.Distributing);

            await _employeeRepository.Update(employee);
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using API.Data;
using API.Models;
using API.Services.Hypertrack;
using API.Utils;
using Microsoft.EntityFrameworkCore;

namespace API.Extensions {

    /// <summary>
    /// Seed the staging database
    /// ONLY USED IN STAGING ENVIRONMENT
    /// </summary>
    public static class SeedDatabaseData {
        //
        // ─── CLEARS DATA FROM THE DATABASE ────────────────────────────────
        //
        public static void ClearAllData(this DatabaseContext context) {
            context.Database.ExecuteSqlCommand("DELETE FROM \"Items\"");
            context.Database.ExecuteSqlCommand(
                "ALTER SEQUENCE \"Items_Id_seq\" RESTART WITH 1");

            context.Database.ExecuteSqlCommand("DELETE FROM \"ProductTypes\"");
            context.Database.ExecuteSqlCommand(
                "ALTER SEQUENCE \"ProductTypes_Id_seq\" RESTART WITH 1");

            context.Database.ExecuteSqlCommand("DELETE FROM \"Orders\"");
            context.Database.ExecuteSqlCommand(
                "ALTER SEQUENCE \"Orders_Id_seq\" RESTART WITH 1");

            context.Database.ExecuteSqlCommand("DELETE FROM \"Products\"");
            context.Database.ExecuteSqlCommand(
                "ALTER SEQUENCE \"Products_Id_seq\" RESTART WITH 1");

            context.Database.ExecuteSqlCommand("DELETE FROM \"ProductGroups\"");
            context.Database.ExecuteSqlCommand(
                "ALTER SEQUENCE \"ProductGroups_Id_seq\" RESTART WITH 1");

            context.Database.ExecuteSqlCommand("DELETE FROM \"Processes\"");
            context.Database.ExecuteSqlCommand(
                "ALTER SEQUENCE \"Processes_Id_seq\" RESTART WITH 1");

            context.Database.ExecuteSqlCommand("DELETE FROM \"ProcessPipelines\"");
            context.Database.ExecuteSqlCommand(
                "ALTER SEQUENCE \"ProcessPipelines_Id_seq\" RESTART WITH 1");

            context.Database.ExecuteSqlCommand("DELETE FROM \"Employees\"");
            context.Database.ExecuteSqlCommand(
                "ALTER SEQUENCE \"Employees_Id_seq\" RESTART WITH 1");

            context.Database.ExecuteSqlCommand("DELETE FROM \"Customers\"");
            context.Database.ExecuteSqlCommand(
                "ALTER SEQUENCE \"Customers_Id_seq\" RESTART WITH 1");

            context.Database.ExecuteSqlCommand("DELETE FROM \"Companies\"");
            context.Database.ExecuteSqlCommand(
                "ALTER SEQUENCE \"Companies_Id_seq\" RESTART WITH 1");
        }

        //
        // ─── SEEDS THE DATABASE ──────────────────────────────────────────
        //
        public static void EnsureSeedDataForContext(
            this DatabaseContext context) {
            AddCompanies(context);

            AddCustomers(context);

            AddEmployees(context);

            AddProcessPipelinesWithProcesses(context);

            AddProductGroups(context);

            AddProductsWithItemTypes(context);

            AddOrdersWithItems(context);

            AddJourneys(context);

            AddOrdersFeedback(context);

            AddCampaigns(context);
        }

        private static void AddCompanies(DatabaseContext context) {
            Company[] companies = {
                new Company {
                    Name = "HdN",
                    Address = "R. Conselheiro Emídio Navarro, 1",
                    PostalCode = "1959-007",
                    MaxAllowedDistance = 100,
                    CountryCode = "pt"
                },
                new Company {
                    Name = "ISEL",
                    Address = "R. Conselheiro Emídio Navarro, 1",
                    PostalCode = "1959-007",
                    MaxAllowedDistance = 100,
                    CountryCode = "pt"
                },
                new Company {
                    Name = "oTrack Company",
                    Address = "Av. da Liberdade, 185",
                    PostalCode = "1269-050",
                    MaxAllowedDistance = 100,
                    CountryCode = "pt"
                }
            };

            context.Companies.AddRange(companies);

            context.SaveChanges();
        }

        private static void AddCustomers(DatabaseContext context) {
            var customers = new[] {
                new Customer {
                    CompanyId = 2,
                    Name = "Nuno Reis",
                    PhoneNumber = "914167555"
                },
                new Customer {
                    CompanyId = 2,
                    Name = "Carlos Florencio",
                    PhoneNumber = "925706007"
                },
                new Customer {
                    CompanyId = 2,
                    Name = "Ricardo Silva",
                    PhoneNumber = "925706008"
                },
                new Customer {
                    CompanyId = 2,
                    Name = "Paulo Alexandre",
                    PhoneNumber = "925706009"
                },
                new Customer {
                    CompanyId = 1,
                    Name = "Joao Gameiro",
                    PhoneNumber = "914167554"
                },
                new Customer {
                    CompanyId = 1,
                    Name = "Ines Castro",
                    PhoneNumber = "914167534"
                },
                new Customer {
                    CompanyId = 1,
                    Name = "Rafael Pereira",
                    PhoneNumber = "914167543"
                },
                new Customer {
                    CompanyId = 1,
                    Name = "Joao Leitao",
                    PhoneNumber = "914167589"
                }
            };

            context.Customers.AddRange(customers);

            context.SaveChanges();
        }

        private static void AddEmployees(DatabaseContext context) {
            Employee[] employees = new Employee[] {
                new Employee {
                    CompanyId = 1,
                    Name = "Nuno Reis",
                    PhoneNumber = "914167544",
                    HashedPassword = BCrypt.Net.BCrypt.HashPassword("password"),
                    Role = Role.Developer
                },
                new Employee {
                    CompanyId = 1,
                    Name = "Carlos Florencio",
                    PhoneNumber = "925706008",
                    HashedPassword = BCrypt.Net.BCrypt.HashPassword("password"),
                    Role = Role.Developer
                },
                new Employee {
                    CompanyId = 1,
                    Name = "Ricardo Cacheira",
                    PhoneNumber = "123456789",
                    HashedPassword = BCrypt.Net.BCrypt.HashPassword("password"),
                    Role = Role.Owner
                },
                new Employee {
                    CompanyId = 1,
                    Name = "Ricardo Silva",
                    PhoneNumber = "123456782",
                    HashedPassword = BCrypt.Net.BCrypt.HashPassword("password"),
                    Role = Role.Developer
                },
                new Employee {
                    CompanyId = 2,
                    Name = "Eduardo Baixo",
                    PhoneNumber = "123456783",
                    HashedPassword = BCrypt.Net.BCrypt.HashPassword("password"),
                    Role = Role.Owner
                },
                new Employee {
                    CompanyId = 2,
                    Name = "José António",
                    PhoneNumber = "123456784",
                    HashedPassword = BCrypt.Net.BCrypt.HashPassword("password"),
                    Role = Role.Employee
                },
                new Employee {
                    CompanyId = 3,
                    Name = "Nuno Reis",
                    PhoneNumber = "914167554",
                    HashedPassword = BCrypt.Net.BCrypt.HashPassword("password"),
                    Role = Role.Owner
                },
                new Employee {
                    CompanyId = 3,
                    Name = "Carlos Florencio",
                    PhoneNumber = "925706007",
                    HashedPassword = BCrypt.Net.BCrypt.HashPassword("password"),
                    Role = Role.Developer
                }
            };

            context.Employees.AddRange(employees);

            context.SaveChanges();
        }

        private static void AddProcessPipelinesWithProcesses(DatabaseContext context) {
            Process[] processes = new Process[] {
                new Process {
                    Name = "Preparação",
                    OrderNumber = 1,
                    CompanyId = 1
                },
                new Process {
                    Name = "Forno",
                    OrderNumber = 2,
                    CompanyId = 1
                },
                new Process {
                    Name = "Preparação",
                    OrderNumber = 1,
                    CompanyId = 1
                },
                new Process {
                    Name = "Corte",
                    OrderNumber = 2,
                    CompanyId = 1
                },
                new Process {
                    Name = "Preparação",
                    OrderNumber = 1,
                    CompanyId = 1
                },
                new Process {
                    Name = "Corte",
                    OrderNumber = 2,
                    CompanyId = 1
                },
                new Process {
                    Name = "Grelhando",
                    OrderNumber = 3,
                    CompanyId = 1
                }
            };

            ProcessPipeline[] processPipelines = new ProcessPipeline[] {
                new ProcessPipeline {
                    CompanyId = 1,
                    Name = "Pizza",
                    Processes = new List<Process>(
                        new[] {
                            processes[0],
                            processes[1]
                        })
                },
                new ProcessPipeline {
                    CompanyId = 1,
                    Name = "Sushi",
                    Processes = new List<Process>(
                        new[] {
                            processes[2],
                            processes[3]
                        })
                },
                new ProcessPipeline {
                    CompanyId = 1,
                    Name = "Churrasco",
                    Processes = new List<Process>(
                        new[] {
                            processes[4],
                            processes[5],
                            processes[6]
                        })
                },
            };

            context.ProcessPipelines.AddRange(processPipelines);

            context.SaveChanges();
        }

        private static void AddProductGroups(DatabaseContext context) {
            ProductGroup[] productGroups = new ProductGroup[] {
                new ProductGroup {
                    CompanyId = 1,
                    Name = "Comidas"
                },
                new ProductGroup {
                    CompanyId = 1,
                    Name = "Bebidas"
                },
                new ProductGroup {
                    CompanyId = 1,
                    Name = "Sobremesas"
                },
                new ProductGroup {
                    CompanyId = 1,
                    Name = "Carnes",
                    PreviousProductGroupId = 1
                },
                new ProductGroup {
                    CompanyId = 1,
                    Name = "Pizzas",
                    PreviousProductGroupId = 1
                },
                new ProductGroup {
                    CompanyId = 1,
                    Name = "Peixes",
                    PreviousProductGroupId = 1
                },
                new ProductGroup {
                    CompanyId = 1,
                    Name = "Vegetarianos",
                    PreviousProductGroupId = 1
                },
                new ProductGroup {
                    CompanyId = 1,
                    Name = "Refrigerantes",
                    PreviousProductGroupId = 2
                },
                new ProductGroup {
                    CompanyId = 1,
                    Name = "Chás",
                    PreviousProductGroupId = 2
                },
                new ProductGroup {
                    CompanyId = 1,
                    Name = "Água",
                    PreviousProductGroupId = 2
                },
            };

            context.ProductGroups.AddRange(productGroups);

            context.SaveChanges();
        }

        private static void AddProductsWithItemTypes(DatabaseContext context) {
            Product[] products = new Product[] {
                new Product {
                    // ID - 1
                    Name = "Picanha",
                    CompanyId = 1,
                    PipelineId = 3,
                    ProductGroupId = 4,
                    ProductTypes = new List<ProductType>(
                        new[] {
                            new ProductType {Name = "Bem Passado"},
                            new ProductType {Name = "Médio"},
                            new ProductType {Name = "Mal Passado"}
                        })
                },
                new Product {
                    // ID - 2
                    Name = "Maminha",
                    CompanyId = 1,
                    PipelineId = 3,
                    ProductGroupId = 4,
                    ProductTypes = new List<ProductType>(
                        new[] {
                            new ProductType {Name = "Bem Passado"},
                            new ProductType {Name = "Médio"},
                            new ProductType {Name = "Mal Passado"}
                        })
                },
                new Product {
                    // ID - 3
                    Name = "Bife de Frango",
                    CompanyId = 1,
                    PipelineId = 3,
                    ProductGroupId = 4,
                    ProductTypes = new List<ProductType>(
                        new[] {
                            new ProductType {Name = "Bem Passado"},
                            new ProductType {Name = "Médio"},
                            new ProductType {Name = "Mal Passado"}
                        })
                },
                new Product {
                    // ID - 4
                    Name = "Pizza",
                    CompanyId = 1,
                    PipelineId = 1,
                    ProductGroupId = 5,
                    ProductTypes = new List<ProductType>(
                        new[] {
                            new ProductType {Name = "Grande"},
                            new ProductType {Name = "Média"},
                            new ProductType {Name = "Pequena"}
                        })
                },
                new Product {
                    // ID - 5
                    Name = "Calzone",
                    CompanyId = 1,
                    PipelineId = 1,
                    ProductGroupId = 5,
                    ProductTypes = new List<ProductType>(
                        new[] {
                            new ProductType {Name = "Grande"},
                            new ProductType {Name = "Média"},
                            new ProductType {Name = "Pequena"}
                        })
                },
                new Product {
                    // ID - 6
                    Name = "Coca-Cola",
                    CompanyId = 1,
                    ProductGroupId = 8,
                    ProductTypes = new List<ProductType>(
                        new[] {
                            new ProductType {Name = "30 cl"},
                            new ProductType {Name = "50 cl"},
                            new ProductType {Name = "1 L"}
                        })
                },
                new Product {
                    // ID - 7
                    Name = "Iced Tea",
                    CompanyId = 1,
                    ProductGroupId = 8,
                    ProductTypes = new List<ProductType>(
                        new[] {
                            new ProductType {Name = "30 cl"},
                            new ProductType {Name = "50 cl"},
                            new ProductType {Name = "1 L"}
                        })
                },
                new Product {
                    // ID - 8
                    Name = "Chá de Tílias",
                    CompanyId = 1,
                    ProductGroupId = 9,
                    ProductTypes = new List<ProductType>(
                        new[] {
                            new ProductType {Name = "30 cl"},
                            new ProductType {Name = "50 cl"},
                            new ProductType {Name = "1 L"}
                        })
                },
                new Product {
                    // ID - 9
                    Name = "Petit Gateau",
                    CompanyId = 1,
                    ProductGroupId = 3,
                    ProductTypes = new List<ProductType>(
                        new[] {
                            new ProductType {Name = "Duas pessoas"},
                            new ProductType {Name = "Uma pessoa"}
                        })
                },
                new Product {
                    // ID - 10
                    Name = "Semi-Frio",
                    CompanyId = 1,
                    ProductGroupId = 3,
                    ProductTypes = new List<ProductType>(
                        new[] {
                            new ProductType {Name = "Duas pessoas"},
                            new ProductType {Name = "Uma pessoa"}
                        })
                },
                new Product {
                    // ID - 11
                    Name = "Cheesecake",
                    CompanyId = 1,
                    ProductGroupId = 3,
                    ProductTypes = new List<ProductType>(
                        new[] {
                            new ProductType {Name = "Duas pessoas"},
                            new ProductType {Name = "Uma pessoa"}
                        })
                }
            };

            context.Products.AddRange(products);

            context.SaveChanges();
        }

        private static void AddOrdersWithItems(DatabaseContext context) {
            List<Product> products = context.Products.ToList();

            Item[] items = new Item[] {
                new Item {
                    Quantity = 1,
                    OrderId = 1,
                    ProcessId = 2,
                    Completed = true,
                    Product = products[3],
                    ProductType = products[3].ProductTypes[0]
                },
                new Item {
                    Quantity = 1,
                    OrderId = 1,
                    Completed = true,
                    Product = products[10],
                    ProductType = products[10].ProductTypes[0]
                },
                new Item {
                    Quantity = 1,
                    OrderId = 1,
                    Completed = true,
                    Product = products[5],
                    ProductType = products[5].ProductTypes[0]
                },

                new Item {
                    Quantity = 1,
                    OrderId = 2,
                    ProcessId = 6,
                    Completed = false,
                    Product = products[0],
                    ProductType = products[0].ProductTypes[0]
                },

                new Item {
                    Quantity = 1,
                    OrderId = 3,
                    ProcessId = 2,
                    Completed = false,
                    Product = products[4],
                    ProductType = products[4].ProductTypes[0]
                },

                new Item {
                    Quantity = 1,
                    OrderId = 4,
                    Completed = true,
                    Product = products[8],
                    ProductType = products[8].ProductTypes[0]
                },

                new Item {
                    Quantity = 1,
                    OrderId = 1,
                    Completed = true,
                    Product = products[3],
                    ProductType = products[3].ProductTypes[0]
                },

                new Item {
                    Quantity = 1,
                    OrderId = 4,
                    Completed = true,
                    Product = products[8],
                    ProductType = products[8].ProductTypes[0]
                },

                new Item {
                    Quantity = 1,
                    OrderId = 4,
                    Completed = true,
                    Product = products[8],
                    ProductType = products[8].ProductTypes[0]
                }
            };

            Order[] orders = new Order[] {
                new Order {
                    CustomerId = 5,
                    CompanyId = 1,
                    EstimatedDate = DateTime.UtcNow.Add(new TimeSpan(0, 0, 20, 0)),
                    ReadyToDistribute = true,
                    ItemsComplete = true,
                    OnGoing = false,
                    CreatedAt = DateTime.UtcNow,
                    Address = "Rua tito morais, 14",
                    Address2 = "1º Esq.",
                    PostalCode = "1234-678",
                    Items = new List<Item>(
                        new[] {
                            items[0],
                            items[1],
                            items[2]
                        })
                },
                new Order {
                    CustomerId = 5,
                    CompanyId = 1,
                    EstimatedDate = DateTime.UtcNow.Add(new TimeSpan(0, 0, 15, 0)),
                    ReadyToDistribute = false,
                    ItemsComplete = false,
                    OnGoing = false,
                    CreatedAt = DateTime.UtcNow,
                    Address = "Alameda",
                    Address2 = "1º Esq.",
                    PostalCode = "1234-678",
                    Items = new List<Item>(new[] {items[3]})
                },
                new Order {
                    CustomerId = 7,
                    CompanyId = 1,
                    EstimatedDate = DateTime.UtcNow.Add(new TimeSpan(0, 0, 20, 0)),
                    ReadyToDistribute = false,
                    ItemsComplete = false,
                    OnGoing = false,
                    CreatedAt = DateTime.UtcNow,
                    Address = "Chelas",
                    Address2 = "1º Esq.",
                    PostalCode = "1234-678",
                    Items = new List<Item>(new[] {items[4]})
                },
                new Order {
                    CustomerId = 8,
                    CompanyId = 1,
                    EstimatedDate = DateTime.UtcNow.Add(new TimeSpan(0, 0, 5, 0)),
                    ReadyToDistribute = false,
                    ItemsComplete = true,
                    OnGoing = false,
                    CreatedAt = DateTime.UtcNow,
                    Address = "Santo António dos Cavaleiros",
                    Address2 = "1º Esq.",
                    PostalCode = "1234-678",
                    Items = new List<Item>(new[] {items[5]})
                },
                new Order {
                    CustomerId = 5,
                    CompanyId = 1,
                    EstimatedDate = DateTime.UtcNow,
                    ReadyToDistribute = true,
                    ItemsComplete = true,
                    OnGoing = false,
                    CreatedAt = DateTime.UtcNow.Add(new TimeSpan(0, 0, -20, 0)),
                    DeliveredAt = DateTime.UtcNow,
                    Address = "Rua tito morais",
                    Address2 = "1º Esq.",
                    PostalCode = "1234-678",
                    Items = new List<Item>(
                        new[] {items[6]})
                },
                new Order {
                    CustomerId = 2,
                    CompanyId = 1,
                    EstimatedDate = DateTime.UtcNow.Add(new TimeSpan(0, 0, -5, 0)),
                    ReadyToDistribute = true,
                    ItemsComplete = true,
                    OnGoing = false,
                    CreatedAt = DateTime.UtcNow.Add(new TimeSpan(0, 0, -20, 0)),
                    DeliveredAt = DateTime.UtcNow,
                    Address = "Rua tito morais 1",
                    Address2 = "1º Esq.",
                    PostalCode = "1234-678",
                    Items = new List<Item>(
                        new[] {items[7]})
                },
                new Order {
                    CustomerId = 2,
                    CompanyId = 1,
                    EstimatedDate = DateTime.UtcNow,
                    ReadyToDistribute = true,
                    ItemsComplete = true,
                    OnGoing = false,
                    CreatedAt = DateTime.UtcNow.Add(new TimeSpan(0, 0, -20, 0)),
                    DeliveredAt = DateTime.UtcNow,
                    Address = "Rua tito morais 2",
                    Address2 = "1º Esq.",
                    PostalCode = "1234-678",
                    Items = new List<Item>(
                        new[] {items[8]})
                }
            };

            context.Orders.AddRange(orders);

            context.SaveChanges();
        }

        private static void AddJourneys(DatabaseContext context) {
            var orders = context.Orders
                .Where(order => order.ReadyToDistribute && order.CompanyId == 1)
                .ToList();

            Journey[] journeys = new Journey[] {
                new Journey {
                    CompanyId = 1,
                    EmployeeId = 1,
                    Completed = false,
                    CreatedAt = DateTime.UtcNow.AddMinutes(-20),
                    Orders = orders.GetRange(0, 1)
                },
                new Journey {
                    CompanyId = 1,
                    EmployeeId = 1,
                    Completed = true,
                    CreatedAt = DateTime.UtcNow.AddMinutes(-20),
                    FinishedAt = DateTime.UtcNow,
                    Orders = orders.GetRange(1, 1)
                },
                new Journey {
                    CompanyId = 1,
                    EmployeeId = 2,
                    Completed = true,
                    CreatedAt = DateTime.UtcNow.AddMinutes(-20),
                    Orders = orders.GetRange(2, 2)
                }
            };

            context.Journeys.AddRange(journeys);

            context.SaveChanges();
        }

        private static void AddOrdersFeedback(DatabaseContext context) {
            var orders = context.Orders
                .Where(order => order.DeliveredAt != null)
                .ToList(); // 3

            Feedback[] feedbacks = new[] {
                new Feedback {
                    Note = "Very good food. Keep Up!",
                    Rating = Rating.Excellent,
                    CompanyId = 1,
                    CreatedAt = DateTime.UtcNow,
                    Order = orders[0]
                },
                new Feedback {
                    Note = "Improve a little or i won't come back",
                    Rating = Rating.NeedsImprovement,
                    CompanyId = 1,
                    CreatedAt = DateTime.UtcNow,
                    Order = orders[1]
                }
            };

            context.Feedbacks.AddRange(feedbacks);

            context.SaveChanges();
        }

        private static void AddCampaigns(DatabaseContext context) {
            List<Campaign> campaigns = new List<Campaign>(
                new[] {
                    new Campaign {
                        CompanyId = 1,
                        CreatedAt = DateTime.UtcNow.AddMinutes(-20),
                        FinishedAt = DateTime.UtcNow,
                        TotalCustomers = 2,
                        TotalSmsSent = 2,
                        Title = "Campaign",
                        Message = "Campaign Message"
                    },
                    new Campaign {
                        CompanyId = 1,
                        CreatedAt = DateTime.UtcNow.AddMinutes(-20),
                        FinishedAt = DateTime.UtcNow,
                        TotalCustomers = 5,
                        TotalSmsSent = 5,
                        Title = "Other Campaign",
                        Message = "Campaign Message x2"
                    }
                }
            );
            
            context.Set<Campaign>().AddRange(campaigns);

            context.SaveChanges();
        }
    }

}
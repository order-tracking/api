﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using MediatR;
using Microsoft.Extensions.DependencyInjection;

namespace API.Extensions
{
    public static class GetServices
    {
        public static IEnumerable<object> GetRequiredServices(
            this IServiceProvider provider,
            Type serviceType)
        {
            return (IEnumerable<object>) provider.GetRequiredService(
                typeof(IEnumerable<>).MakeGenericType(serviceType));
        }

        /// <summary>
        /// Load all Event handlers to the DI container with Transient lifetime
        /// </summary>
        /// <param name="services"></param>
        /// <param name="assembly"></param>
        /// <returns></returns>
        public static IServiceCollection AddEventHandlers(
            this IServiceCollection services,
            Assembly assembly)
        {
            var classTypes = assembly.ExportedTypes.Select(t => t.GetTypeInfo())
                .Where(t => t.IsClass && !t.IsAbstract);

            foreach (var type in classTypes)
            {
                var interfaces = type.ImplementedInterfaces.Select(i => i.GetTypeInfo());

                foreach (var handlerType in interfaces.Where(i =>
                    i.IsGenericType &&
                    i.GetGenericTypeDefinition() == typeof(IAsyncNotificationHandler<>)))
                {
                    services.AddTransient(handlerType.AsType(), type.AsType());
                }
            }

            return services;
        }
    }
}

﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace API.Migrations
{
    public partial class PipelineOptionalItemsCompletedByDefault : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Products_ProcessPipelines_PipelineId",
                table: "Products");

            migrationBuilder.AlterColumn<int>(
                name: "PipelineId",
                table: "Products",
                nullable: true,
                oldClrType: typeof(int));

            migrationBuilder.AddColumn<bool>(
                name: "ItemsComplete",
                table: "Orders",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddForeignKey(
                name: "FK_Products_ProcessPipelines_PipelineId",
                table: "Products",
                column: "PipelineId",
                principalTable: "ProcessPipelines",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Products_ProcessPipelines_PipelineId",
                table: "Products");

            migrationBuilder.DropColumn(
                name: "ItemsComplete",
                table: "Orders");

            migrationBuilder.AlterColumn<int>(
                name: "PipelineId",
                table: "Products",
                nullable: false,
                oldClrType: typeof(int),
                oldNullable: true);

            migrationBuilder.AddForeignKey(
                name: "FK_Products_ProcessPipelines_PipelineId",
                table: "Products",
                column: "PipelineId",
                principalTable: "ProcessPipelines",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }
    }
}

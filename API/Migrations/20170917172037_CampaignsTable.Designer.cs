﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;
using API.Data;
using API.Models;

namespace API.Migrations
{
    [DbContext(typeof(DatabaseContext))]
    [Migration("20170917172037_CampaignsTable")]
    partial class CampaignsTable
    {
        protected override void BuildTargetModel(ModelBuilder modelBuilder)
        {
            modelBuilder
                .HasAnnotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.SerialColumn)
                .HasAnnotation("ProductVersion", "1.1.1");

            modelBuilder.Entity("API.Models.Campaign", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<int>("CompanyId");

                    b.Property<DateTime>("CreatedAt");

                    b.Property<DateTime?>("FinishedAt");

                    b.Property<string>("Message");

                    b.Property<int>("TotalCustomers");

                    b.Property<int>("TotalSmsSent");

                    b.HasKey("Id");

                    b.HasIndex("CompanyId");

                    b.ToTable("Campaigns");
                });

            modelBuilder.Entity("API.Models.Company", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("Address")
                        .IsRequired();

                    b.Property<string>("CountryCode")
                        .IsRequired();

                    b.Property<int>("MaxAllowedDistance");

                    b.Property<string>("Name")
                        .IsRequired();

                    b.Property<string>("PostalCode")
                        .IsRequired();

                    b.HasKey("Id");

                    b.ToTable("Companies");
                });

            modelBuilder.Entity("API.Models.Customer", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<int>("CompanyId");

                    b.Property<string>("Name")
                        .IsRequired();

                    b.Property<string>("PhoneNumber")
                        .IsRequired();

                    b.HasKey("Id");

                    b.HasIndex("CompanyId");

                    b.ToTable("Customers");
                });

            modelBuilder.Entity("API.Models.Employee", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<int>("CompanyId");

                    b.Property<string>("HashedPassword")
                        .IsRequired();

                    b.Property<string>("Name")
                        .IsRequired();

                    b.Property<string>("PhoneNumber")
                        .IsRequired();

                    b.Property<int>("RoleId");

                    b.HasKey("Id");

                    b.HasIndex("CompanyId");

                    b.HasIndex("PhoneNumber")
                        .IsUnique();

                    b.HasIndex("RoleId");

                    b.ToTable("Employees");
                });

            modelBuilder.Entity("API.Models.EmployeeProcessPipeline", b =>
                {
                    b.Property<int>("PipelineId");

                    b.Property<int>("EmployeeId");

                    b.HasKey("PipelineId", "EmployeeId");

                    b.HasIndex("EmployeeId");

                    b.ToTable("EmployeeProcessPipeline");
                });

            modelBuilder.Entity("API.Models.Feedback", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("Note");

                    b.Property<int>("OrderId");

                    b.Property<int>("Rating");

                    b.HasKey("Id");

                    b.HasIndex("OrderId");

                    b.ToTable("Feedbacks");
                });

            modelBuilder.Entity("API.Models.Item", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<bool>("Completed");

                    b.Property<string>("Note");

                    b.Property<int>("OrderId");

                    b.Property<int?>("ProcessId");

                    b.Property<int?>("ProcessPipelineId");

                    b.Property<int>("ProductId");

                    b.Property<int?>("ProductTypeId");

                    b.Property<int>("Quantity");

                    b.HasKey("Id");

                    b.HasIndex("OrderId");

                    b.HasIndex("ProcessId");

                    b.HasIndex("ProcessPipelineId");

                    b.HasIndex("ProductId");

                    b.HasIndex("ProductTypeId");

                    b.ToTable("Items");
                });

            modelBuilder.Entity("API.Models.Journey", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<int>("CompanyId");

                    b.Property<bool>("Completed");

                    b.Property<DateTime>("CreatedAt");

                    b.Property<int>("EmployeeId");

                    b.Property<DateTime?>("FinishedAt");

                    b.HasKey("Id");

                    b.HasIndex("CompanyId");

                    b.HasIndex("EmployeeId");

                    b.ToTable("Journeys");
                });

            modelBuilder.Entity("API.Models.Order", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("Address")
                        .IsRequired();

                    b.Property<string>("Address2");

                    b.Property<int>("CompanyId");

                    b.Property<DateTime>("CreatedAt");

                    b.Property<int>("CustomerId");

                    b.Property<DateTime?>("DeliveredAt");

                    b.Property<DateTime>("EstimatedDate");

                    b.Property<int?>("FeedbackId");

                    b.Property<bool>("ItemsComplete");

                    b.Property<int?>("JourneyId");

                    b.Property<bool>("OnArrivingNotified");

                    b.Property<bool>("OnGoing");

                    b.Property<string>("PostalCode")
                        .IsRequired();

                    b.Property<bool>("ReadyToDistribute");

                    b.HasKey("Id");

                    b.HasIndex("CompanyId");

                    b.HasIndex("CustomerId");

                    b.HasIndex("FeedbackId");

                    b.HasIndex("JourneyId");

                    b.ToTable("Orders");
                });

            modelBuilder.Entity("API.Models.Process", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<int?>("EmployeeId");

                    b.Property<string>("Name")
                        .IsRequired();

                    b.Property<int>("OrderNumber");

                    b.Property<int>("PipelineId");

                    b.HasKey("Id");

                    b.HasIndex("EmployeeId");

                    b.HasIndex("PipelineId");

                    b.ToTable("Processes");
                });

            modelBuilder.Entity("API.Models.ProcessPipeline", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<int>("CompanyId");

                    b.Property<string>("Name")
                        .IsRequired();

                    b.HasKey("Id");

                    b.HasIndex("CompanyId");

                    b.ToTable("ProcessPipelines");
                });

            modelBuilder.Entity("API.Models.Product", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("Name")
                        .IsRequired();

                    b.Property<string>("Photo");

                    b.Property<int?>("PipelineId");

                    b.Property<int>("ProductGroupId");

                    b.HasKey("Id");

                    b.HasIndex("PipelineId");

                    b.HasIndex("ProductGroupId");

                    b.ToTable("Products");
                });

            modelBuilder.Entity("API.Models.ProductGroup", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<int>("CompanyId");

                    b.Property<string>("Name")
                        .IsRequired();

                    b.Property<int?>("PreviousProductGroupId");

                    b.HasKey("Id");

                    b.HasIndex("CompanyId");

                    b.HasIndex("PreviousProductGroupId");

                    b.ToTable("ProductGroups");
                });

            modelBuilder.Entity("API.Models.ProductType", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("Name")
                        .IsRequired();

                    b.Property<int>("ProductId");

                    b.HasKey("Id");

                    b.HasIndex("ProductId");

                    b.ToTable("ProductTypes");
                });

            modelBuilder.Entity("API.Models.Role", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("Name");

                    b.HasKey("Id");

                    b.ToTable("Roles");
                });

            modelBuilder.Entity("API.Models.Campaign", b =>
                {
                    b.HasOne("API.Models.Company", "Company")
                        .WithMany()
                        .HasForeignKey("CompanyId")
                        .OnDelete(DeleteBehavior.Cascade);
                });

            modelBuilder.Entity("API.Models.Customer", b =>
                {
                    b.HasOne("API.Models.Company", "Company")
                        .WithMany("Customers")
                        .HasForeignKey("CompanyId")
                        .OnDelete(DeleteBehavior.Cascade);
                });

            modelBuilder.Entity("API.Models.Employee", b =>
                {
                    b.HasOne("API.Models.Company", "Company")
                        .WithMany("Employees")
                        .HasForeignKey("CompanyId")
                        .OnDelete(DeleteBehavior.Cascade);

                    b.HasOne("API.Models.Role", "Role")
                        .WithMany("Employees")
                        .HasForeignKey("RoleId")
                        .OnDelete(DeleteBehavior.Cascade);
                });

            modelBuilder.Entity("API.Models.EmployeeProcessPipeline", b =>
                {
                    b.HasOne("API.Models.Employee", "Employee")
                        .WithMany("EmployeeProcessPipelines")
                        .HasForeignKey("EmployeeId")
                        .OnDelete(DeleteBehavior.Cascade);

                    b.HasOne("API.Models.ProcessPipeline", "Pipeline")
                        .WithMany("EmployeeProcessPipelines")
                        .HasForeignKey("PipelineId")
                        .OnDelete(DeleteBehavior.Cascade);
                });

            modelBuilder.Entity("API.Models.Feedback", b =>
                {
                    b.HasOne("API.Models.Order", "Order")
                        .WithMany()
                        .HasForeignKey("OrderId")
                        .OnDelete(DeleteBehavior.Cascade);
                });

            modelBuilder.Entity("API.Models.Item", b =>
                {
                    b.HasOne("API.Models.Order", "Order")
                        .WithMany("Items")
                        .HasForeignKey("OrderId")
                        .OnDelete(DeleteBehavior.Cascade);

                    b.HasOne("API.Models.Process", "Process")
                        .WithMany("Items")
                        .HasForeignKey("ProcessId");

                    b.HasOne("API.Models.ProcessPipeline")
                        .WithMany("Items")
                        .HasForeignKey("ProcessPipelineId");

                    b.HasOne("API.Models.Product", "Product")
                        .WithMany("Items")
                        .HasForeignKey("ProductId")
                        .OnDelete(DeleteBehavior.Cascade);

                    b.HasOne("API.Models.ProductType", "ProductType")
                        .WithMany()
                        .HasForeignKey("ProductTypeId");
                });

            modelBuilder.Entity("API.Models.Journey", b =>
                {
                    b.HasOne("API.Models.Company", "Company")
                        .WithMany()
                        .HasForeignKey("CompanyId")
                        .OnDelete(DeleteBehavior.Cascade);

                    b.HasOne("API.Models.Employee", "Employee")
                        .WithMany("Journeys")
                        .HasForeignKey("EmployeeId")
                        .OnDelete(DeleteBehavior.Cascade);
                });

            modelBuilder.Entity("API.Models.Order", b =>
                {
                    b.HasOne("API.Models.Company", "Company")
                        .WithMany("Orders")
                        .HasForeignKey("CompanyId")
                        .OnDelete(DeleteBehavior.Cascade);

                    b.HasOne("API.Models.Customer", "Customer")
                        .WithMany("Orders")
                        .HasForeignKey("CustomerId")
                        .OnDelete(DeleteBehavior.Cascade);

                    b.HasOne("API.Models.Feedback", "Feedback")
                        .WithMany()
                        .HasForeignKey("FeedbackId");

                    b.HasOne("API.Models.Journey", "Journey")
                        .WithMany("Orders")
                        .HasForeignKey("JourneyId")
                        .OnDelete(DeleteBehavior.SetNull);
                });

            modelBuilder.Entity("API.Models.Process", b =>
                {
                    b.HasOne("API.Models.Employee")
                        .WithMany("Processes")
                        .HasForeignKey("EmployeeId");

                    b.HasOne("API.Models.ProcessPipeline", "Pipeline")
                        .WithMany("Processes")
                        .HasForeignKey("PipelineId")
                        .OnDelete(DeleteBehavior.Cascade);
                });

            modelBuilder.Entity("API.Models.ProcessPipeline", b =>
                {
                    b.HasOne("API.Models.Company", "Company")
                        .WithMany()
                        .HasForeignKey("CompanyId")
                        .OnDelete(DeleteBehavior.Cascade);
                });

            modelBuilder.Entity("API.Models.Product", b =>
                {
                    b.HasOne("API.Models.ProcessPipeline", "Pipeline")
                        .WithMany()
                        .HasForeignKey("PipelineId");

                    b.HasOne("API.Models.ProductGroup", "ProductGroup")
                        .WithMany("Products")
                        .HasForeignKey("ProductGroupId")
                        .OnDelete(DeleteBehavior.Cascade);
                });

            modelBuilder.Entity("API.Models.ProductGroup", b =>
                {
                    b.HasOne("API.Models.Company", "Company")
                        .WithMany()
                        .HasForeignKey("CompanyId")
                        .OnDelete(DeleteBehavior.Cascade);

                    b.HasOne("API.Models.ProductGroup", "PreviousProductGroup")
                        .WithMany("NextProductGroups")
                        .HasForeignKey("PreviousProductGroupId")
                        .OnDelete(DeleteBehavior.Cascade);
                });

            modelBuilder.Entity("API.Models.ProductType", b =>
                {
                    b.HasOne("API.Models.Product", "Product")
                        .WithMany("ProductTypes")
                        .HasForeignKey("ProductId")
                        .OnDelete(DeleteBehavior.Cascade);
                });
        }
    }
}

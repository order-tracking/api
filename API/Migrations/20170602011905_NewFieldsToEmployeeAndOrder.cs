﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace API.Migrations {

    public partial class NewFieldsToEmployeeAndOrder : Migration {
        protected override void Up(MigrationBuilder migrationBuilder) {
            migrationBuilder.AddColumn<bool>(
                name: "IsLoggedIn",
                table: "Employees",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<DateTime>(
                name: "CreatedAt",
                table: "Orders",
                nullable: false,
                defaultValue: DateTime.MinValue);
        }

        protected override void Down(MigrationBuilder migrationBuilder) {
            migrationBuilder.DropColumn(
                name: "IsLoggedIn",
                table: "Employees");

            migrationBuilder.DropColumn(
                name: "CreatedAt",
                table: "Orders");
        }
    }

}
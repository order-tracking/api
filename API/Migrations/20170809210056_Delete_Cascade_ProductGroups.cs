﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace API.Migrations {

    public partial class Delete_Cascade_ProductGroups : Migration {
        protected override void Up(MigrationBuilder migrationBuilder) {
            migrationBuilder.DropForeignKey(
                name: "FK_ProductGroups_ProductGroups_PreviousProductGroupId",
                table: "ProductGroups");

            migrationBuilder.AddForeignKey(
                name: "FK_ProductGroups_ProductGroups_PreviousProductGroupId",
                table: "ProductGroups",
                column: "PreviousProductGroupId",
                principalTable: "ProductGroups",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder) {
            migrationBuilder.DropForeignKey(
                name: "FK_ProductGroups_ProductGroups_PreviousProductGroupId",
                table: "ProductGroups");

            migrationBuilder.AddForeignKey(
                name: "FK_ProductGroups_ProductGroups_PreviousProductGroupId",
                table: "ProductGroups",
                column: "PreviousProductGroupId",
                principalTable: "ProductGroups",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }
    }

}
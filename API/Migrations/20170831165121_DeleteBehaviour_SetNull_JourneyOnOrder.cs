﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace API.Migrations
{
    public partial class DeleteBehaviour_SetNull_JourneyOnOrder : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Orders_Journeys_JourneyId",
                table: "Orders");

            migrationBuilder.AddForeignKey(
                name: "FK_Orders_Journeys_JourneyId",
                table: "Orders",
                column: "JourneyId",
                principalTable: "Journeys",
                principalColumn: "Id",
                onDelete: ReferentialAction.SetNull);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Orders_Journeys_JourneyId",
                table: "Orders");

            migrationBuilder.AddForeignKey(
                name: "FK_Orders_Journeys_JourneyId",
                table: "Orders",
                column: "JourneyId",
                principalTable: "Journeys",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }
    }
}

﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace API.Migrations
{
    public partial class RenamedItemTypesTableToProductTypes : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Items_ItemTypes_ProductTypeId",
                table: "Items");

            migrationBuilder.DropForeignKey(
                name: "FK_ItemTypes_Products_ProductId",
                table: "ItemTypes");

            migrationBuilder.DropPrimaryKey(
                name: "PK_ItemTypes",
                table: "ItemTypes");

            migrationBuilder.RenameTable(
                name: "ItemTypes",
                newName: "ProductTypes");
            
            migrationBuilder.RenameIndex(
                name: "ItemTypes_Id_seq",
                table: "ProductTypes",
                newName: "ProductTypes_Id_seq");

            migrationBuilder.RenameIndex(
                name: "IX_ItemTypes_ProductId",
                table: "ProductTypes",
                newName: "IX_ProductTypes_ProductId");

            migrationBuilder.AddPrimaryKey(
                name: "PK_ProductTypes",
                table: "ProductTypes",
                column: "Id");

            migrationBuilder.AddForeignKey(
                name: "FK_Items_ProductTypes_ProductTypeId",
                table: "Items",
                column: "ProductTypeId",
                principalTable: "ProductTypes",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_ProductTypes_Products_ProductId",
                table: "ProductTypes",
                column: "ProductId",
                principalTable: "Products",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Items_ProductTypes_ProductTypeId",
                table: "Items");

            migrationBuilder.DropForeignKey(
                name: "FK_ProductTypes_Products_ProductId",
                table: "ProductTypes");

            migrationBuilder.DropPrimaryKey(
                name: "PK_ProductTypes",
                table: "ProductTypes");

            migrationBuilder.RenameTable(
                name: "ProductTypes",
                newName: "ItemTypes");
            
            migrationBuilder.RenameIndex(
                name: "ProductTypes_Id_seq",
                table: "ItemTypes",
                newName: "ItemTypes_Id_seq");

            migrationBuilder.RenameIndex(
                name: "IX_ProductTypes_ProductId",
                table: "ItemTypes",
                newName: "IX_ItemTypes_ProductId");

            migrationBuilder.AddPrimaryKey(
                name: "PK_ItemTypes",
                table: "ItemTypes",
                column: "Id");

            migrationBuilder.AddForeignKey(
                name: "FK_Items_ItemTypes_ProductTypeId",
                table: "Items",
                column: "ProductTypeId",
                principalTable: "ItemTypes",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_ItemTypes_Products_ProductId",
                table: "ItemTypes",
                column: "ProductId",
                principalTable: "Products",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }
    }
}

﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace API.Migrations
{
    public partial class ItemTypeIsOptional : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Items_ItemTypes_ProductTypeId",
                table: "Items");

            migrationBuilder.AlterColumn<int>(
                name: "ProductTypeId",
                table: "Items",
                nullable: true,
                defaultValueSql: "NULL",
                oldClrType: typeof(int));

            migrationBuilder.AddForeignKey(
                name: "FK_Items_ItemTypes_ProductTypeId",
                table: "Items",
                column: "ProductTypeId",
                principalTable: "ItemTypes",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Items_ItemTypes_ProductTypeId",
                table: "Items");

            migrationBuilder.AlterColumn<int>(
                name: "ProductTypeId",
                table: "Items",
                nullable: false,
                oldClrType: typeof(int),
                oldNullable: true);

            migrationBuilder.AddForeignKey(
                name: "FK_Items_ItemTypes_ProductTypeId",
                table: "Items",
                column: "ProductTypeId",
                principalTable: "ItemTypes",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }
    }
}

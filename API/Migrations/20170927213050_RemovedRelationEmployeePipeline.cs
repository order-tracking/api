﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace API.Migrations
{
    public partial class RemovedRelationEmployeePipeline : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "EmployeeProcessPipeline");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "EmployeeProcessPipeline",
                columns: table => new
                {
                    PipelineId = table.Column<int>(nullable: false),
                    EmployeeId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_EmployeeProcessPipeline", x => new { x.PipelineId, x.EmployeeId });
                    table.ForeignKey(
                        name: "FK_EmployeeProcessPipeline_Employees_EmployeeId",
                        column: x => x.EmployeeId,
                        principalTable: "Employees",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_EmployeeProcessPipeline_ProcessPipelines_PipelineId",
                        column: x => x.PipelineId,
                        principalTable: "ProcessPipelines",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_EmployeeProcessPipeline_EmployeeId",
                table: "EmployeeProcessPipeline",
                column: "EmployeeId");
        }
    }
}

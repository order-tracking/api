﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace API.Migrations {

    public partial class Rename_ItemType_To_ProductType : Migration {
        protected override void Up(MigrationBuilder migrationBuilder) {
            migrationBuilder.DropForeignKey(
                name: "FK_Items_ItemTypes_ItemTypeId",
                table: "Items");

            migrationBuilder.RenameColumn(
                name: "Name",
                table: "Items",
                newName: "Note");

            migrationBuilder.RenameColumn(
                name: "ItemTypeId",
                table: "Items",
                newName: "ProductTypeId");

            migrationBuilder.RenameIndex(
                name: "IX_Items_ItemTypeId",
                table: "Items",
                newName: "IX_Items_ProductTypeId");

            migrationBuilder.AddForeignKey(
                name: "FK_Items_ItemTypes_ProductTypeId",
                table: "Items",
                column: "ProductTypeId",
                principalTable: "ItemTypes",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder) {
            migrationBuilder.DropForeignKey(
                name: "FK_Items_ItemTypes_ProductTypeId",
                table: "Items");

            migrationBuilder.RenameColumn(
                name: "ProductTypeId",
                table: "Items",
                newName: "ItemTypeId");

            migrationBuilder.RenameColumn(
                name: "Note",
                table: "Items",
                newName: "Name");

            migrationBuilder.RenameIndex(
                name: "IX_Items_ProductTypeId",
                table: "Items",
                newName: "IX_Items_ItemTypeId");

            migrationBuilder.AddForeignKey(
                name: "FK_Items_ItemTypes_ItemTypeId",
                table: "Items",
                column: "ItemTypeId",
                principalTable: "ItemTypes",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }
    }

}
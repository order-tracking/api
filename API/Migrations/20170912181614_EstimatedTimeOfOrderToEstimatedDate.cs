﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace API.Migrations
{
    public partial class EstimatedTimeOfOrderToEstimatedDate : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "EstimatedTime",
                table: "Orders");

            migrationBuilder.AddColumn<DateTime>(
                name: "EstimatedDate",
                table: "Orders",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "EstimatedDate",
                table: "Orders");

            migrationBuilder.AddColumn<int>(
                name: "EstimatedTime",
                table: "Orders",
                nullable: false,
                defaultValue: 0);
        }
    }
}

﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace API.Migrations {

    public partial class AddedFinishedDateToJourney : Migration {
        protected override void Up(MigrationBuilder migrationBuilder) {
            migrationBuilder.AddColumn<DateTime>(
                name: "FinishedAt",
                table: "Journeys",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder) {
            migrationBuilder.DropColumn(
                name: "FinishedAt",
                table: "Journeys");
        }
    }

}

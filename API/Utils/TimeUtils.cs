﻿using System;

namespace API.Utils
{
    public class TimeUtils
    {
        /// <summary>
        /// Get the hours from a date
        /// Return: 19:20
        /// </summary>
        /// <param name="time"></param>
        /// <returns></returns>
        public static string GetHours(DateTime time)
        {
            return time.ToString("HH:mm");
        }

        /// <summary>
        /// Get minutes remaining to a utc date
        /// 
        /// if the date is in past, the result is negative
        /// </summary>
        /// <param name="utcDate"></param>
        /// <returns></returns>
        public static int GetMinutesToDate(string utcDate)
        {
            var eta = DateTime.Parse(utcDate).ToUniversalTime();
            var now = DateTime.UtcNow;
            var span = eta.Subtract(now);

            return (int) span.TotalMinutes;
        }
    }
}

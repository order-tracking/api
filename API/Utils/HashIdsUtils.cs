﻿using HashidsNet;

namespace API.Utils
{
    /// <summary>
    /// Hash ids to prevent guessing
    /// </summary>
    public class HashIdsUtils
    {
        private const string  Secret = "myhashidsecretvarforotrack";
        
        public static string Encode(int id)
        {
            var hashids = new Hashids(Secret);
            
            return hashids.Encode(id);
        }
        
        public static int Decode(string hash)
        {
            var hashids = new Hashids(Secret);
            
            return hashids.Decode(hash)[0];
        }
    }
}

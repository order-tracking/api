﻿namespace API.Utils
{
    public class AddressUtils
    {
        /// <summary>
        /// Join two addresses
        /// </summary>
        /// <param name="address"></param>
        /// <param name="address2"></param>
        /// <returns></returns>
        public static string BuildAddress(string address, string address2)
        {
            if (string.IsNullOrEmpty(address2))
            {
                return address;
            }

            return address + ", " + address2;
        }
        
        /// <summary>
        /// Remove unwanted chars from an address
        /// </summary>
        /// <param name="address"></param>
        /// <returns></returns>
        public static string Parse(string address)
        {
            return address.Trim();
        }
        
        /// <summary>
        /// Remove unwanted chars from a postal code
        /// </summary>
        /// <param name="postal"></param>
        /// <returns></returns>
        public static string ParsePostalCode(string postal)
        {
            return postal.Trim();
        }
    }
}

﻿using System.Text.RegularExpressions;
using API.Models;

namespace API.Utils
{
    public class PhoneUtils
    {
        /// <summary>
        /// Clean spaces and prepend the country prefix code
        /// </summary>
        /// <param name="input"></param>
        /// <param name="countryCode"></param>
        /// <returns></returns>
        public static string Parse(string input, string countryCode)
        {
            var trimmed = Regex.Replace(input, @"\s+", "");

            return CountryList.Get(countryCode).PhonePrefix + input;
        }
    }
}

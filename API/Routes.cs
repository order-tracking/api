namespace API {

    public class Routes {
        // HomeController
        public const string Index = nameof(Index);

        // SeedController
        public const string Seed = nameof(Seed);
        public const string DeleteSeed = nameof(DeleteSeed);
        public const string HypertrackClear = nameof(HypertrackClear);

        // CustomersController
        public const string GetAllCustomers = nameof(GetAllCustomers);
        public const string GetCustomer = nameof(GetCustomer);
        public const string GetCustomerByPhoneNumber = nameof(GetCustomerByPhoneNumber);
        public const string CreateCustomer = nameof(CreateCustomer);
        public const string UpdateCustomer = nameof(UpdateCustomer);
        public const string DeleteCustomer = nameof(DeleteCustomer);

        // CompaniesController
        public const string GetCompany = nameof(GetCompany);
        public const string CreateCompany = nameof(CreateCompany);
        public const string EditCompany = nameof(EditCompany);

        // EmployeesController
        public const string GetEmployees = nameof(GetEmployees);

        public const string GetEmployee = nameof(GetEmployee);
        public const string CreateEmployee = nameof(CreateEmployee);
        public const string UpdateEmployee = nameof(UpdateEmployee);
        public const string DeleteEmployee = nameof(DeleteEmployee);

        // OrdersController
        public const string GetOrders = nameof(GetOrders);
        public const string CalculateDistance = nameof(CalculateDistance);
        public const string GetIncompleteOrders = nameof(GetIncompleteOrders);
        public const string GetReadyOrders = nameof(GetReadyOrders);
        public const string GetOrder = nameof(GetOrder);
        public const string GetOrderHashed = nameof(GetOrderHashed);
        public const string GetOrderFeedback = nameof(GetOrderFeedback);
        public const string GetHashedOrderFeedback = nameof(GetHashedOrderFeedback);
        public const string GetOrderItems = nameof(GetOrderItems);
        public const string CreateOrder = nameof(CreateOrder);
        public const string EditOrder = nameof(EditOrder);
        public const string EditOrderItem = nameof(EditOrderItem);
        public const string DeleteOrder = nameof(DeleteOrder);

        // ProcessPipelinesController
        public const string GetProcessPipelines = nameof(GetProcessPipelines);

        public const string GetProcessPipeline = nameof(GetProcessPipeline);
        public const string GetProcessPipelineItems = nameof(GetProcessPipelineItems);
        public const string CreateProcessPipeline = nameof(CreateProcessPipeline);
        public const string EditProcessPipeline = nameof(EditProcessPipeline);
        public const string DeleteProcessPipeline = nameof(DeleteProcessPipeline);

        // ProductGroupsController
        public const string GetProductGroup = nameof(GetProductGroup);

        public const string GetFirstProductGroups = nameof(GetFirstProductGroups);
        public const string GetProductGroupProducts = nameof(GetProductGroupProducts);
        public const string GetNextProductGroups = nameof(GetNextProductGroups);
        public const string CreateProductGroup = nameof(CreateProductGroup);
        public const string EditProductGroup = nameof(EditProductGroup);
        public const string DeleteProductGroup = nameof(DeleteProductGroup);

        // ProductsController
        public const string GetProduct = nameof(GetProduct);

        public const string CreateProduct = nameof(CreateProduct);
        public const string DeleteProduct = nameof(DeleteProduct);
        
        // JourneysController
        public const string GetJourneys = nameof(GetJourneys);
        public const string GetJourney = nameof(GetJourney);
        public const string CreateJourney = nameof(CreateJourney);
        public const string EditJourney = nameof(EditJourney);
        public const string DeleteJourney = nameof(DeleteJourney);

        // RolesController
        public const string GetAllRoles = nameof(GetAllRoles);

        // DashboardController
        public const string GetHomeStats = nameof(GetHomeStats);

        // AuthController
        public const string AuthenticateEmployee = nameof(AuthenticateEmployee);
        
        // FeedbackController
        public const string GetFeedback = nameof(GetFeedback);
        public const string UpdateFeedback = nameof(UpdateFeedback);
        public const string CreateFeedback = nameof(CreateFeedback);
        
        // CampaignsController
        public const string GetCampaigns = nameof(GetCampaigns);
        public const string CreateCampaign = nameof(CreateCampaign);
        public const string GetCampaign = nameof(GetCampaign);
        public const string DeleteCampaign = nameof(DeleteCampaign);
        
        // Hypertrack Webhook 
        public const string HypertrackWebhook = nameof(HypertrackWebhook);
        public const string HypertrackSK = nameof(HypertrackSK);
        
        // ReportsController 
        public const string GetReports = nameof(GetReports);
    }

}

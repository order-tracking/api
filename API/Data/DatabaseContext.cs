using API.Models;
using EntityFrameworkCore.Triggers;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;

namespace API.Data {

    public class DatabaseContext : DbContextWithTriggers {
        public DatabaseContext(DbContextOptions<DatabaseContext> options)
            : base(options) { }

        protected override void OnModelCreating(ModelBuilder modelBuilder) {
            //-------------------------------------------------------------------------
            // Delete Cascade Product Group
            //-------------------------------------------------------------------------
            modelBuilder.Entity<ProductGroup>()
                .HasOne(pg => pg.PreviousProductGroup)
                .WithMany(m => m.NextProductGroups)
                .HasForeignKey(k => k.PreviousProductGroupId)
                .OnDelete(DeleteBehavior.Cascade);
            
            //-------------------------------------------------------------------------
            // Delete Cascade Product Types
            //-------------------------------------------------------------------------
            modelBuilder.Entity<Product>()
                .HasMany(pg => pg.ProductTypes)
                .WithOne(m => m.Product)
                .HasForeignKey(k => k.ProductId)
                .OnDelete(DeleteBehavior.Cascade);

            //-------------------------------------------------------------------------
            // Delete SetNull Journey on Order
            //-------------------------------------------------------------------------
            modelBuilder.Entity<Journey>()
                .HasMany(journey => journey.Orders)
                .WithOne(order => order.Journey)
                .HasForeignKey(order => order.JourneyId)
                .OnDelete(DeleteBehavior.SetNull);

            //-------------------------------------------------------------------------
            // Unique Constraints
            //-------------------------------------------------------------------------
            modelBuilder.Entity<Employee>()
                .HasIndex(c => c.PhoneNumber)
                .IsUnique();
//
//            modelBuilder.Entity<Customer>()
//                .HasIndex(c => c.PhoneNumber)
//                .IsUnique();

            //-------------------------------------------------------------------------
            // M2M Relation With Worker and ProcessPipeline
            //-------------------------------------------------------------------------
//            modelBuilder.Entity<EmployeeProcessPipeline>()
//                .HasKey(t => new {t.PipelineId, t.EmployeeId});
//
//            modelBuilder.Entity<EmployeeProcessPipeline>()
//                .HasOne(pt => pt.Pipeline)
//                .WithMany(p => p.EmployeeProcessPipelines)
//                .HasForeignKey(pt => pt.PipelineId);
//
//            modelBuilder.Entity<EmployeeProcessPipeline>()
//                .HasOne(pt => pt.Employee)
//                .WithMany(t => t.EmployeeProcessPipelines)
//                .HasForeignKey(pt => pt.EmployeeId);
        }

        public DbSet<Customer> Customers { get; set; }

        public DbSet<Company> Companies { get; set; }

        public DbSet<Employee> Employees { get; set; }

        public DbSet<Journey> Journeys { get; set; }

        public DbSet<Order> Orders { get; set; }

        public DbSet<Process> Processes { get; set; }

        public DbSet<ProcessPipeline> ProcessPipelines { get; set; }

        public DbSet<Item> Items { get; set; }

        public DbSet<Product> Products { get; set; }

        public DbSet<ProductGroup> ProductGroups { get; set; }

        public DbSet<ProductType> ProductTypes { get; set; }
        
        public DbSet<Feedback> Feedbacks { get; set; }
        
        public DbSet<Campaign> Campaigns { get; set; }
    }

}

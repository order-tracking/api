﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using API.Data.Repository.Generic;
using API.Models;
using Microsoft.EntityFrameworkCore;

namespace API.Data.Repository
{
    public class ReportsRepository : IReportsRepository
    {
        private readonly DbContext _context;

        public ReportsRepository(DatabaseContext context)
        {
            _context = context;
        }

        /// <summary>
        /// Get all information at the same time and wait for all
        /// </summary>
        /// <param name="companyId"></param>
        /// <param name="from"></param>
        /// <param name="to"></param>
        /// <returns></returns>
        public async Task<Report> GetReportBetween(int companyId, DateTime from, DateTime to)
        {
            List<int> res = new List<int>();

            res.Add(await _context.Set<Order>()
                .Where(o => o.CreatedAt >= from && o.CreatedAt <= to && o.CompanyId == companyId)
                .CountAsync());

            res.Add(await _context.Set<Journey>()
                .Where(o => o.CreatedAt >= from && o.CreatedAt <= to && o.CompanyId == companyId)
                .CountAsync());

            res.Add(await _context.Set<Employee>()
                .Where(o => o.CompanyId == companyId)
                .CountAsync());

            res.Add(await _context.Set<Customer>()
                .Where(o => o.CompanyId == companyId)
                .CountAsync());

            res.Add(await _context.Set<Product>()
                .Where(o => o.CompanyId == companyId)
                .CountAsync());

            res.Add(await _context.Set<Feedback>()
                .Where(o => o.CreatedAt >= from && o.CreatedAt <= to && o.CompanyId == companyId)
                .CountAsync());

            res.Add(await _context.Set<Campaign>()
                .Where(o => o.CreatedAt >= from && o.CreatedAt <= to && o.CompanyId == companyId)
                .CountAsync());

            res.Add(await _context.Set<ProcessPipeline>()
                .Where(o => o.CompanyId == companyId)
                .CountAsync());

            res.Add(await _context.Set<Process>()
                .Where(o => o.CompanyId == companyId)
                .CountAsync());

            //var res = await Task.WhenAll(tasks); // wait for all

            return new Report() {
                From = from,
                To = to,
                TotalOrders = res[0],
                TotalJourneys = res[1],
                TotalEmployees = res[2],
                TotalCustomers = res[3],
                TotalProducts = res[4],
                TotalFeedbacks = res[5],
                TotalCampaigns = res[6],
                TotalPipelines = res[7],
                TotalProcesses = res[8]
            };
        }
    }
}

﻿using System.Linq;
using System.Threading.Tasks;
using API.Data.Repository.Generic;
using API.Models;
using Microsoft.EntityFrameworkCore;
using X.PagedList;

namespace API.Data.Repository
{
    public class CampaignRepository : GenericRepository<Campaign>, ICampaignRepository
    {
        public CampaignRepository(DatabaseContext context) : base(context) { }
        
        public Task<IPagedList<Campaign>> GetPaginated(int companyId, int pageNumber, int limit) {
            return _context.Set<Campaign>()
                .Where(cl => cl.CompanyId == companyId)
                .ToPagedListAsync(pageNumber, limit);
        }
    }
}

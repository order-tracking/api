using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using API.Data.Repository.Generic;
using API.Models;
using Microsoft.EntityFrameworkCore;
using X.PagedList;

namespace API.Data.Repository {

    public class CustomersRepository : GenericRepository<Customer>, ICustomerRepository {
        public CustomersRepository(DatabaseContext context) : base(context) { }

        public Task<List<Customer>> GetAllCustomersOfCompany(int companyId) {
            return _context.Set<Customer>()
                .Where(cl => cl.CompanyId == companyId)
                .ToListAsync();
        }

        public Task<Customer> GetCustomerByPhoneNumber(int companyId, string phoneNumber) {
            return _context.Set<Customer>()
                .Where(cl => cl.PhoneNumber.Equals(phoneNumber) && cl.CompanyId == companyId)
                .FirstOrDefaultAsync();
        }

        public Task<IPagedList<Customer>> GetPaginatedCustomersOfCompany(int companyId, int pageNumber, int limit) {
            return _context.Set<Customer>()
                .Where(cl => cl.CompanyId == companyId)
                .ToPagedListAsync(pageNumber, limit);
        }
    }

}
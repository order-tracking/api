using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using API.Data.Repository.Generic;
using API.Models;
using Microsoft.EntityFrameworkCore;
using X.PagedList;

namespace API.Data.Repository {

    public class OrdersRepository : GenericRepository<Order>, IOrdersRepository {
        public OrdersRepository(DatabaseContext context) : base(context) { }

        public new Task<Order> Get(int id) {
            return _context.Set<Order>()
                .Where(order => order.Id == id)
                .OrderByDescending(order => order.CreatedAt)
                .Include(order => order.Customer)
                .Include(order => order.Journey)
                .ThenInclude(journey => journey.Employee)
                .Include(order => order.Items)
                .ThenInclude(item => item.Product)
                .Include(order => order.Items)
                .ThenInclude(item => item.ProductType)
                .Include(order => order.Items)
                .ThenInclude(item => item.Process)
                .FirstOrDefaultAsync();
        }

        public Task<List<Order>> GetAllOrdersOfCustomer(Customer customer) {
            return _context.Set<Order>()
                .Where(order => order.CompanyId == customer.CompanyId && order.CustomerId == customer.Id)
                .OrderByDescending(order => order.CreatedAt)
                .Include(order => order.Customer)
                .Include(order => order.Items)
                .ThenInclude(item => item.ProductType)
                .Include(order => order.Items)
                .ThenInclude(item => item.Product)
                .ToListAsync();
        }

        public Task<List<Order>> GetAllOrdersOfCompany(int companyId) {
            return _context.Set<Order>()
                .Where(order => order.CompanyId == companyId)
                .OrderByDescending(order => order.CreatedAt)
                .Include(order => order.Customer)
                .Include(order => order.Items)
                .ThenInclude(item => item.ProductType)
                .Include(order => order.Items)
                .ThenInclude(item => item.Product)
                .ToListAsync();
        }

        public Task<IPagedList<Order>> GetPaginatedOrdersOfCompany(int companyId, int page, int limit) {
            return _context.Set<Order>()
                .Where(order => order.CompanyId == companyId)
                .OrderByDescending(order => order.CreatedAt)
                .Include(order => order.Customer)
                .Include(order => order.Items)
                .ThenInclude(item => item.ProductType)
                .Include(order => order.Items)
                .ThenInclude(item => item.Product)
                .ToPagedListAsync(page, limit);
        }
        
        public Task<IPagedList<Order>> GetPaginatedReadyOrders(int companyId, int page, int limit) {
            return _context.Set<Order>()
                .Where(order => order.CompanyId == companyId && 
                                order.ItemsComplete && 
                                order.JourneyId == null && 
                                order.DeliveredAt == null)
                .OrderByDescending(order => order.CreatedAt)
                .Include(order => order.Customer)
                .Include(order => order.Items)
                .ThenInclude(item => item.ProductType)
                .Include(order => order.Items)
                .ThenInclude(item => item.Product)
                .ToPagedListAsync(page, limit);
        }

        public Task<IPagedList<Order>> GetPaginatedIncompleteOrders(int companyId, int page, int limit) {
            return _context.Set<Order>()
                .Where(order => order.CompanyId == companyId && !order.ReadyToDistribute)
                .OrderByDescending(order => order.CreatedAt)
                .Include(order => order.Customer)
                .Include(order => order.Items)
                .ThenInclude(item => item.ProductType)
                .Include(order => order.Items)
                .ThenInclude(item => item.Product)
                .ToPagedListAsync(page, limit);
        }

        public Task<List<Order>> GetLastOrdersOfCustomer(Customer customer, int number) {
            return _context.Set<Order>()
                .Where(order => order.CompanyId == customer.CompanyId && order.CustomerId == customer.Id)
                .OrderByDescending(order => order.CreatedAt)
                .Include(order => order.Items)
                    .ThenInclude(item => item.ProductType)
                .Include(order => order.Items)
                    .ThenInclude(item => item.Product)
                .Take(number)
                .ToListAsync();
        }

        public new Task<Order> Create(Order model) {
            bool itemsComplete = true;
            
            // Add First Process of Pipeline to Items
            var enumerable = model.Items.Select(async item => {
                var itemProduct = await _context.Set<Product>()
                    .Where(p => p.Id == item.ProductId)
                    .FirstOrDefaultAsync();
                
                item.Process = await _context.Set<Process>()
                    .Where(process => process.PipelineId == itemProduct.PipelineId && process.OrderNumber == 1)
                    .FirstOrDefaultAsync();

                // No Process, Item is completed by default
                item.Completed = item.Process == null;
                
                // Helper to define if order is ready to be assembled on creation
                // Example: A Coce and an already made Desert
                itemsComplete = itemsComplete && item.Completed;
            });
            
            return Task.WhenAll(enumerable).ContinueWith(res => {
                model.ItemsComplete = itemsComplete;
                _context.Set<Order>().Add(model);
                
                return SaveAsync(model);
            }).Unwrap();
        }
        
        public Task<Item> GetOrderItem(int itemId) {
            return _context.Set<Item>()
                .Where(item => item.Id == itemId)
                .Include(item => item.Product)
                .Include(item => item.ProductType)
                .FirstOrDefaultAsync();
        }

        public Task<bool> UpdateOrderItem(Item item) {
            _context.Entry(item).State = EntityState.Modified;
            return SaveAsync();
        }

        public Task<Feedback> GetOrderFeedback(int orderId) {
            return _context.Set<Feedback>()
                .Where(feedback => feedback.OrderId == orderId)
                .FirstOrDefaultAsync();
        }
    }

}

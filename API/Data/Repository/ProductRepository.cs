using System.Linq;
using System.Threading.Tasks;
using API.Data.Repository.Generic;
using API.Models;
using Microsoft.EntityFrameworkCore;

namespace API.Data.Repository {

    public class ProductRepository : GenericRepository<Product>, IProductRepository {
        public ProductRepository(DatabaseContext context) : base(context) { }
        
        public new Task<Product> Get(int id) {
            return _context.Set<Product>()
                .Where(p => p.Id == id)
                .Include(product => product.Pipeline)
                .Include(product => product.ProductTypes)
                .FirstOrDefaultAsync();
        }
    }

}
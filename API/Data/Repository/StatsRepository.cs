using System;
using System.Linq;
using System.Threading.Tasks;
using API.Data.Repository.Generic;
using API.Models;
using Microsoft.EntityFrameworkCore;

namespace API.Data.Repository {

    public class StatsRepository : IStatsRepository {
        private readonly DatabaseContext _context;

        public StatsRepository(DatabaseContext context) {
            _context = context;
        }

        public Task<Stats> GetCompanyStats(int companyId) {
            return _context.Set<Order>()
                .Where(o => o.CompanyId == companyId && o.CreatedAt > DateTime.Today)
                .ToArrayAsync()
                .ContinueWith(orderTask => {
                    Order[] orders = orderTask.Result;
                    return new Stats {
                        PendingOrders = orders
                            .Count(o => !o.DeliveredAt.HasValue),
                        OrdersCompletedToday = orders
                            .Count(o => o.DeliveredAt.HasValue)
                    };
                });
        }
    }

}

using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using API.Data.Repository.Generic;
using API.Models;
using Microsoft.EntityFrameworkCore;

namespace API.Data.Repository {

    public class ProductGroupsRepository : GenericRepository<ProductGroup>, IProductGroupsRepository {
        public ProductGroupsRepository(DatabaseContext context) : base(context) { }

        public new Task<ProductGroup> Get(int id) {
            return _context.Set<ProductGroup>()
                .Where(pg => pg.Id == id)
                .Include(pg => pg.Products)
                .ThenInclude(products => products.ProductTypes)
                .Include(pg => pg.NextProductGroups)
                .FirstOrDefaultAsync();
        }

        public Task<List<ProductGroup>> GetFirstProductGroups(int companyId) {
            return _context.Set<ProductGroup>()
                .Where(pg => pg.CompanyId == companyId && pg.PreviousProductGroupId == null)
                .Include(pg => pg.Products)
                .ToListAsync();
        }

        public Task<List<ProductGroup>> GetNextProductGroups(int id) {
            return _context.Set<ProductGroup>()
                .Where(pg => pg.PreviousProductGroupId == id)
                .Include(pg => pg.Products)
                .ToListAsync();
        }

        public Task<List<Product>> GetProducts(int id) {
            return _context.Set<Product>()
                .Where(product => product.ProductGroupId == id)
                .Include(product => product.Pipeline)
                .Include(product => product.ProductTypes)
                .ToListAsync();
        }
    }

}
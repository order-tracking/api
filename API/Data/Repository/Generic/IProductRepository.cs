using API.Models;

namespace API.Data.Repository.Generic {

    public interface IProductRepository : IRepository<Product> { }

}
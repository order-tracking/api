﻿using System.Threading.Tasks;
using API.Models;
using X.PagedList;

namespace API.Data.Repository.Generic
{
    public interface ICampaignRepository : IRepository<Campaign>
    {
        Task<IPagedList<Campaign>> GetPaginated(int companyId, int pageNumber, int limit);
    }
}

using System.Collections.Generic;
using System.Threading.Tasks;
using API.Models;
using X.PagedList;

namespace API.Data.Repository.Generic {

    public interface ICustomerRepository : IRepository<Customer> {
        Task<List<Customer>> GetAllCustomersOfCompany(int companyId);

        Task<IPagedList<Customer>> GetPaginatedCustomersOfCompany(int companyId, int pageNumber, int limit);

        Task<Customer> GetCustomerByPhoneNumber(int companyId, string phoneNumber);
    }

}
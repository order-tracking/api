using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace API.Data.Repository.Generic {

    public interface IRepository<T> {
        Task<List<T>> GetAll(Expression<Func<T, bool>> pred = default(Expression<Func<T, bool>>));

        Task<T> Get(int Id);

        Task<T> Create(T model);

        Task<bool> Update(T model);

        Task<bool> Delete(T model);
    }

}
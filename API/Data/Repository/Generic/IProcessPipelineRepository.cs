using System.Collections.Generic;
using System.Threading.Tasks;
using API.Models;
using X.PagedList;

namespace API.Data.Repository.Generic {

    public interface IProcessPipelineRepository : IRepository<ProcessPipeline> {

        Task<IPagedList<ProcessPipeline>> GetPaginatedPipelinesOfCompany(int companyId, int page, int limit);

        Task<List<Item>> GetProcessItems(int processId);
    }

}
using System.Collections.Generic;
using System.Threading.Tasks;
using API.Models;

namespace API.Data.Repository.Generic {

    public interface IProductGroupsRepository : IRepository<ProductGroup> {
        Task<List<ProductGroup>> GetFirstProductGroups(int companyId);

        Task<List<ProductGroup>> GetNextProductGroups(int Id);

        Task<List<Product>> GetProducts(int id);
    }

}
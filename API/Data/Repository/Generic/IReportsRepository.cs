﻿using System;
using System.Threading.Tasks;
using API.Models;

namespace API.Data.Repository.Generic
{
    public interface IReportsRepository
    {
        Task<Report> GetReportBetween(int companyId, DateTime from, DateTime to);
    }
}

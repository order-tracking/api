using System.Collections.Generic;
using System.Threading.Tasks;
using API.Models;
using X.PagedList;

namespace API.Data.Repository.Generic
{

    public interface IEmployeeRepository : IRepository<Employee>
    {
        Task<List<Employee>> GetAllEmployeesOfCompany(int companyId);

        Task<IPagedList<Employee>> GetPaginatedEmployeesOfCompany(
            int companyId,
            int pageNumber,
            int limit);

        Task<Employee> GetEmployee(int id);
        Task<Employee> GetEmployeeByPhone(string phoneNumber);

        Task<List<Employee>> GetEmployeesDistributing();

        Task<List<Employee>> GetEmployeesWorking();
    }

}

using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;

namespace API.Data.Repository.Generic {

    public abstract class GenericRepository<T> : IRepository<T> where T : class {
        protected DbContext _context;

        protected GenericRepository(DbContext context) {
            _context = context;
        }

        public Task<List<T>> GetAll(Expression<Func<T, bool>> pred) {
            return _context.Set<T>().Where(pred).ToListAsync();
        }

        public Task<T> Get(int Id) {
            return _context.Set<T>().FindAsync(Id);
        }

        public Task<T> Create(T model) {
            _context.Set<T>().Add(model);

            return SaveAsync(model);
        }

        public Task<bool> Delete(T model) {
            _context.Set<T>().Remove(model);
            return SaveAsync();
        }

        public Task<bool> Update(T model) {
            _context.Entry(model).State = EntityState.Modified;
            return SaveAsync();
        }

        protected Task<bool> SaveAsync() {
            return _context.SaveChangesAsync()
                .ContinueWith(antecedent => antecedent.Result > 0);
        }
        
        protected Task<T> SaveAsync(T model) {
            return _context.SaveChangesAsync()
                .ContinueWith(antecedent => antecedent.Result > 0 ? model : null);
        }
    }

}
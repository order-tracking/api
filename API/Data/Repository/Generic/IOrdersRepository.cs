using System.Collections.Generic;
using System.Threading.Tasks;
using API.Models;
using X.PagedList;

namespace API.Data.Repository.Generic {

    public interface IOrdersRepository : IRepository<Order> {
        Task<List<Order>> GetAllOrdersOfCompany(int companyId);
        Task<List<Order>> GetAllOrdersOfCustomer(Customer customer);
        Task<IPagedList<Order>> GetPaginatedOrdersOfCompany(int companyId, int page, int limit);
        Task<IPagedList<Order>> GetPaginatedReadyOrders(int companyId, int page, int limit);
        Task<IPagedList<Order>> GetPaginatedIncompleteOrders(int companyId, int page, int limit);
        Task<List<Order>> GetLastOrdersOfCustomer(Customer customer, int number);

        //Task<List<Order>> GetAllReadyOrdersOfCompany(int companyId);
        Task<Item> GetOrderItem(int itemId);
        Task<bool> UpdateOrderItem(Item item);
        Task<Feedback> GetOrderFeedback(int id);
    }

}
﻿using API.Models;

namespace API.Data.Repository.Generic {

    public interface IFeedbackRepository : IRepository<Feedback> {
        
    }

}
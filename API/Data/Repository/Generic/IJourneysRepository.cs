﻿using System.Collections.Generic;
using System.Threading.Tasks;
using API.Models;
using X.PagedList;

namespace API.Data.Repository.Generic {

    public interface IJourneysRepository : IRepository<Journey> {

        Task<IPagedList<Journey>> GetPaginatedJourneys(int companyId, int page, int limit);
        
        Task<IPagedList<Journey>> GetCompletePaginatedJourneys(int companyId, int page, int limit);
        
        Task<IPagedList<Journey>> GetIncompletePaginatedJourneys(int companyId, int page, int limit);
        
        Task<Journey> GetJourney(int id);

        Task<List<Journey>> GetIncompleteEmployeeJourneys(int employeeId);
    }

}
using API.Models;

namespace API.Data.Repository.Generic {

    public interface ICompaniesRepository : IRepository<Company> { }

}
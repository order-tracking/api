using System.Threading.Tasks;
using API.Models;

namespace API.Data.Repository.Generic {

    public interface IStatsRepository {
        Task<Stats> GetCompanyStats(int companyId);
    }

}
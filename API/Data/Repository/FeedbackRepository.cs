﻿using API.Data.Repository.Generic;
using API.Models;

namespace API.Data.Repository {

    public class FeedbackRepository : GenericRepository<Feedback>, IFeedbackRepository {
        public FeedbackRepository(DatabaseContext context) : base(context) { }
    }

}
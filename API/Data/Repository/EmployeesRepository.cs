using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using API.Data.Repository.Generic;
using API.Models;
using Microsoft.EntityFrameworkCore;
using X.PagedList;

namespace API.Data.Repository
{

    public class EmployeesRepository : GenericRepository<Employee>, IEmployeeRepository
    {
        public EmployeesRepository(DatabaseContext context) : base(context) { }

        public Task<List<Employee>> GetAllEmployeesOfCompany(int companyId)
        {
            return _context.Set<Employee>()
                .Where(emp => emp.CompanyId == companyId)
                .ToListAsync();
        }

        public Task<IPagedList<Employee>> GetPaginatedEmployeesOfCompany(
            int companyId,
            int pageNumber,
            int limit)
        {
            return _context.Set<Employee>()
                .Where(emp => emp.CompanyId == companyId)
                .ToPagedListAsync(pageNumber, limit);
        }

        public Task<Employee> GetEmployee(int id)
        {
            return _context.Set<Employee>()
                .Where(emp => emp.Id == id)
                .FirstOrDefaultAsync();
        }

        public Task<Employee> GetEmployeeByPhone(string phoneNumber)
        {
            return _context.Set<Employee>()
                .Where(emp => emp.PhoneNumber.Equals(phoneNumber))
                .FirstOrDefaultAsync();
        }

        public Task<List<Employee>> GetEmployeesDistributing()
        {
            return _context.Set<Employee>()
                .Where(emp => emp.Distributing)
                .ToListAsync();
        }

        /// <summary>
        /// Working users are the ones with the Pipeline view open
        /// They are working their most recent heartbeat (LastTimeOnline) is more recent than
        /// x minutes
        /// </summary>
        /// <returns></returns>
        public Task<List<Employee>> GetEmployeesWorking()
        {
            const int minutes = -3;
            var future = DateTime.UtcNow.AddMinutes(minutes);
            return _context.Set<Employee>()
                .Where(emp => !emp.Distributing)
                .Where(emp =>
                    emp.LastTimeOnline != null &&
                    emp.LastTimeOnline >= future)
                .ToListAsync();
        }
    }
}

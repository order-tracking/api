using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using API.Data.Repository.Generic;
using API.Models;
using Microsoft.EntityFrameworkCore;
using X.PagedList;

namespace API.Data.Repository {

    public class ProcessPipelineRepository : GenericRepository<ProcessPipeline>, IProcessPipelineRepository {
        public ProcessPipelineRepository(DatabaseContext context) : base(context) { }

        public new Task<ProcessPipeline> Get(int id) {
            return _context.Set<ProcessPipeline>()
                .Where(pp => pp.Id == id)
                .Include(pp => pp.Processes)
                .ThenInclude(process => process.Items)
                .ThenInclude(item => item.Product)
                .Include(pp => pp.Processes)
                .ThenInclude(process => process.Items)
                .ThenInclude(item => item.ProductType)
                .FirstOrDefaultAsync();
        }

        public Task<IPagedList<ProcessPipeline>> GetPaginatedPipelinesOfCompany(int companyId, int page, int limit) {
            return _context.Set<ProcessPipeline>()
                .Where(pp => pp.CompanyId == companyId)
                .Include(pp => pp.Processes)
                .ThenInclude(process => process.Items)
                .ThenInclude(item => item.Product)
                .Include(pp => pp.Processes)
                .ThenInclude(process => process.Items)
                .ThenInclude(item => item.ProductType)
                .ToPagedListAsync(page, limit);
        }
        
        public Task<List<Item>> GetProcessItems(int processId) {
            return _context.Set<Item>()
                .Where(item => item.ProcessId == processId)
                .Include(item => item.Product)
                .Include(item => item.ProductType)
                .ToListAsync();
        }
    }

}
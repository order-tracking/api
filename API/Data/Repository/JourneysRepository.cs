﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using API.Data.Repository.Generic;
using API.Models;
using Microsoft.EntityFrameworkCore;
using X.PagedList;

namespace API.Data.Repository {

    public class JourneysRepository : GenericRepository<Journey>, IJourneysRepository {
        public JourneysRepository(DatabaseContext context) : base(context) { }


        public Task<IPagedList<Journey>> GetPaginatedJourneys(int companyId, int page, int limit) {
            return _context.Set<Journey>()
                .Where(journey => journey.CompanyId == companyId)
                .OrderBy(journey => journey.CreatedAt)
                .Include(journey => journey.Employee)
                .Include(journey => journey.Orders)
                .ThenInclude(journey => journey.Customer)
                .ToPagedListAsync(page, limit);
        }

        public Task<IPagedList<Journey>> GetCompletePaginatedJourneys(int companyId, int page, int limit) {
            return _context.Set<Journey>()
                .Where(journey => journey.CompanyId == companyId && journey.Completed)
                .OrderBy(journey => journey.CreatedAt)
                .Include(journey => journey.Employee)
                .Include(journey => journey.Orders)
                .ThenInclude(journey => journey.Customer)
                .ToPagedListAsync(page, limit);
        }

        public Task<IPagedList<Journey>> GetIncompletePaginatedJourneys(int companyId, int page, int limit) {
            return _context.Set<Journey>()
                .Where(journey => journey.CompanyId == companyId && !journey.Completed)
                .OrderBy(journey => journey.CreatedAt)
                .Include(journey => journey.Employee)
                .Include(journey => journey.Orders)
                .ThenInclude(journey => journey.Customer)
                .ToPagedListAsync(page, limit);
        }

        public Task<Journey> GetJourney(int id) {
            return _context.Set<Journey>()
                .Where(journey => journey.Id == id)
                .Include(journey => journey.Employee)
                .Include(journey => journey.Company)
                .Include(journey => journey.Orders)
                .ThenInclude(journey => journey.Customer)
                .Include(journey => journey.Orders)
                .ThenInclude(order => order.Items)
                .ThenInclude(item => item.Product)
                .Include(journey => journey.Orders)
                .ThenInclude(order => order.Items)
                .ThenInclude(item => item.ProductType)
                .FirstOrDefaultAsync();
        }

        public Task<List<Journey>> GetIncompleteEmployeeJourneys(int employeeId) {
            return _context.Set<Journey>()
                .Where(journey => journey.EmployeeId == employeeId && !journey.Completed)
                .Include(journey => journey.Employee)
                .Include(journey => journey.Orders)
                .ThenInclude(journey => journey.Customer)
                .ToListAsync();
        }

        public new Task<Journey> Create(Journey journey) {
            return _context.Set<Order>()
                // Locate all orders for journey
                .Where(order => journey.Orders.Exists(o => o.Id == order.Id))
                .ToListAsync()
                .ContinueWith(orders => {
                    // Create the order on the DB
                    journey.Orders = new List<Order>(orders.Result);
                    _context.Set<Journey>().Add(journey);
                    return SaveAsync(journey);
                }).Unwrap();
        }
    }

}

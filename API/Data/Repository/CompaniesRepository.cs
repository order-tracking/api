using API.Data.Repository.Generic;
using API.Models;

namespace API.Data.Repository {

    public class CompaniesRepository : GenericRepository<Company>, ICompaniesRepository {
        public CompaniesRepository(DatabaseContext context) : base(context) { }
    }

}
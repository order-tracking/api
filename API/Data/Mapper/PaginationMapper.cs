using System.Collections.Generic;
using API.DTOs.View;

namespace API.Data.Mapper {

    public class PaginationMapper {
        public static PaginatedListViewDto<T> Map<T>(List<T> items, int page, int limit, int count, bool hasNextPage,
            bool hasPreviousPage) {
            return new PaginatedListViewDto<T> {
                Page = page,
                Limit = limit,
                Count = count,
                HasNextPage = hasNextPage,
                HasPreviousPage = hasPreviousPage,
                Items = items
            };
        }
    }

}
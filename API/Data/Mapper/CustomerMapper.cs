using System.Collections.Generic;
using API.DTOs.Creation;
using API.DTOs.Edition;
using API.DTOs.View;
using API.Models;

namespace API.Data.Mapper {

    public static class CustomerMapper {
        /// <summary>
        /// Maps the Model entity to View entity
        /// </summary>
        /// <param name="dto">Model entity</param>
        /// <returns>View entity</returns>
        public static CustomerViewDto Map(Customer dto) {
            List<OrderViewDto> orders = new List<OrderViewDto>();

            dto.Orders?.ForEach(order => {
                OrderViewDto item = OrderMapper.Map(order);
                orders.Add(item);
            });

            return new CustomerViewDto {
                Id = dto.Id,
                Name = dto.Name,
                PhoneNumber = dto.PhoneNumber,
                Orders = orders.Count > 0 ? orders : null
            };
        }
        
        /// <summary>
        /// Maps the Creation dto to Model entity
        /// </summary>
        /// <param name="dto">Creation dto</param>
        /// <returns>Model entity</returns>
        public static Customer Map(CustomerCreationDTO dto) {
            return new Customer {
                Name = dto.Name,
                PhoneNumber = dto.PhoneNumber
            };
        }
        
        /// <summary>
        /// Edits the customer model entity with the dto fields
        /// </summary>
        /// <param name="customer">Customer model entity</param>
        /// <param name="dto">Customer edition entity</param>
        public static void Change(this Customer customer, CustomerEditionDto dto) {
            customer.Name = dto.Name;
            customer.PhoneNumber = dto.PhoneNumber;
        }
    }

}
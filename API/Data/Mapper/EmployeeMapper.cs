using API.DTOs.Creation;
using API.DTOs.Edition;
using API.DTOs.View;
using API.Models;

namespace API.Data.Mapper {

    public static class EmployeeMapper {
        /// <summary>
        /// Maps the Model entity to View entity
        /// </summary>
        /// <param name="dto">Model entity</param>
        /// <returns>View entity</returns>
        public static EmployeeViewDto Map(Employee dto)
        {
            if (dto == null)
                return null;
            
            return new EmployeeViewDto {
                Id = dto.Id,
                Name = dto.Name,
                PhoneNumber = dto.PhoneNumber,
                CompanyId = dto.CompanyId,
                Role = dto.Role,
                CompanyName = dto.Company?.Name
            };
        }
        
        /// <summary>
        /// Maps the Creation dto to Model entity 
        /// </summary>
        /// <param name="dto">Creation dto</param>
        /// <returns>Model entity</returns>
        public static Employee Map(EmployeeCreationDTO dto) {
            return new Employee {
                Name = dto.Name,
                PhoneNumber = dto.PhoneNumber,
                HashedPassword = dto.Password,
                Role = dto.Role
            };
        }
        
        /// <summary>
        /// Edits the employee model entity with the dto fields
        /// </summary>
        /// <param name="employee">Employee model entity</param>
        /// <param name="dto">Employee edition entity</param>
        public static void Change(this Employee employee, EmployeeEditionDto dto) {
            employee.Name = dto.Name;
            employee.Role = dto.Role;
        }
    }

}

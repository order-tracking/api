using System.Collections.Generic;
using System.Linq;
using API.DTOs.Creation;
using API.DTOs.View;
using API.Models;

namespace API.Data.Mapper {

    public class ProductMapper {
        
        /// <summary>
        /// Maps the Creation dto to Model entity
        /// </summary>
        /// <param name="dto">Creation dto</param>
        /// <returns>Model entity</returns>
        public static Product Map(ProductCreationDTO dto) {
            return new Product {
                Name = dto.Name,
                Photo = dto.Photo,
                PipelineId = dto.PipelineId,
                ProductGroupId = dto.ProductGroupId,
                ProductTypes = dto.ProductTypes?.Select(ProductTypeMapper.Map).ToList()
            };
        }

        /// <summary>
        /// Maps the Model entity to View entity
        /// </summary>
        /// <param name="dto">Model entity</param>
        /// <returns>View entity</returns>
        public static ProductViewDto Map(Product dto) {
            var product = new ProductViewDto {
                Id = dto.Id,
                Photo = dto.Photo,
                Name = dto.Name,
                ProductTypes = new List<ProductTypeViewDto>()
            };

            if (dto.ProductTypes != null)
            {
                product.ProductTypes = dto.ProductTypes
                    .Select(ProductTypeMapper.Map).ToList();
            }
            
            return product;
        }
    }

}

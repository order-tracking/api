﻿using API.DTOs.View;
using API.Models;

namespace API.Data.Mapper {

    public class CampaignMapper {
        
        /// <summary>
        /// Maps the Model entity to View entity
        /// </summary>
        /// <param name="dto">Model entity</param>
        /// <returns>View entity</returns>
        public static CampaignViewDto Map(Campaign dto) {
            return new CampaignViewDto {
                Id = dto.Id,
                Title = dto.Title,
                Message = dto.Message,
                CreatedAt = dto.CreatedAt,
                TotalSmsSent = dto.TotalSmsSent,
                TotalCustomers = dto.TotalCustomers
            };
        }
    }

}

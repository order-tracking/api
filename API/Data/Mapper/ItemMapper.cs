using API.DTOs.Creation;
using API.DTOs.Edition;
using API.DTOs.View;
using API.Models;

namespace API.Data.Mapper {

    public static class ItemMapper {
        /// <summary>
        /// Maps the Model entity to View entity
        /// </summary>
        /// <param name="dto">Model entity</param>
        /// <returns>View entity</returns>
        public static ItemViewDto Map(Item dto) {
            return new ItemViewDto {
                Id = dto.Id,
                Note = dto.Note,
                Quantity = dto.Quantity,
                Completed = dto.Completed,
                OrderId = dto.OrderId,
                ProcessId = dto.ProcessId,
                Product = ProductMapper.Map(dto.Product),
                ProductType = dto.ProductType != null ? ProductTypeMapper.Map(dto.ProductType) : null
            };
        }
        
        /// <summary>
        /// Maps the Creation dto to Model entity
        /// </summary>
        /// <param name="dto">Creation dto</param>
        /// <returns>Model entity</returns>
        public static Item Map(ItemCreationDTO dto) {
            return new Item {
                Note = dto.Note,
                Quantity = dto.Quantity,
                Completed = false,
                ProductId = dto.ProductId,
                ProductTypeId = dto.ProductTypeId
            };
        }
        
        /// <summary>
        /// Edits the item model entity with the dto fields
        /// </summary>
        /// <param name="item">Item model entity</param>
        /// <param name="dto">Item edition entity</param>
        public static void Change(this Item item, ItemEditionDto dto) {
            item.Completed = dto.Completed;
            item.ProcessId = dto.ProcessId;
        }
    }

}

using API.DTOs.Creation;
using API.DTOs.View;
using API.Models;

namespace API.Data.Mapper {

    public class ProductTypeMapper {
        
        /// <summary>
        /// Maps the Model entity to View entity
        /// </summary>
        /// <param name="dto">Model entity</param>
        /// <returns>View entity</returns>
        public static ProductType Map(ProductTypeCreationDTO dto) {
            return new ProductType {
                Name = dto.Name
            };
        }
        
        /// <summary>
        /// Maps the Model entity to View entity
        /// </summary>
        /// <param name="dto">Model entity</param>
        /// <returns>View entity</returns>
        public static ProductTypeViewDto Map(ProductType dto) {
            return new ProductTypeViewDto {
                Id = dto.Id,
                Name = dto.Name
            };
        }
    }

}
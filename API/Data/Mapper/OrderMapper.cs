using System.Collections.Generic;
using System.Linq;
using API.DTOs.Creation;
using API.DTOs.Edition;
using API.DTOs.View;
using API.Models;
using API.Utils;

namespace API.Data.Mapper {

    public static class OrderMapper {
        
        /// <summary>
        /// Maps the Creation dto to Model entity
        /// </summary>
        /// <param name="dto">Creation dto</param>
        /// <returns>Model entity</returns>
        public static Order Map(OrderCreationDTO dto) {
            Order order = new Order {
                ReadyToDistribute = false,
                ItemsComplete = false,
                OnGoing = false,
                OnArrivingNotified = false,
                CustomerId = dto.CustomerId,
                Address = dto.Address,
                Address2 = dto.Address2,
                PostalCode = dto.PostalCode,
                Items = new List<Item>()
            };

            dto.Items.ForEach(itemDto => {
                Item item = ItemMapper.Map(itemDto);
                item.Order = order;
                order.Items.Add(item);
            });

            return order;
        }

        /// <summary>
        /// Maps the Model entity to View entity
        /// </summary>
        /// <param name="dto">Model entity</param>
        /// <returns>View entity</returns>
        public static OrderViewDto Map(Order dto) {
            return new OrderViewDto {
                Id = dto.Id,
                Distance = dto.Distance,
                ConfectionTime = dto.ConfectionTime,
                JourneyDuration = dto.JourneyDuration,
                HashId = HashIdsUtils.Encode(dto.Id),
                EstimatedDate = dto.EstimatedDate,
                ReadyToDistribute = dto.ReadyToDistribute,
                ItemsComplete = dto.ItemsComplete,
                OnGoing = dto.OnGoing,
                CreatedAt = dto.CreatedAt,
                DeliveredAt = dto.DeliveredAt,
                OnArrivingNotified = dto.OnArrivingNotified,
                Address = dto.Address,
                Address2 = dto.Address2,
                PostalCode = dto.PostalCode,
                FeedbackId = dto.FeedbackId,
                Latitude = dto.Latitude,
                Longitude = dto.Longitude,
                Items = dto.Items?.Select(item => ItemMapper.Map(item)).ToList(),
                Customer = dto.Customer != null ? new CustomerViewDto {
                    Id = dto.Customer.Id,
                    Name = dto.Customer.Name,
                    PhoneNumber = dto.Customer.PhoneNumber
                } : null
            };
        }
        
        /// <summary>
        /// Edits the order model entity with the dto fields
        /// </summary>
        /// <param name="order">Order model entity</param>
        /// <param name="dto">Order edition entity</param>
        public static void Change(this Order order, OrderEditionDto dto) {
            order.Address = dto.Address;
            order.Address2 = dto.Address2;
            order.PostalCode = dto.PostalCode;
            order.ReadyToDistribute = dto.ReadyToDistribute;
            order.OnGoing = dto.OnGoing;
            order.DeliveredAt = dto.DeliveredAt;
        }
    }

}

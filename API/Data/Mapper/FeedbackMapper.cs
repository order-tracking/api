﻿using API.DTOs.Creation;
using API.DTOs.Edition;
using API.DTOs.View;
using API.Models;
using API.Utils;

namespace API.Data.Mapper {

    public static class FeedbackMapper {
        /// <summary>
        /// Maps the Model entity to View entity
        /// </summary>
        /// <param name="dto">Model entity</param>
        /// <returns>View entity</returns>
        public static FeedbackViewDto Map(Feedback dto) {
            return new FeedbackViewDto {
                Id = dto.Id,
                Note = dto.Note,
                Rating = dto.Rating,
                OrderId = dto.OrderId,
                HashOrderId = HashIdsUtils.Encode(dto.OrderId)
            };
        }
        
        /// <summary>
        /// Maps the Creation dto to Model entity
        /// </summary>
        /// <param name="dto">Creation dto</param>
        /// <returns>Model entity</returns>
        public static Feedback Map(FeedbackCreationDTO dto) {
            return new Feedback {
                Rating = dto.Rating,
                Note = dto.Note,
                OrderId = HashIdsUtils.Decode(dto.OrderHashedId)
            };
        }

        /// <summary>
        /// Edits the feedback model entity with the dto fields
        /// </summary>
        /// <param name="feedback">Feedback model entity</param>
        /// <param name="dto">Feedback edition entity</param>
        public static void Change(this Feedback feedback, FeedbackEditionDto dto) {
            feedback.Note = dto.Note;
            feedback.Rating = dto.Rating;
        }
    }

}
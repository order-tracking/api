using System.Collections.Generic;
using System.Linq;
using API.DTOs.Creation;
using API.DTOs.Edition;
using API.DTOs.View;
using API.Models;

namespace API.Data.Mapper {

    public static class ProcessPipelineMapper {
        
        /// <summary>
        /// Maps the Creation dto to Model entity
        /// </summary>
        /// <param name="dto">Creation dto</param>
        /// <returns>Model entity</returns>
        public static ProcessPipeline Map(ProcessPipelineCreationDTO dto) {
            ProcessPipeline pipeline = new ProcessPipeline {
                Name = dto.Name,
                Processes = new List<Process>()
            };

            dto.Processes.ForEach(processDto => {
                Process process = ProcessMapper.Map(processDto);
                process.Pipeline = pipeline;
                pipeline.Processes.Add(process);
            });

            return pipeline;
        }

        /// <summary>
        /// Maps the Model entity to View entity
        /// </summary>
        /// <param name="dto">Model entity</param>
        /// <returns>View entity</returns>
        public static ProcessPipelineViewDto Map(ProcessPipeline dto) {
            ProcessPipelineViewDto view = new ProcessPipelineViewDto {
                Id = dto.Id,
                Name = dto.Name
            };

            if (dto.Processes != null)
                view.Processes = dto.Processes
                    .Select(process => ProcessMapper.Map(process))
                    .OrderBy(process => process.OrderNumber)
                    .ToList(); 

            return view;
        }
        
        /// <summary>
        /// Edits the ProcessPipeline model entity with the dto fields
        /// </summary>
        /// <param name="pipeline">ProcessPipeline model entity</param>
        /// <param name="dto">ProcessPipeline edition entity</param>
        public static void Change(this ProcessPipeline pipeline, ProcessPipelineEditionDto dto) {
            pipeline.Name = dto.Name;
            for (var i = 0; i < pipeline.Processes.Count; i++) {
                pipeline.Processes[i].Name = dto.Processes[i].Name;
            }
        }
    }

}
﻿using System;
using System.Linq;
using API.DTOs.Creation;
using API.DTOs.Edition;
using API.DTOs.View;
using API.Models;

namespace API.Data.Mapper {

    public static class JourneyMapper {
        
        /// <summary>
        /// Maps the Model entity to View entity
        /// </summary>
        /// <param name="journey">Model entity</param>
        /// <returns>View entity</returns>
        public static JourneyViewDto Map(Journey journey) {
            return new JourneyViewDto {
                Id = journey.Id,
                Completed = journey.Completed,
                OnGoing = journey.OnGoing,
                Employee = EmployeeMapper.Map(journey.Employee),
                Orders = journey.Orders.Select(OrderMapper.Map).ToList(),
                FinishedAt = journey.FinishedAt,
                CreatedAt = journey.CreatedAt
            };
        }

        /// <summary>
        /// Maps the Creation dto to Model entity
        /// </summary>
        /// <param name="journey">Creation dto</param>
        /// <returns>Model entity</returns>
        public static Journey Map(JourneyCreationDTO journey) {
            return new Journey {
                Completed = false,
                EmployeeId = journey.EmployeeId,
                Orders = journey.Orders.Select(order => new Order{Id = order.Id}).ToList()
            };
        }
        
        /// <summary>
        /// Edits the journey model entity with the dto fields
        /// </summary>
        /// <param name="journey">Journey model entity</param>
        /// <param name="dto">Journey edition entity</param>
        public static void Change(this Journey journey, JourneyEditionDto dto) {
            journey.Completed = dto.Completed;
            if (journey.Completed)
            {
                journey.FinishedAt = DateTime.UtcNow;
            }
        }
    }

}

using System.Linq;
using API.DTOs.Creation;
using API.DTOs.Edition;
using API.DTOs.View;
using API.Models;

namespace API.Data.Mapper {

    public static class ProductGroupMapper {
        
        /// <summary>
        /// Maps the Creation dto to Model entity
        /// </summary>
        /// <param name="dto">Creation dto</param>
        /// <returns>Model entity</returns>
        public static ProductGroup Map(ProductGroupCreationDTO dto) {
            return new ProductGroup {
                Name = dto.Name,
                PreviousProductGroupId = dto.PreviousProductGroupId
            };
        }

        /// <summary>
        /// Maps the Model entity to View entity
        /// </summary>
        /// <param name="dto">Model entity</param>
        /// <returns>View entity</returns>
        public static ProductGroupViewDto Map(ProductGroup dto) {
            var view = new ProductGroupViewDto {
                Id = dto.Id,
                Name = dto.Name
            };

            if (dto.Products != null) {
                view.Products = dto.Products
                    .Select(product => ProductMapper.Map(product))
                    .ToList();
            }

            if (dto.NextProductGroups != null) {
                view.NextProductGroups = dto.NextProductGroups
                    .Select(Map)
                    .ToList();
            }

            return view;
        }
        
        /// <summary>
        /// Edits the ProductGroup model entity with the dto fields
        /// </summary>
        /// <param name="pg">ProductGroup model entity</param>
        /// <param name="dto">ProductGroup edition entity</param>
        public static void Change(this ProductGroup pg, ProductGroupEditionDto dto) {
            pg.Name = dto.Name;
        }
    }

}
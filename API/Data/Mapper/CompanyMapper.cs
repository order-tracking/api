﻿using API.DTOs.Edition;
using API.DTOs.View;
using API.Models;

namespace API.Data.Mapper {

    public static class CompanyMapper {
        
        /// <summary>
        /// Maps the Model entity to View entity
        /// </summary>
        /// <param name="dto">Model entity</param>
        /// <returns>View entity</returns>
        public static CompanyViewDto Map(Company dto) {
            return new CompanyViewDto {
                Id = dto.Id,
                Name = dto.Name,
                Address = dto.Address,
                PostalCode = dto.PostalCode,
                MaxAllowedDistance = dto.MaxAllowedDistance
            };
        }

        /// <summary>
        /// Edits the company model entity with the dto fields
        /// </summary>
        /// <param name="company">Company model entity</param>
        /// <param name="dto">Company edition entity</param>
        public static void Change(this Company company, CompanyEditionDto dto) {
            company.Name = dto.Name;
            company.Address = dto.Address;
            company.PostalCode = dto.PostalCode;
            company.MaxAllowedDistance = dto.MaxAllowedDistance;
        }
    }

}
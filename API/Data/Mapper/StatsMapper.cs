using API.DTOs.View;
using API.Models;

namespace API.Data.Mapper {

    public class StatsMapper {
        
        /// <summary>
        /// Maps the Model entity to View entity
        /// </summary>
        /// <param name="stats">Model entity</param>
        /// <returns>View entity</returns>
        public static StatsViewDto Map(Stats stats) {
            return new StatsViewDto {
                PendingOrders = stats.PendingOrders,
                DistributorsWorking = stats.DistributorsWorking,
                EmployeesWorking = stats.EmployeesWorking,
                OrdersCompletedToday = stats.OrdersCompletedToday
            };
        }
    }

}
using System.Collections.Generic;
using System.Linq;
using API.DTOs.Creation;
using API.DTOs.View;
using API.Models;

namespace API.Data.Mapper {

    public static class ProcessMapper {
        
        /// <summary>
        /// Maps the Creation dto to Entity Model
        /// </summary>
        /// <param name="dto">Creation dto</param>
        /// <returns>Entity Model</returns>
        public static Process Map(ProcessCreationDTO dto) {
            return new Process {
                Name = dto.Name,
                OrderNumber = dto.OrderNumber
            };
        }

        /// <summary>
        /// Maps the Model entity to View entity
        /// </summary>
        /// <param name="dto">Model entity</param>
        /// <returns>View entity</returns>
        public static ProcessViewDto Map(Process dto) {
            return new ProcessViewDto {
                Id = dto.Id,
                Name = dto.Name,
                OrderNumber = dto.OrderNumber,
                Items = dto.Items != null ? dto.Items.Select(ItemMapper.Map).ToList() : new List<ItemViewDto>()
            };
        }
    }

}